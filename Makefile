SHADOW_BUILD_DIR = release

SHADOW_DEBUG_DIR = debug

LIBNAME = Scale

DYNAMIC_RELEASE_LIB =  lib$(LIBNAME).so
DYNAMIC_DEBUG_LIB =  lib$(LIBNAME)-d.so
STATIC_RELEASE_LIB =  lib$(LIBNAME)-s.so
STATIC_DEBUG_LIB =  lib$(LIBNAME)-d-s.so
RELEASE_TEST =  Test$(LIBNAME)
DEBUG_TEST =  Test$(LIBNAME)-d

all: $(DYNAMIC_RELEASE_LIB)

$(DYNAMIC_RELEASE_LIB) :
	@mkdir -p $(SHADOW_BUILD_DIR)
	@cd $(SHADOW_BUILD_DIR) && cmake -DCMAKE_BUILD_TYPE=Release ..
	@make -j8 -C $(SHADOW_BUILD_DIR)

$(DYNAMIC_DEBUG_LIB) :
	@mkdir -p $(SHADOW_DEBUG_DIR)
	@cd $(SHADOW_DEBUG_DIR) && cmake -DCMAKE_BUILD_TYPE=Debug ..
	@make -j8 -C $(SHADOW_DEBUG_DIR)

release : $(DYNAMIC_RELEASE_LIB)
	@./deploy.sh

debug : $(DYNAMIC_DEBUG_LIB)

clean :
	@rm -Rf $(SHADOW_BUILD_DIR)
	@rm -Rf $(SHADOW_DEBUG_DIR)

fclean : clean
	@rm -Rf bin/linux/*

re: fclean all

test: fclean debug
	clear
	@cd ./bin/linux && ./$(DEBUG_TEST)
	
memory: fclean debug
	clear
	@cd ./bin/linux && valgrind ./$(DEBUG_TEST)

install: uninstall fclean release debug
	sudo cp bin/linux/$(DYNAMIC_RELEASE_LIB) /usr/lib
	sudo cp bin/linux/$(DYNAMIC_DEBUG_LIB) /usr/lib
	sudo cp -R include/$(LIBNAME) /usr/include

uninstall:
	sudo rm -rf /usr/lib/$(DYNAMIC_RELEASE_LIB)
	sudo rm -rf /usr/lib/$(DYNAMIC_DEBUG_LIB)
	sudo rm -rf /usr/include/$(LIBNAME)
