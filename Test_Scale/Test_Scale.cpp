#include <iostream>
#include "Test_Scale.h"

int sum(Scale::CallStack& stack)
{
	int a = static_cast<int>(stack["a"]);
	int b = static_cast<int>(stack["b"]);
	return a + b;
}

void mutateVar(Scale::CallStack& stack)
{
	stack["pos"].Set("{42, 42}");
}

TestMain::TestMain(const std::vector<std::string>& arguments) : interpreter(arguments) {}

void TestMain::RegisterLinkers()
{
	Scale::Linker<Pos> posLinker("Pos");
	posLinker.AddTag(Scale::Tag{ "Cpp", {"Object"} });
	posLinker.Register("x", &Pos::x);
	posLinker.Register("y", &Pos::y);
	Scale::Linker<City> cityLinker("City");
	cityLinker.AddTag(Scale::Tag{ "Cpp", {"Object"} });
	cityLinker.Register("pos", &City::pos, posLinker, Pos{ 0, 0 });
	cityLinker.Register("name", &City::name);
	Scale::Linker<Country> countryLinker("Country");
	countryLinker.AddTag(Scale::Tag{ "Cpp", {"Object"} });
	countryLinker.Register("name", &Country::name);
	countryLinker.Register("capital", { Scale::Tag {"Hidden"} }, &Country::capital, cityLinker);
	interpreter.RegisterLinker(posLinker);
	interpreter.RegisterLinker(cityLinker); //Add city AFTER pos because it depend on it
	interpreter.RegisterLinker(countryLinker); //Add country AFTER city because it depend on it
}

void TestMain::RegisterFunctions()
{
	interpreter.RegisterFunction("int sum(int a, int b ? 4)", &sum, { Scale::Tag{"Cpp", {"Function"}} });
	interpreter.RegisterFunction("mutate(Pos &pos)", &mutateVar, { Scale::Tag{"Cpp", {"Function"}} });
}

void TestMain::RegisterGlobals()
{
	std::vector<Pos> vectorOfPos = { {0, 0}, {42, 42}, {69, 69}, {12, 3} };
	//array vectorOfPos = [Pos {0, 0}, {42, 42}, {69, 69}, {12, 3}] or array[Pos] vectorOfPos = [{0, 0}, {42, 42}, {69, 69}, {12, 3}]
	interpreter.RegisterArray("ARRAY_OF_POS", { Scale::Tag{"Cpp", {"Global"}} }, vectorOfPos);
}

void TestMain::RegisterAll()
{
	RegisterLinkers();
	RegisterFunctions();
	RegisterGlobals();
}

void TestMain::TestPureCpp()
{
	int result = -1;
	interpreter.CallInto("sum", result, 2, 3);
	std::cout << "Result = " << result << std::endl;
	interpreter.CallInto("sum", result, 6);
	std::cout << "Result with default = " << result << std::endl;
	Pos test{ 0, 0 };
	std::cout << "Pos {x = " << test.x << ", y = " << test.y << "}" << std::endl;
	interpreter.CallVoid("mutate", test);
	interpreter.Mutate(0, test);
	std::cout << "Pos {x = " << test.x << ", y = " << test.y << "}" << std::endl;
}

void TestMain::TestComboCppScale()
{
	Scale::OutputManager::Print(Scale::OutputType::OUTPUT, interpreter.GetGlobal("ITALY"));
	Scale::OutputManager::Print(Scale::OutputType::OUTPUT, interpreter.GetGlobal("ARRAY_OF_POS"));
	Scale::Variable var = interpreter.CreateVar("testVar", "City", { Scale::Tag{"Cpp", {"Variable"}} });
	Scale::OutputManager::Print(Scale::OutputType::OUTPUT, var);
	if (var.Have("pos")) //Check if pos exist to build it with SmartBuild
		Scale::OutputManager::Print(Scale::OutputType::OUTPUT, var);
}

void TestMain::TestPureScale()
{
	Scale::OutputManager::Print(Scale::OutputType::OUTPUT, interpreter.GetGlobal("FRANCE"));
	Scale::OutputManager::Print(Scale::OutputType::OUTPUT, interpreter.GetGlobal("TEST_ARRAY"));
	Scale::Variable scaleVar = interpreter.CreateVar("testScaleVar", "ScaleCity", { Scale::Tag{"Scale", {"Variable"}} });
	Scale::OutputManager::Print(Scale::OutputType::OUTPUT, scaleVar);
	if (scaleVar.Have("pos")) //Check if pos exist to build it with SmartBuild
		Scale::OutputManager::Print(Scale::OutputType::OUTPUT, scaleVar);
}

void TestMain::Start()
{
	RegisterAll();
	TestPureCpp();
	if (interpreter.Load("D:/Programmation/Scale/scripts/test.sca", true))
	{
		TestComboCppScale();
		TestPureScale();
	}
}

int main()
{
	TestMain testMain({ "--DoVerbose:false", "--DisplayName:true", "--DisplayTag:true", "--DoCodingStyle:true", "--DefaultCodingStyle:true" });
	testMain.Start();
	return 0;
}