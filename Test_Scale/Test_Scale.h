#pragma once

#include "Scale/CallStack.h"
#include "Scale/Interpreter.h"
#include "Scale/Linker.h"
#include "Scale/OutputManager.h"

struct Pos
{
	int x;
	int y;
};

struct City
{
	Pos pos;
	std::string name;
};

struct Country
{
	std::string name;
	City capital;
};

class TestMain
{
private:
	Scale::Interpreter interpreter;

public:
	TestMain(const std::vector<std::string>&);
	void RegisterLinkers();
	void RegisterFunctions();
	void RegisterGlobals();
	void RegisterAll();
	void TestPureCpp();
	void TestComboCppScale();
	void TestPureScale();
	void Start();
};