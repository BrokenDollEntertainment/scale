#include "Interpreter.h"

#include "Caster.h"
#include "Caster.h"
#include "OutputManager.h"
#include "Parameters.h"
#include "ScriptLoader.h"

namespace Scale
{
	Interpreter::Interpreter(): m_native(new Native()), m_scriptLoader(new ScriptLoader()), m_current(nullptr)
	{
		Parameters::ParseArgs();
		if (!m_native)
			OutputManager::Error(OutputType::INITIALISATION, "Cannot create new native handler: Out of memory");
		if (!m_scriptLoader)
			OutputManager::Error(OutputType::INITIALISATION, "Cannot create new script loader: Out of memory");
	}

	Interpreter::Interpreter(const std::vector<std::string> &vec) : m_native(new Native()), m_scriptLoader(new ScriptLoader()), m_current(nullptr)
	{
		Parameters::ParseArgs(vec);
		if (!m_native)
			OutputManager::Error(OutputType::INITIALISATION, "Cannot create new native handler: Out of memory");
		if (!m_scriptLoader)
			OutputManager::Error(OutputType::INITIALISATION, "Cannot create new script loader: Out of memory");
	}

	Interpreter::Interpreter(int argc, char **args) : Interpreter(std::vector<std::string>(args, args + argc)) {}

	bool Interpreter::Load(const std::string &path, bool useLoadedScript)
	{
		if (!m_scriptLoader)
			return false;
		Script *loadedScript = m_scriptLoader->Load(path, m_native);
		if (!loadedScript)
			return false;
		if (useLoadedScript)
			m_current = loadedScript;
		return true;
	}

	bool Interpreter::Use(const std::string &path)
	{
		if (!m_scriptLoader)
			return false;
		Script *tmp = m_scriptLoader->Get(path);
		if (!tmp)
			return false;
		m_current = tmp;
		return true;
	}

	bool Interpreter::Reload(const std::string &path)
	{
		if (!m_scriptLoader)
			return false;
		return m_scriptLoader->Reload(path, m_native);
	}

	Variable Interpreter::CreateVar(const std::string &variableName, const std::string &variableType, const std::string &variableValue, const std::vector<Tag> &tags)
	{
		if (!m_scriptLoader)
			return Variable::NULL_VARIABLE;
		if (m_native && m_native->CheckType(variableType))
			return m_native->CreateVar(variableName, variableType, variableValue, tags);
		if (m_current)
			return m_current->CreateVar(variableName, variableType, variableValue, false, tags);
		return Variable::NULL_VARIABLE;
	}

	Variable Interpreter::GetGlobal(const std::string &name)
	{
		if (!m_scriptLoader)
			return Variable::NULL_VARIABLE;
		if (m_native && m_native->HaveGlobal(name))
			return m_native->GetGlobalVariable(name);
		if (m_current)
			return m_current->GetGlobal(name, false);
		return Variable::NULL_VARIABLE;
	}

	Callable *Interpreter::GetCallable(const std::string &name)
	{
		if (!m_scriptLoader)
			return nullptr;
		if (m_native && m_native->HaveCallable(name))
			return m_native->GetCallable(name);
		if (m_current)
			return m_current->GetCallable(name, false, false);
		return nullptr;
	}

	DeclaredObjectDefinition *Interpreter::GetDeclaredObject(const std::string &name)
	{
		if (!m_scriptLoader)
			return nullptr;
		if (m_native)
		{
			if (DeclaredObjectDefinition *nativeDefinition = m_native->GetObjectDefinition(name))
				return nativeDefinition;
		}
		if (m_current)
			return m_current->GetObjectDefinition(name, false, false);
		return nullptr;
	}

	Interpreter::~Interpreter()
	{
		if (m_native)
			delete(m_native);
		if (m_scriptLoader)
			delete(m_scriptLoader);
	}

	bool Interpreter::RegisterCaster(const Caster &caster)
	{
		if (Caster *newCaster = new Caster(caster))
			return m_native->AddCaster(newCaster);
		else
		{
			OutputManager::Error(OutputType::INITIALISATION, "Cannot create caster: Out of memory");
			return false;
		}
	}

	Variable Interpreter::Cast(Variable &from, const VariableType &type)
	{
// 		Unit *newUnit = nullptr;
// 		if (type == EVariableType::OBJECT)
// 		{
// 			if (auto *definitionType = GetObjectDefinition(arrayType->GetName()))
// 			{
// 				if (Parameters::Is("DoSmartBuild", true))
// 					newUnit = new Unit(definitionType, false, "", m_helper);
// 				else
// 					newUnit = new Unit(definitionType, true, "", m_helper);
// 			}
// 		}
// 		else
// 			newUnit = new Unit(unit);
		return Variable::NULL_VARIABLE;
	}

	Variable Interpreter::Cast(Variable &from, const EVariableType &type)
	{
		return Cast(from, VariableType(type));
	}

	Variable Interpreter::Cast(Variable &from, const std::string &type)
	{
		return Cast(from, VariableType(type));
	}
}