#pragma once

#include <vector>
#include "IResolutionTreeOperation.h"

namespace Scale
{
	class Script;
	class ResolutionTree final
	{
		friend class Function;
	protected:
		bool m_hasResolve;
		IResolutionTreeOperation *m_operation;
		std::vector<ResolutionTree *> m_childs;

	private:
		ResolutionTree(IResolutionTreeOperation *);
		ResolutionTree(IResolutionTreeOperation *, const std::vector<ResolutionTree *> &);
		Variable ResolveChild(Script *, CallStack &);
		bool Resolve(Script *, CallStack &);

	public:
		~ResolutionTree();
	};
}