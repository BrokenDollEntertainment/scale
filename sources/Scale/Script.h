﻿#pragma once

#include <atomic>
#include <vector>
#include "Native.h"
#include "Taggable.h"

namespace Scale
{
	enum class OutputType: char;
	class ScriptLoader;
	class Function;
	class Script final : public Taggable
	{
		friend class CurrentEnvironment;
		friend class CallStack;
		friend class Interpreter;
		friend class ScriptLoader;
	private:
		enum class ScriptInnerCall : int
		{
			Compile,
			GetObjectDefinition,
			GetGlobal,
			GetCallable,
			GetCaster,
			HaveObjectDefinition,
			HaveGlobal,
			HaveCallable,
			HaveCaster,
			CheckDuplicateObjectDefinition,
			CheckDuplicateGlobal,
			CheckDuplicateCallable,
			CheckDuplicateCaster,
			COUNT,
			INVALID
		};
		class TreeLocker;
		class TreeLock final
		{
			friend class TreeLocker;
			static constexpr unsigned int SCRIPT_INNER_CALL_SIZE = static_cast<unsigned int>(ScriptInnerCall::COUNT);
		private:
			std::atomic_bool m_locks[SCRIPT_INNER_CALL_SIZE];

		private:
			bool Lock(ScriptInnerCall);
			void Unlock(ScriptInnerCall);

		public:
			TreeLock();
		};
		class TreeLocker final
		{
		private:
			TreeLock *m_lock;
			ScriptInnerCall m_lockType;

		public:
			TreeLocker(TreeLock *lock, const ScriptInnerCall &lockType): m_lock(lock), m_lockType(lockType) {}
			inline bool Lock() {return m_lock->Lock(m_lockType);}
			~TreeLocker() {m_lock->Unlock(m_lockType);}
		};
		struct GlobalDefinition
		{
			std::string name;
			std::string typeName;
			std::string value;
			bool isPrivate;
			std::vector<Tag> tags;
		};
	private:
		const CurrentEnvironment &m_helper;
		TreeLock m_treeLock;
		std::string m_scriptName;
		bool m_compiled;
		//Native elements (implemented through the Interpreter)
		Native *m_native;
		//Imports
		std::vector<Script *> m_allImports;
		std::vector<Script *> m_pr_imports;
		std::vector<Script *> m_imports;
		std::unordered_set<std::string> m_registeredImportToCompile;
		//Declared object
		std::unordered_map<std::string, DeclaredObjectDefinition *> m_pr_declaredObjectDefinition;
		std::unordered_map<std::string, DeclaredObjectDefinition *> m_declaredObjectDefinition;
		std::unordered_set<std::string> m_registeredDefinitionToCompile;
		//Globals
		std::unordered_map<std::string, Unit *> m_pr_globals;
		std::unordered_map<std::string, Unit *> m_globals;
		std::unordered_set<std::string> m_registeredGlobalToCompile;
		std::vector<GlobalDefinition> m_globalToCompile;
		//Functions
		std::unordered_map<std::string, Callable *> m_pr_functions;
		std::unordered_map<std::string, Callable *> m_functions;
		std::unordered_set<std::string> m_registeredFunctionToCompile;
		//Casters
		std::unordered_map<VariableType, std::vector<Caster *>, VariableTypeHasher> m_pr_casters;
		std::unordered_map<VariableType, std::vector<Caster *>, VariableTypeHasher> m_casters;
		std::unordered_map<VariableType, std::unordered_set<VariableType, VariableTypeHasher>, VariableTypeHasher> m_registeredCasterToCompile;

	private:
		Script(Native *, const std::string &);
		bool CheckType(const VariableType &, bool, bool);
		bool CheckType(const std::string &, bool);
		bool AddGlobal(const std::string &, const std::vector<Tag> &);
		bool AddImport(const std::string &, const std::string &, const std::vector<Tag> &, ScriptLoader *);
		bool AddDeclaredObject(const std::string &, const std::vector<Tag> &);
		bool AddFunction(const std::string &, const std::vector<Tag> &);
		bool AddCast(const std::string &, const std::vector<Tag> &, bool);
		bool AddPublicCast(const std::string &, const std::vector<Tag> &);
		bool AddPrivateCast(const std::string &, const std::vector<Tag> &);
		DeclaredObjectDefinition *GetObjectDefinition(const std::string &, bool, bool);
		Unit *GetGlobal(const std::string &, bool, bool);
		Callable *GetCallable(const std::string &, bool, bool);
		ACaster *GetCaster(const VariableType &, const VariableType &, bool, bool);
		ACaster *GetCaster(const VariableType &, const VariableType &, bool, bool, bool);
		bool HaveObjectDefinition(const std::string &, bool, bool);
		bool HaveGlobal(const std::string &, bool, bool);
		bool HaveCallable(const std::string &, bool, bool);
		bool HaveCaster(const VariableType &, const VariableType &, bool, bool);
		bool HaveCaster(const VariableType &, const VariableType &, bool, bool, bool);
		template <typename t_SearchType>
		t_SearchType *Get(const std::string &name, bool searchInPrivate, bool searchSelf, bool searchNative, t_SearchType *(Native::*nativePtr)(const std::string &), t_SearchType *(Script::*ptr)(const std::string &, bool, bool), ScriptInnerCall lock, const std::unordered_map<std::string, t_SearchType *> &privateMap, const std::unordered_map<std::string, t_SearchType *> &publicMap)
		{
			TreeLocker locker(&m_treeLock, lock);
			if (!locker.Lock())
				return nullptr;
			if (searchNative)
			{
				auto *value = (m_native->*nativePtr)(name);
				if (value)
					return value;
			}
			if (searchSelf)
			{
				if (searchInPrivate)
				{
					auto privateMapIt = privateMap.find(name);
					if (privateMapIt != privateMap.end())
						return (*privateMapIt).second;
				}
				auto publicMapIt = publicMap.find(name);
				if (publicMapIt != publicMap.end())
					return (*publicMapIt).second;
			}
			if (searchInPrivate)
			{
				for (Script *privateImports : m_allImports)
				{
					auto *value = (privateImports->*ptr)(name, false, false);
					if (value)
						return value;
				}
			}
			else
			{
				for (Script *publicImport : m_imports)
				{
					auto *value = (publicImport->*ptr)(name, false, false);
					if (value)
						return value;
				}
			}
			return nullptr;
		}
		template <typename t_SearchType>
		bool Have(const std::string &name, bool searchInPrivate, bool searchSelf, bool searchNative, bool (Native::*nativePtr)(const std::string &), bool(Script::*ptr)(const std::string &, bool, bool), ScriptInnerCall lock, const std::unordered_map<std::string, t_SearchType *> &privateMap, const std::unordered_map<std::string, t_SearchType *> &publicMap)
		{
			TreeLocker locker(&m_treeLock, lock);
			if (!locker.Lock())
				return false;
			if ((searchNative && (m_native->*nativePtr)(name)) || (searchSelf && ((searchInPrivate && privateMap.find(name) != privateMap.end()) || publicMap.find(name) != publicMap.end())))
				return true;
			if (searchInPrivate)
			{
				for (Script *privateImports : m_allImports)
				{
					if ((privateImports->*ptr)(name, false, false))
						return true;
				}
			}
			else
			{
				for (Script *publicImport : m_imports)
				{
					if ((publicImport->*ptr)(name, false, false))
						return true;
				}
			}
			return false;
		}
		template <typename t_SearchType>
		std::string CheckDuplicate(std::unordered_set<std::string> &alreadyDeclared, std::string (Script::*ptr)(std::unordered_set<std::string> &), ScriptInnerCall lock, const std::unordered_map<std::string, t_SearchType *> &map)
		{
			TreeLocker locker(&m_treeLock, lock);
			if (!locker.Lock())
				return "";
			for (const auto &pair : map)
			{
				if (alreadyDeclared.find(pair.first) != alreadyDeclared.end())
					return pair.first;
				alreadyDeclared.insert(pair.first);
			}
			for (Script *import : m_imports)
			{
				const std::string &duplicateFound = (import->*ptr)(alreadyDeclared);
				if (duplicateFound.size() != 0)
					return duplicateFound;
			}
			return "";
		}
		std::string CheckDuplicateObjectDefinition(std::unordered_set<std::string> &);
		std::string CheckDuplicateGlobal(std::unordered_set<std::string> &);
		std::string CheckDuplicateCallable(std::unordered_set<std::string> &);
		std::string CheckDuplicateCaster(std::unordered_map<VariableType, std::unordered_set<VariableType, VariableTypeHasher>, VariableTypeHasher> &);
		bool CheckType(const VariableType &);
		bool CompileImport(Script *, std::unordered_set<std::string> &, std::unordered_set<std::string> &, std::unordered_set<std::string> &, std::unordered_map<VariableType, std::unordered_set<VariableType, VariableTypeHasher>, VariableTypeHasher> &);
		bool CompileImports();
		bool CompileDeclaredObjectDefinition(DeclaredObjectDefinition *);
		bool CompileDeclaredObjectDefinitions();
		bool CompileGlobals();
		bool CompileFunction(Function *);
		bool CompileFunctions();
		bool CompileCaster(Caster *);
		bool CompileCasters();
		void Clear();
		Unit *CreateUnit(OutputType, const std::string &, const std::string &, const std::string &, bool, const std::vector<Tag> &);
		Unit *CreateUnit(OutputType output, const std::string &variableName, const std::string &variableTypeName, bool searchInNative, const std::vector<Tag> &tags) {return CreateUnit(output, variableName, variableTypeName, "", searchInNative, tags);}
		Unit *CreateVar(OutputType, const std::string &, const std::string &, const std::string &, bool, const std::vector<Tag> &);
		Unit *CreateVar(OutputType output, const std::string &variableName, const std::string &variableTypeName, bool searchInNative, const std::vector<Tag> &tags) {return CreateVar(output, variableName, variableTypeName, "", searchInNative, tags);}
		Variable CreateVar(const std::string &, const std::string &, const std::string &, bool, const std::vector<Tag> &);
		Variable CreateVar(const std::string &, const std::string &, bool, const std::vector<Tag> &);
		Variable GetGlobal(const std::string &, bool);
		bool Compile();

	public:
		~Script();
	};
}
