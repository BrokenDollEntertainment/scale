#pragma once

#include "VariableType.h"

namespace Scale
{
	class Unit;
	class Parameter final
	{
		friend class Signature;
	private:
		std::string m_name;
		VariableType m_type;
		bool m_isMutable;
		Unit *m_defaultValue;

	private:
		void AssignDefaultValue(Unit *);

	public:
		Parameter(const Parameter &);
		Parameter &operator=(const Parameter &);
		Parameter(const std::string &, const VariableType &, bool);
		inline const std::string &GetName() const {return m_name;}
		inline const VariableType &GetType() const {return m_type;}
		inline bool IsMutable() const {return m_isMutable;}
		inline bool HaveDefaultValue() const {return m_defaultValue != nullptr;}
		inline Unit *GetDefaultValue() const {return m_defaultValue;}
	};
}