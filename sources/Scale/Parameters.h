#pragma once

#include <unordered_map>
#include <vector>

namespace Scale
{
	class Interpreter;
	class Parameters final
	{
		friend class Interpreter;
	private:
		static bool ms_isInit;
		static Parameters ms_parameters;
		std::unordered_map<std::string, bool> m_boolParameters;

	private:
		Parameters();
		void TreatArgumentValue(const std::string &, const std::string &);
		void AddParameter(const std::string &, bool);
		bool CompareParameterTo(const std::string &, bool);
		static void ParseArgs(const std::vector<std::string> &);
		static void ParseArgs();

	public:
		static inline bool Is(const std::string &name, bool value) {return ms_parameters.CompareParameterTo(name, value);}
	};
}