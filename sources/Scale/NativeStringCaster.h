#pragma once

#include "ACaster.h"

namespace Scale
{
	class NativeStringCaster : public ACaster
	{
		using super = ACaster;
		friend class Native;
	private:
		bool CanCast(const VariableType &, const VariableType &) override;
		bool Cast(Unit *, Unit *) override;
	};
}