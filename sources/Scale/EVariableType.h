#pragma once

namespace Scale
{
	enum class EVariableType : char
	{
		VOID,
		ARRAY,
		GROUP,
		BOOL,
		CHAR,
		UNSIGNED_CHAR,
		SHORT,
		UNSIGNED_SHORT,
		INT,
		UNSIGNED_INT,
		LONG,
		UNSIGNED_LONG,
		LONG_LONG,
		UNSIGNED_LONG_LONG,
		FLOAT,
		DOUBLE,
		LONG_DOUBLE,
		STRING,
		OBJECT,
		COUNT,
		INVALID
	};

	constexpr int EVARIABLETYPE_SIZE = static_cast<int>(EVariableType::COUNT);
}