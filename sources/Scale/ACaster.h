#pragma once

#include "Taggable.h"

namespace Scale
{
	class Unit;
	class VariableType;
	class ACaster : public Taggable
	{
		friend class CurrentEnvironment;
		friend class Native;
		friend class Script;
	private:
		virtual bool Cast(Unit *, Unit *) = 0;
		virtual bool CanCast(const VariableType &, const VariableType &) = 0;

	public:
		virtual ~ACaster() = default;
	};
}