#pragma once

#include "Signature.h"
#include "Taggable.h"

namespace Scale
{
	class CurrentEnvironment;
	class CallStack;
	class Variable;
	class Callable : public Taggable
	{
		friend class Function;
		friend class Native;
		friend class NativeUtils;
		friend class Script;
	protected:
		Signature m_signature;

	private:
		std::unordered_map<std::string, std::string> m_defaultValues;

	private:
		bool Compile(const CurrentEnvironment &);
		virtual bool Call(CallStack &) = 0;
		bool SetSignature(const std::string &, bool &);
		virtual bool CheckSignature() {return true;}

	public:
		bool operator()(CallStack &);
		bool CheckParameter(const std::vector<Variable> &) const;
		inline const std::string &GetName() const {return m_signature.m_name;}
		inline const VariableType &GetReturnType() const {return m_signature.m_returnType;}
		inline const std::vector<Parameter> &GetParameters() const {return m_signature.m_arguments;}
		virtual ~Callable() = default;
	};
}