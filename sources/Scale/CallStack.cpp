#include "CallStack.h"

#include "CodingStyle.h"
#include "LocalScope.h"
#include "Native.h"
#include "OutputManager.h"
#include "Parameter.h"
#include "Script.h"
#include "Variable.h"

namespace Scale
{
	CallStack::CallStack(Native *native, Script *current) : m_native(native), m_current(current)
	{
		OpenScope();
	}
	
	CallStack::CallStack(Native *native): CallStack(native, nullptr) {}

	CallStack::CallStack(Native *native, const std::vector<Parameter> &parameters, const std::vector<Variable> &variables): CallStack(native, nullptr, parameters, variables) {}

	CallStack::CallStack(Native *native, Script *current, const std::vector<Parameter> &parameters, const std::vector<Variable> &variables): CallStack(native, current)
	{
		size_t idx = 0;
		size_t variableSize = variables.size();
		for (const auto &parameter : parameters)
		{
			if (idx != variableSize)
			{
				if (parameter.IsMutable())
					m_parameters[parameter.GetName()] = variables[idx].GetMutable();
				else
					m_parameters[parameter.GetName()] = variables[idx].GetCopy();
				++idx;
			}
			else
				m_parameters[parameter.GetName()] = Variable(parameter.GetDefaultValue(), false);
		}
	}

	bool CallStack::CreateVar(const std::string &variableName, const std::string &variableType, const std::string &variableValue, const std::vector<Tag> &variableTags)
	{
		if (!CodingStyle::CheckName(CodingStyle::NameType::VARIABLE, variableName))
			return false;
		for (const auto &locals : m_locals)
		{
			if (locals->Have(variableName))
			{
				OutputManager::Error(OutputType::RUNTIME, "A local variable with the name ", variableName, " already exist");
				return false;
			}
		}
		if (m_parameters.find(variableName) != m_parameters.end())
		{
			OutputManager::Error(OutputType::RUNTIME, "A parameter with the name ", variableName, " already exist");
			return false;
		}
		else if (m_native->HaveGlobal(variableName) || (m_current && m_current->HaveGlobal(variableName, true, false)))
		{
			OutputManager::Error(OutputType::RUNTIME, "A global variable with the name ", variableName, " already exist");
			return false;
		}
		if (m_native->CheckType(variableType))
		{
			m_locals[0]->Add(variableName, m_native->CreateVar(variableName, variableType, variableValue, variableTags));
			return true;
		}
		else if (m_current && m_current->CheckType(variableType, false))
		{
			m_locals[0]->Add(variableName, m_current->CreateVar(variableName, variableType, variableValue, false, variableTags));
			return true;
		}
		OutputManager::Error(OutputType::RUNTIME, "Unknown type ", variableType);
		return false;
	}

	void CallStack::SetReturn(Variable &&ret)
	{
		m_return = ret;
	}

	bool CallStack::OpenScope()
	{
		LocalScope *newScope = new LocalScope();
		if (newScope)
		{
			m_locals.insert(m_locals.begin(), newScope);
			return true;
		}
		OutputManager::Error(OutputType::RUNTIME, "Cannot create new scope: Out of memory");
		return false;
	}

	void CallStack::CloseScope()
	{
		delete(*m_locals.erase(m_locals.begin()));
	}

	Variable CallStack::operator[](const std::string &name)
	{
		for (const auto &locals : m_locals)
		{
			if (locals->Have(name))
				return locals->Get(name);
		}
		auto it = m_parameters.find(name);
		if (it != m_parameters.end())
			return (*it).second.GetMutable();
		else if (m_native->HaveGlobal(name))
			return m_native->GetGlobal(name);
		else if (m_current)
			return m_current->GetGlobal(name, false);
		return Variable::NULL_VARIABLE;
	}

	CallStack::~CallStack()
	{
		for (auto *locals : m_locals)
			delete(locals);
	}
}