#include "Caster.h"

#include "OutputManager.h"
#include "Unit.h"

namespace Scale
{
	Caster::Caster(const std::string &typeA, const std::string &typeB, bool bidirectionnal): m_isBidirectional(bidirectionnal), m_typeA(typeA), m_typeB(typeB) {}

	Caster::Caster(const Caster &other): m_isBidirectional(other.m_isBidirectional), m_typeA(other.m_typeA), m_typeB(other.m_typeB), m_aToB(other.m_aToB), m_bToA(other.m_bToA)
	{
		AddTags(&other);
	}

	void Caster::AddCastDefinition(const std::string &from, const std::string &to)
	{
		m_aToB[from] = to;
		if (m_isBidirectional)
			m_bToA[to] = from;
	}

	bool Caster::Cast(Unit *from, Unit *to)
	{
		if (from->m_type == m_typeA && to->m_type == m_typeB)
		{
			for (const auto &pair : m_aToB)
			{
				if (!Cast(from, pair.first, to, pair.second))
					return false;
			}
			to->m_null = from->m_null;
			return true;
		}
		else if (m_isBidirectional && from->m_type == m_typeB && to->m_type == m_typeA)
		{
			for (const auto &pair : m_bToA)
			{
				if (!Cast(from, pair.first, to, pair.second))
					return false;
			}
			to->m_null = from->m_null;
			return true;
		}
		return false;
	}

	bool Caster::Cast(Unit *from, const std::string &fromKey, Unit *to, const std::string &toKey)
	{
		if (Unit *valueToUse = GetUnitToUse(from, fromKey))
		{
			if (Unit *valueToSet = GetUnitToUse(to, toKey))
				return valueToSet->Set(valueToUse);
			OutputManager::Error(OutputType::RUNTIME, "Cannot cast ", from->m_type.GetName(), " into ", to->m_type.GetName(), ": Unknown subtype ", toKey);
			return false;
		}
		OutputManager::Error(OutputType::RUNTIME, "Cannot cast ", from->m_type.GetName(), " into ", to->m_type.GetName(), ": Unknown subtype ", fromKey);
		return false;
	}

	bool Caster::CanCast(const VariableType &from, const VariableType &to)
	{
		if ((from == m_typeA && to == m_typeB) || (m_isBidirectional && from == m_typeB && to == m_typeA))
			return true;
		return false;
	}

	Scale::Unit *Caster::GetUnitToUse(Unit *unit, const std::string &key)
	{
		size_t keySize = key.size();
		Unit *valueToUse = nullptr;
		if (keySize == 0)
			valueToUse = unit;
		else if (key[0] == '[' && key[keySize - 1] == ']')
		{
			size_t idx = static_cast<size_t>(std::stoull(key.substr(1, keySize - 2)));
			valueToUse = unit->Get(idx);
		}
		else
		{
			valueToUse = unit->Get(key);
		}
		return valueToUse;
	}

}