#pragma once

#include <string>

namespace Scale
{
	class ResolutionTree;
	class ResolutionTreeUtils final
	{
		friend class Function;
	private:
		static ResolutionTree *Compile(const std::string &);
	};
}