#include "CurrentEnvironment.h"

#include "ACaster.h"
#include "Native.h"
#include "OutputManager.h"
#include "Script.h"
#include "Unit.h"
#include "Variable.h"

namespace Scale
{
	CurrentEnvironment::CurrentEnvironment(Native *native) : m_native(native), m_script(nullptr) {}
	CurrentEnvironment::CurrentEnvironment(Script *script) : m_native(nullptr), m_script(script) {}

	DeclaredObjectDefinition *CurrentEnvironment::GetObjectDefinition(const std::string &name) const
	{
		if (m_script)
			return m_script->GetObjectDefinition(name, true, true);
		return m_native->GetObjectDefinition(name);
	}

	Variable CurrentEnvironment::GetGlobal(const std::string &name) const
	{
		if (m_script)
			return m_script->GetGlobal(name, true);
		return m_native->GetGlobalVariable(name);
	}

	Callable *CurrentEnvironment::GetCallable(const std::string &name) const
	{
		if (m_script)
			return m_script->GetCallable(name, true, true);
		return m_native->GetCallable(name);
	}

	Unit *CurrentEnvironment::CreateVar(const std::string &variableName, const std::string &variableTypeName, const std::string &variableValue, const std::vector<Tag> &tags) const
	{
		if (m_script)
			return m_script->CreateVar(OutputType::RUNTIME, variableName, variableTypeName, variableValue, true, tags);
		return m_native->CreateUnit(variableName, variableTypeName, variableValue, tags);
	}

	bool CurrentEnvironment::Cast(Unit *from, Unit *to) const
	{
		if (from == nullptr)
		{
			to->m_null = true;
			return true;
		}
		else if (to == nullptr)
		{
			OutputManager::Error(OutputType::RUNTIME, "Cannot cast: Receiver unit is null");
			return false;
		}
		if (m_script)
		{
			if (ACaster *caster = m_script->GetCaster(from->m_type, to->m_type, true, true))
				return caster->Cast(from, to);
		}
		else
		{
			if (ACaster *caster = m_native->GetCaster(from->m_type, to->m_type))
				return caster->Cast(from, to);
		}
		return false;
	}

	bool CurrentEnvironment::Cast(Variable &from, Variable &to) const
	{
		return Cast(from.m_unit, to.m_unit);
	}
}