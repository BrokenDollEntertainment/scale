#include "UnsignedShortTyper.h"

#include "../OutputManager.h"
#include "../Unit.h"

namespace Scale
{
	namespace UnitTyper
	{
		bool UnsignedShortTyper::Set(Unit *unit, const std::string &value, bool, const CurrentEnvironment &) const
		{
			try
			{
				unit->m_ushortValue = static_cast<unsigned short>(std::stoi(value));
				return true;
			}
			catch (const std::exception &)
			{
				OutputManager::Error(OutputType::RUNTIME, "Bad format value \"", value, "\": not an ushort");
				return false;
			}
		}

		bool UnsignedShortTyper::Set(Unit *unit, Unit *value, bool, const CurrentEnvironment &) const
		{
			unit->m_ushortValue = value->m_ushortValue;
			return true;
		}

		std::string UnsignedShortTyper::ToString(const Unit *unit) const
		{
			if (unit->m_null)
				return "null";
			return std::to_string(unit->m_ushortValue);
		}
	}
}