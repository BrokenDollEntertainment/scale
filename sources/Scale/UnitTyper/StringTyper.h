#pragma once

#include "Typer.h"

namespace Scale
{
	namespace UnitTyper
	{
		class StringTyper final : public Typer
		{
			using super = Typer;
		private:
			bool Set(Unit *, const std::string &, bool, const CurrentEnvironment &) const override;
			bool Set(Unit *, Unit *, bool, const CurrentEnvironment &) const override;
			std::string ToString(const Unit *) const override;

		public:
			StringTyper() = default;
			virtual ~StringTyper() = default;
		};
	}
}