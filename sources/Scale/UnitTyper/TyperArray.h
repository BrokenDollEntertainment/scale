#pragma once

#include <array>
#include "../EVariableType.h"
#include "Typer.h"

namespace Scale
{
	class Unit;
	namespace UnitTyper
	{
		class TyperArray
		{
			friend class Scale::Unit;
		private:
			std::array<Typer *, EVARIABLETYPE_SIZE> m_typers;

		private:
			bool Set(const EVariableType &, Unit *, const std::string &, bool, const CurrentEnvironment &) const;
			bool Set(const EVariableType &, Unit *, Unit *, bool, const CurrentEnvironment &) const;
			std::string ToString(const EVariableType &, const Unit *) const;

		public:
			TyperArray();
			~TyperArray();
		};
	}
}