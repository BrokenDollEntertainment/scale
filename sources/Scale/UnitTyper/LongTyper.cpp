#include "LongTyper.h"

#include "../OutputManager.h"
#include "../Unit.h"

namespace Scale
{
	namespace UnitTyper
	{
		bool LongTyper::Set(Unit *unit, const std::string &value, bool, const CurrentEnvironment &) const
		{
			try
			{
				unit->m_longValue = std::stol(value);
				return true;
			}
			catch (const std::exception &)
			{
				OutputManager::Error(OutputType::RUNTIME, "Bad format value \"", value, "\": not a long");
				return false;
			}
		}

		bool LongTyper::Set(Unit *unit, Unit *value, bool, const CurrentEnvironment &) const
		{
			unit->m_longValue = value->m_longValue;
			return true;
		}

		std::string LongTyper::ToString(const Unit *unit) const
		{
			if (unit->m_null)
				return "null";
			return std::to_string(unit->m_longValue);
		}
	}
}