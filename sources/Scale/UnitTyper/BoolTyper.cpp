#include "BoolTyper.h"

#include "../Unit.h"

namespace Scale
{
	namespace UnitTyper
	{
		bool BoolTyper::Set(Unit *unit, const std::string &value, bool, const CurrentEnvironment &) const
		{
			if (value == "true")
			{
				unit->m_boolValue = true;
				unit->m_null = false;
				return true;
			}
			else if (value == "false")
			{
				unit->m_boolValue = false;
				unit->m_null = false;
				return true;
			}
			return false;
		}

		bool BoolTyper::Set(Unit *unit, Unit *value, bool, const CurrentEnvironment &) const
		{
			unit->m_boolValue = value->m_boolValue;
			return true;
		}

		std::string BoolTyper::ToString(const Unit *unit) const
		{
			if (unit->m_null)
				return "null";
			return (unit->m_boolValue ? "true" : "false");
		}
	}
}