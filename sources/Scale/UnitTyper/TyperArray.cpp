#include "TyperArray.h"

#include "ArrayTyper.h"
#include "BoolTyper.h"
#include "CharTyper.h"
#include "DoubleTyper.h"
#include "FloatTyper.h"
#include "IntTyper.h"
#include "LongDoubleTyper.h"
#include "LongLongTyper.h"
#include "LongTyper.h"
#include "ObjectTyper.h"
#include "ShortTyper.h"
#include "StringTyper.h"
#include "UnsignedCharTyper.h"
#include "UnsignedIntTyper.h"
#include "UnsignedLongLongTyper.h"
#include "UnsignedLongTyper.h"
#include "UnsignedShortTyper.h"
#include "VoidTyper.h"
#include "../OutputManager.h"
#include "../Unit.h"

namespace Scale
{
	namespace UnitTyper
	{
		bool TyperArray::Set(const EVariableType &type, Unit *unit, const std::string &value, bool setSubType, const CurrentEnvironment &helper) const
		{
			if (type >= EVariableType::COUNT)
				return false;
			Typer *typer = m_typers[static_cast<int>(type)];
			if (typer)
			{
				if (typer->Set(unit, value, setSubType, helper))
				{
					unit->m_null = (value == "null");
					return true;
				}
			}
			unit->m_null = true;
			return false;
		}

		bool TyperArray::Set(const EVariableType &type, Unit *unit, Unit *value, bool setSubType, const CurrentEnvironment &helper) const
		{
			if (type >= EVariableType::COUNT)
				return false;
			Typer *typer = m_typers[static_cast<int>(type)];
			if (typer)
			{
				if (typer->Set(unit, value, setSubType, helper))
				{
					unit->m_null = value->m_null;
					unit->m_null = false;
					return true;
				}
			}
			unit->m_null = true;
			return false;
		}

		std::string TyperArray::ToString(const EVariableType &type, const Unit *unit) const
		{
			if (type >= EVariableType::COUNT)
				return "";
			Typer *typer = m_typers[static_cast<int>(type)];
			if (typer)
				return typer->ToString(unit);
			return "";
		}

		TyperArray::TyperArray()
		{
			m_typers[static_cast<int>(EVariableType::VOID)] = new VoidTyper();
			m_typers[static_cast<int>(EVariableType::GROUP)] = new ArrayTyper();
			m_typers[static_cast<int>(EVariableType::ARRAY)] = new ArrayTyper();
			m_typers[static_cast<int>(EVariableType::BOOL)] = new BoolTyper();
			m_typers[static_cast<int>(EVariableType::CHAR)] = new CharTyper();
			m_typers[static_cast<int>(EVariableType::UNSIGNED_CHAR)] = new UnsignedCharTyper();
			m_typers[static_cast<int>(EVariableType::SHORT)] = new ShortTyper();
			m_typers[static_cast<int>(EVariableType::UNSIGNED_SHORT)] = new UnsignedShortTyper();
			m_typers[static_cast<int>(EVariableType::INT)] = new IntTyper();
			m_typers[static_cast<int>(EVariableType::UNSIGNED_INT)] = new UnsignedIntTyper();
			m_typers[static_cast<int>(EVariableType::LONG)] = new LongTyper();
			m_typers[static_cast<int>(EVariableType::UNSIGNED_LONG)] = new UnsignedLongTyper();
			m_typers[static_cast<int>(EVariableType::LONG_LONG)] = new LongLongTyper();
			m_typers[static_cast<int>(EVariableType::UNSIGNED_LONG_LONG)] = new UnsignedLongLongTyper();
			m_typers[static_cast<int>(EVariableType::FLOAT)] = new FloatTyper();
			m_typers[static_cast<int>(EVariableType::DOUBLE)] = new DoubleTyper();
			m_typers[static_cast<int>(EVariableType::LONG_DOUBLE)] = new LongDoubleTyper();
			m_typers[static_cast<int>(EVariableType::STRING)] = new StringTyper();
			m_typers[static_cast<int>(EVariableType::OBJECT)] = new ObjectTyper();
		}

		TyperArray::~TyperArray()
		{
			for (Typer *typer : m_typers)
				delete(typer);
		}
	}
}