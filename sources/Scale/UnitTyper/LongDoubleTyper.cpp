#include "LongDoubleTyper.h"

#include "../OutputManager.h"
#include "../Unit.h"

namespace Scale
{
	namespace UnitTyper
	{
		bool LongDoubleTyper::Set(Unit *unit, const std::string &value, bool, const CurrentEnvironment &) const
		{
			try
			{
				unit->m_longDoubleValue = std::stold(value);
				return true;
			}
			catch (const std::exception &)
			{
				OutputManager::Error(OutputType::RUNTIME, "Bad format value \"", value, "\": not a ldouble");
				return false;
			}
		}

		bool LongDoubleTyper::Set(Unit *unit, Unit *value, bool, const CurrentEnvironment &) const
		{
			unit->m_longDoubleValue = value->m_longDoubleValue;
			return true;
		}

		std::string LongDoubleTyper::ToString(const Unit *unit) const
		{
			if (unit->m_null)
				return "null";
			return std::to_string(unit->m_longDoubleValue);
		}
	}
}