#include "UnsignedCharTyper.h"

#include "../OutputManager.h"
#include "../Unit.h"

namespace Scale
{
	namespace UnitTyper
	{
		bool UnsignedCharTyper::Set(Unit *unit, const std::string &value, bool, const CurrentEnvironment &) const
		{
			try
			{
				unit->m_ucharValue = static_cast<unsigned char>(std::stoi(value));
				return true;
			}
			catch (const std::exception &)
			{
				OutputManager::Error(OutputType::RUNTIME, "Bad format value \"", value, "\": not an uchar");
				return false;
			}
		}

		bool UnsignedCharTyper::Set(Unit *unit, Unit *value, bool, const CurrentEnvironment &) const
		{
			unit->m_ucharValue = value->m_ucharValue;
			return true;
		}

		std::string UnsignedCharTyper::ToString(const Unit *unit) const
		{
			if (unit->m_null)
				return "null";
			return std::to_string(static_cast<unsigned int>(unit->m_ucharValue));
		}
	}
}