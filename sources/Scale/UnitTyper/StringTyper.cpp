#include "StringTyper.h"

#include "../Unit.h"

namespace Scale
{
	namespace UnitTyper
	{
		bool StringTyper::Set(Unit *unit, const std::string &value, bool, const CurrentEnvironment &) const
		{
			size_t valueSize = value.size();
			if (value.find('"') == std::string::npos)
			{
				if (valueSize != 0)
				{
					unit->m_stringValue = value;
					return true;
				}
			}
			else if (value[0] == '"' && value[valueSize - 1] == '"')
			{
				if (valueSize >= 3)
				{
					unit->m_stringValue = value.substr(1, valueSize - 2);
					return true;
				}
			}
			return false;
		}

		bool StringTyper::Set(Unit *unit, Unit *value, bool, const CurrentEnvironment &) const
		{
			unit->m_stringValue = value->m_stringValue;
			return true;
		}

		std::string StringTyper::ToString(const Unit *unit) const
		{
			if (unit->m_null)
				return "null";
			return '"' + unit->m_stringValue + '"';
		}
	}
}