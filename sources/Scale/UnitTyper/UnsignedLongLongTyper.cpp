#include "UnsignedLongLongTyper.h"

#include "../OutputManager.h"
#include "../Unit.h"

namespace Scale
{
	namespace UnitTyper
	{
		bool UnsignedLongLongTyper::Set(Unit *unit, const std::string &value, bool, const CurrentEnvironment &) const
		{
			try
			{
				unit->m_ulongLongValue = std::stoull(value);
				return true;
			}
			catch (const std::exception &)
			{
				OutputManager::Error(OutputType::RUNTIME, "Bad format value \"", value, "\": not an ullong");
				return false;
			}
		}

		bool UnsignedLongLongTyper::Set(Unit *unit, Unit *value, bool, const CurrentEnvironment &) const
		{
			unit->m_ulongLongValue = value->m_ulongLongValue;
			return true;
		}

		std::string UnsignedLongLongTyper::ToString(const Unit *unit) const
		{
			if (unit->m_null)
				return "null";
			return std::to_string(unit->m_ulongLongValue);
		}
	}
}