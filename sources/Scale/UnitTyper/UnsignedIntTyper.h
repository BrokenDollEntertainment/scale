#pragma once

#include "Typer.h"

namespace Scale
{
	namespace UnitTyper
	{
		class UnsignedIntTyper final : public Typer
		{
			using super = Typer;
		private:
			bool Set(Unit *, const std::string &, bool, const CurrentEnvironment &) const override;
			bool Set(Unit *, Unit *, bool, const CurrentEnvironment &) const override;
			std::string ToString(const Unit *) const override;

		public:
			UnsignedIntTyper() = default;
			virtual ~UnsignedIntTyper() = default;
		};
	}
}