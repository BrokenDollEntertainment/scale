#include "UnsignedIntTyper.h"

#include "../OutputManager.h"
#include "../Unit.h"

namespace Scale
{
	namespace UnitTyper
	{
		bool UnsignedIntTyper::Set(Unit *unit, const std::string &value, bool, const CurrentEnvironment &) const
		{
			try
			{
				unit->m_uintValue = static_cast<unsigned int>(std::stoi(value));
				return true;
			}
			catch (const std::exception &)
			{
				OutputManager::Error(OutputType::RUNTIME, "Bad format value \"", value, "\": not an uint");
				return false;
			}
		}

		bool UnsignedIntTyper::Set(Unit *unit, Unit *value, bool, const CurrentEnvironment &) const
		{
			unit->m_uintValue = value->m_uintValue;
			return true;
		}

		std::string UnsignedIntTyper::ToString(const Unit *unit) const
		{
			if (unit->m_null)
				return "null";
			return std::to_string(unit->m_uintValue);
		}
	}
}