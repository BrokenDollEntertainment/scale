#include "LongLongTyper.h"

#include "../OutputManager.h"
#include "../Unit.h"

namespace Scale
{
	namespace UnitTyper
	{
		bool LongLongTyper::Set(Unit *unit, const std::string &value, bool, const CurrentEnvironment &) const
		{
			try
			{
				unit->m_longLongValue = std::stoll(value);
				return true;
			}
			catch (const std::exception &)
			{
				OutputManager::Error(OutputType::RUNTIME, "Bad format value \"", value, "\": not a llong");
				return false;
			}
		}

		bool LongLongTyper::Set(Unit *unit, Unit *value, bool, const CurrentEnvironment &) const
		{
			unit->m_longLongValue = value->m_longLongValue;
			return true;
		}

		std::string LongLongTyper::ToString(const Unit *unit) const
		{
			if (unit->m_null)
				return "null";
			return std::to_string(unit->m_longLongValue);
		}
	}
}