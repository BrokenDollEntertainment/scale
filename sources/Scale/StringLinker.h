#pragma once

#include "SubTypeLinker.h"

namespace Scale
{
	template <typename t_LinkerObjectType>
	class StringLinker final : SubTypeLinker<t_LinkerObjectType>
	{
		using super = SubTypeLinker<t_LinkerObjectType>;
		friend class Linker<t_LinkerObjectType>;
	private:
		const std::string &(t_LinkerObjectType::*m_getter)() const;
		void (t_LinkerObjectType::*m_setter)(const std::string &);
		std::string t_LinkerObjectType::*m_raw;

	private:
		StringLinker(const std::string &(t_LinkerObjectType::*getter)() const, void (t_LinkerObjectType::*setter)(const std::string &)) : SubTypeLinker<t_LinkerObjectType>(getter != nullptr && setter != nullptr), m_getter(getter), m_setter(setter), m_raw(nullptr) {}
		StringLinker(std::string t_LinkerObjectType::*raw) : SubTypeLinker<t_LinkerObjectType>(raw != nullptr), m_getter(nullptr), m_setter(nullptr), m_raw(raw) {}
		Variable Convert(const std::string &name, const t_LinkerObjectType &value, const CurrentEnvironment &) override
		{
			if (m_getter)
				return Variable(name, (value.*m_getter)());
			return Variable(name, value.*m_raw);
		}
		bool Fill(t_LinkerObjectType &value, const Variable &variable) override
		{
			if (variable.Is<std::string>())
			{
				if (m_setter)
					(value.*m_setter)(static_cast<std::string>(variable));
				else
					value.*m_raw = static_cast<std::string>(variable);
				return true;
			}
			return false;
		}
		std::string ToString(const t_LinkerObjectType &value) override
		{
			if (m_getter)
				return (value.*m_getter)();
			return '"' + value.*m_raw + '"';
		}
	};
}