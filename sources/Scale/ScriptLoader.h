#pragma once

#include "Script.h"

namespace Scale
{
	class ScriptLoader final
	{
		friend class DeclaredObjectDefinition;
		friend class Interpreter;
		friend class Script;
	private:
		std::unordered_map<std::string, Script *> m_loadedScript;

	private:
		static bool TreatTags(const std::string &, size_t &, std::vector<Tag> &);

	private:
		bool LoadFromContent(Script *, const std::string &, const std::string &, const std::string &);
		Script *Load(const std::string &, bool, Native *);
		Script *Get(const std::string &);
		Script *Load(const std::string &, Native *);
		bool Reload(const std::string &, Native *);

	public:
		ScriptLoader() = default;
		~ScriptLoader();
	};
}