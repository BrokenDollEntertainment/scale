#include "DeclaredObjectDefinition.h"

#include "CodingStyle.h"
#include "OutputManager.h"
#include "ScriptLoader.h"
#include "StringUtils.h"
#include "VariableDefinition.h"

namespace Scale
{

	void DeclaredObjectDefinition::AddDefinitionOf(DeclaredObjectDefinition *subTypeDefinition)
	{
		if (subTypeDefinition)
			m_subTypesDefinition[subTypeDefinition->m_typeName] = subTypeDefinition;
	}

	DeclaredObjectDefinition *DeclaredObjectDefinition::GetDefinitionOf(const std::string &type) const
	{
		auto it = m_subTypesDefinition.find(type);
		if (it == m_subTypesDefinition.end())
			return nullptr;
		return (*it).second;
	}

	bool DeclaredObjectDefinition::AddVariable(const std::string &name, const VariableType &type, const std::string &defaultValue, const std::vector<Tag> &tags)
	{
		if (!CodingStyle::CheckName(CodingStyle::NameType::VARIABLE, name))
			return false;
		if (type == EVariableType::OBJECT && type.GetName().size() != 0)
			m_subTypes.insert(type.GetName());
		m_variables.emplace_back(VariableDefinition{name, type, defaultValue, tags});
		return true;
	}
	bool DeclaredObjectDefinition::AddVariable(const std::string &name, const VariableType &type, const std::vector<Tag> &tags)
	{
		return AddVariable(name, type, "", tags);
	}

	DeclaredObjectDefinition *DeclaredObjectDefinition::StringToDefinition(const std::string &definition)
	{
		size_t openingBracketPos = definition.find('{');
		if (openingBracketPos == std::string::npos || definition[definition.size() - 1] != '}')
		{
			OutputManager::Error(OutputType::PARSING, "Invalid object definition \"", definition, "\": No object definition");
			return nullptr;
		}
		std::string typeName = definition.substr(0, openingBracketPos);
		StringUtils::Trim(typeName);
		if (typeName.size() == 0)
		{
			OutputManager::Error(OutputType::PARSING, "Invalid object definition \"", definition, "\": No object name");
			return nullptr;
		}

		if (!CodingStyle::CheckName(CodingStyle::NameType::DECALRED_OBJECT, typeName))
			return nullptr;
		std::string typeDefinition = definition.substr(openingBracketPos);
		size_t valueSize = typeDefinition.size();
		if (valueSize >= 3)
		{
			DeclaredObjectDefinition *newDeclaredObjectDefiniton = new DeclaredObjectDefinition();
			if (!newDeclaredObjectDefiniton)
			{
				OutputManager::Error(OutputType::PARSING, "Cannot create new object definition for ", typeName, ": Out of memory");
				return nullptr;
			}
			newDeclaredObjectDefiniton->m_typeName = typeName;
			std::vector<std::string> subValues;
			StringUtils::SmartSplit(subValues, typeDefinition.substr(1, valueSize - 2), ",", false);
			if (subValues.size() == 0)
			{
				OutputManager::Error(OutputType::PARSING, "Empty object definition");
				delete(newDeclaredObjectDefiniton);
				return nullptr;
			}
			OutputManager::Verbose(OutputType::PARSING, "Creating object definition for ", newDeclaredObjectDefiniton->m_typeName);
			for (const auto &line : subValues)
			{
				std::vector<Tag> tags;
				std::string subValue = line;
				while (StringUtils::StartWith(subValue, "["))
				{
					size_t tagSize = 1;
					if (!ScriptLoader::TreatTags(subValue, tagSize, tags))
						return false;
					subValue = subValue.substr(tagSize);
				}
				std::string subValueTypeAndName;
				std::string subValueDefaultValue = "";
				size_t equalOperatorPos = subValue.find('=');
				if (equalOperatorPos != std::string::npos)
				{
					subValueTypeAndName = subValue.substr(0, equalOperatorPos);
					subValueDefaultValue = subValue.substr(equalOperatorPos + 1);
				}
				else
					subValueTypeAndName = subValue;
				StringUtils::Trim(subValueTypeAndName);
				StringUtils::Trim(subValueDefaultValue);
				std::vector<std::string> subValueInfo = StringUtils::Split(subValueTypeAndName, " ", true, false);
				if (subValueInfo.size() == 2)
				{
					std::string subValueTypeName = subValueInfo[0];
					std::string subValueName = subValueInfo[1];
					if (!newDeclaredObjectDefiniton->AddVariable(subValueName, VariableType(subValueTypeName), subValueDefaultValue, tags))
					{
						delete(newDeclaredObjectDefiniton);
						return nullptr;
					}
					if (subValueDefaultValue.size() == 0)
						OutputManager::Verbose(OutputType::PARSING, "    Adding ", subValueTypeName, ' ', subValueName, " to object definition of ", newDeclaredObjectDefiniton->m_typeName, " without default value");
					else
						OutputManager::Verbose(OutputType::PARSING, "    Adding ", subValueTypeName, ' ', subValueName, " to object definition of ", newDeclaredObjectDefiniton->m_typeName, " with default value: ", subValueDefaultValue);
				}
			}
			return newDeclaredObjectDefiniton;
		}
		else
		{
			OutputManager::Error(OutputType::PARSING, "Invalid object definition \"", definition, "\": No definition inside {}");
			return nullptr;
		}
	}
}