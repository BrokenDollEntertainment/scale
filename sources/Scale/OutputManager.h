#pragma once

#include <array>
#include <functional>
#include <sstream>
#include <vector>

namespace Scale
{
	enum class OutputType: char
	{
		INITIALISATION,
		PARSING,
		COMPILATION,
		RUNTIME,
		OUTPUT,
		COUNT,
		INVALID
	};
	class OutputManager final
	{
		friend class Callable;
		friend class DeclaredObjectDefinition;
		friend class Function;
		friend class Native;
		friend class NativeUtils;
		friend class Parameters;
		friend class ResolutionTree;
		friend class Script;
		friend class ScriptLoader;
		friend class Signature;
		friend class Unit;
	private:
		static constexpr unsigned int NB_OUTPUT_CANAL = static_cast<unsigned int>(OutputType::COUNT);
		class Output
		{
		private:
			std::vector<std::function<void(OutputType, const std::string)>> m_listeners;
		public:
			Output() = default;
			inline void AddListener(const std::function<void(OutputType, const std::string)> &listener) {m_listeners.emplace_back(listener);}
			void Print(OutputType, const std::string &);
		};

	private:
		static bool ms_isVerbose;
		static std::array<Output, NB_OUTPUT_CANAL> ms_outputs;
		static std::array<Output, NB_OUTPUT_CANAL> ms_errors;

	private:
		template <typename t_ElementToPrintType>
		static inline void ToStream(std::ostream &os, t_ElementToPrintType elem) {os << elem;}

		static void Init();
		template <typename ...t_Arguments>
		static void Verbose(OutputType output, t_Arguments &&...args)
		{
			if (ms_isVerbose)
				Print(output, std::forward<t_Arguments>(args)...);
		}

	public:
		static void AddOutputListener(OutputType, void (*)(OutputType, const std::string &));
		static void AddErrorListener(OutputType, void (*)(OutputType, const std::string &));
		template <typename ...t_Arguments>
		static void Error(OutputType output, t_Arguments &&...args)
		{
			if (output < OutputType::COUNT)
			{
				std::stringstream stream;
				(ToStream<t_Arguments>(stream, args), ...);
				ms_errors[static_cast<char>(output)].Print(output, stream.str());
			}
		}

		template <typename ...t_Arguments>
		static void Print(OutputType output, t_Arguments &&...args)
		{
			if (output < OutputType::COUNT)
			{
				std::stringstream stream;
				(ToStream<t_Arguments>(stream, args), ...);
				ms_outputs[static_cast<char>(output)].Print(output, stream.str());
			}
		}
	};
	template <> inline void OutputManager::ToStream<bool>(std::ostream &os, bool elem) {if (elem) os << "true"; else os << "false";}
}