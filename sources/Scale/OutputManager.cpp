#include "OutputManager.h"

#include <cstdarg>
#include <iostream>
#include "Parameters.h"

namespace Scale
{
	static void formatted_output(std::ostream &os, OutputType output, const std::string &str)
	{
		switch (output)
		{
		case OutputType::INITIALISATION:
		{
			os << "INITIALISATION: ";
			break;
		}
		case OutputType::PARSING:
		{
			os << "PARSING: ";
			break;
		}
		case OutputType::COMPILATION:
		{
			os << "COMPILATION: ";
			break;
		}
		case OutputType::RUNTIME:
		{
			os << "RUNTIME: ";
			break;
		}
		case OutputType::OUTPUT:
		case OutputType::COUNT:
		case OutputType::INVALID:
			break;
		}
		os << str << std::endl;
	}

	static void standard_output(OutputType output, const std::string &str)
	{
		formatted_output(std::cout, output, str);
	}

	static void error_output(OutputType output, const std::string &str)
	{
		std::cerr << "ERROR: ";
		formatted_output(std::cerr, output, str);
	}

	bool OutputManager::ms_isVerbose = false;
	std::array<OutputManager::Output, OutputManager::NB_OUTPUT_CANAL> OutputManager::ms_outputs;
	std::array<OutputManager::Output, OutputManager::NB_OUTPUT_CANAL> OutputManager::ms_errors;

	void OutputManager::Init()
	{
		ms_isVerbose = Parameters::Is("DoVerbose", true);
		if (Parameters::Is("DoConsoleOutput", true))
		{
			for (auto &output : ms_outputs)
				output.AddListener(&standard_output);
			for (auto &error : ms_errors)
				error.AddListener(&error_output);
		}
	}

	void OutputManager::Output::Print(OutputType output, const std::string &str)
	{
		for (auto &listener : m_listeners)
			listener(output, str);
	}

	void OutputManager::AddOutputListener(OutputType output, void (*listener)(OutputType, const std::string &))
	{
		if (output != OutputType::COUNT && output != OutputType::INVALID)
			ms_outputs[static_cast<char>(output)].AddListener(listener);
	}

	void OutputManager::AddErrorListener(OutputType output, void (*listener)(OutputType, const std::string &))
	{
		if (output != OutputType::COUNT && output != OutputType::INVALID)
			ms_errors[static_cast<char>(output)].AddListener(listener);
	}
}