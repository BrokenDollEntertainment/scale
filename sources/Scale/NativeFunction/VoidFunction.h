#pragma once

#include "../Callable.h"

namespace Scale
{
	class NativeUtils;
	namespace NativeFunction
	{
		class VoidFunction final : public Scale::Callable
		{
			using super = Scale::Callable;
			friend class Scale::NativeUtils;
		private:
			void (*m_function)(CallStack &);

		private:
			VoidFunction(void (*)(CallStack &));
			bool Call(CallStack &) override;
			inline bool CheckSignature() override {return GetReturnType() == EVariableType::VOID;}
		};
	}
}