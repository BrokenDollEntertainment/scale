#pragma once

#include "../Callable.h"
#include "../CallStack.h"
#include "../Variable.h"

namespace Scale
{
	class NativeUtils;
	namespace NativeFunction
	{
		template <typename t_RawType>
		class RawFunction final : public Scale::Callable
		{
			using super = Scale::Callable;
			friend class Scale::NativeUtils;
		private:
			EVariableType m_expectedReturn;
			const t_RawType &(*m_constRefFunction)(CallStack &);
			t_RawType &(*m_refFunction)(CallStack &);
			t_RawType (*m_function)(CallStack &);

		private:
			RawFunction(EVariableType expectedReturn, const t_RawType &(*constRefFunction)(CallStack &)): m_expectedReturn(expectedReturn), m_constRefFunction(constRefFunction), m_refFunction(nullptr), m_function(nullptr) {}
			RawFunction(EVariableType expectedReturn, t_RawType &(*refFunction)(CallStack &)): m_expectedReturn(expectedReturn), m_constRefFunction(nullptr), m_refFunction(refFunction), m_function(nullptr) {}
			RawFunction(EVariableType expectedReturn, t_RawType (*function)(CallStack &)): m_expectedReturn(expectedReturn), m_constRefFunction(nullptr), m_refFunction(nullptr), m_function(function) {}
			bool Call(CallStack &stack) override
			{
				if (m_function)
				{
					stack.SetReturn(Variable("", m_function(stack)));
					return true;
				}
				else if (m_refFunction)
				{
					stack.SetReturn(Variable("", m_refFunction(stack)));
					return true;
				}
				else if (m_constRefFunction)
				{
					stack.SetReturn(Variable("", m_constRefFunction(stack)));
					return true;
				}
				return false;
			}
			inline bool CheckSignature() override {return GetReturnType() == m_expectedReturn;}
		};
	}
}