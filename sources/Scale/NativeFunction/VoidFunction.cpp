#include "VoidFunction.h"

namespace Scale
{
	namespace NativeFunction
	{
		VoidFunction::VoidFunction(void (*function)(CallStack &)) : m_function(function) {}

		bool VoidFunction::Call(CallStack &stack)
		{
			if (m_function)
			{
				m_function(stack);
				return true;
			}
			return false;
		}
	}
}