#pragma once

#include "../Callable.h"
#include "../CallStack.h"
#include "../LinkerWrapper.h"
#include "../NativeUtils.h"

namespace Scale
{
	class Native;
	class NativeUtils;
	namespace NativeFunction
	{
		template <typename t_DecalredObjectType>
		class DeclaredObjectFunction final : public Scale::Callable
		{
			using super = Scale::Callable;
			friend class Scale::Native;
			friend class Scale::NativeUtils;
		private:
			const CurrentEnvironment &m_helper;
			Native *m_native;
			LinkerWrapper<t_DecalredObjectType> *m_wrapper;
			const t_DecalredObjectType &(*m_constRefFunction)(CallStack &);
			t_DecalredObjectType &(*m_refFunction)(CallStack &);
			t_DecalredObjectType (*m_function)(CallStack &);

		private:
			DeclaredObjectFunction(const CurrentEnvironment &helper, Native *native, const t_DecalredObjectType &(*constRefFunction)(CallStack &)) : m_helper(helper), m_native(native), m_wrapper(nullptr), m_constRefFunction(constRefFunction), m_refFunction(nullptr), m_function(nullptr) {}
			DeclaredObjectFunction(const CurrentEnvironment &helper, Native *native, t_DecalredObjectType &(*refFunction)(CallStack &)) : m_helper(helper), m_native(native), m_wrapper(nullptr), m_constRefFunction(nullptr), m_refFunction(refFunction), m_function(nullptr) {}
			DeclaredObjectFunction(const CurrentEnvironment &helper, Native *native, t_DecalredObjectType (*function)(CallStack &)) : m_helper(helper), m_native(native), m_wrapper(nullptr), m_constRefFunction(nullptr), m_refFunction(nullptr), m_function(function) {}
			inline void SetLinkerWrapper(LinkerWrapper<t_DecalredObjectType> *wrapper) {m_wrapper = wrapper;}

			bool CheckSignature() override
			{
				if (std::is_same<Variable, t_DecalredObjectType>::value)
					return true;
				if (m_wrapper)
					return GetReturnType() == m_wrapper->GetTypeOf();
				return false;
			}

			bool Call(CallStack &stack) override
			{
				if (!m_wrapper)
					return false;
				if (m_function)
				{
					stack.SetReturn(m_wrapper->Convert("", m_function(stack), m_helper));
					return true;
				}
				else if (m_refFunction)
				{
					stack.SetReturn(m_wrapper->Convert("", m_refFunction(stack), m_helper));
					return true;
				}
				else if (m_constRefFunction)
				{
					stack.SetReturn(m_wrapper->Convert("", m_constRefFunction(stack), m_helper));
					return true;
				}
				return false;
			}
		};
	}
}