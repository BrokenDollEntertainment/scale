#include "Function.h"

#include "CurrentEnvironment.h"
#include "CallStack.h"
#include "OutputManager.h"
#include "ResolutionTree.h"
#include "ResolutionTreeUtils.h"
#include "Script.h"
#include "StringUtils.h"
#include "VariableType.h"

namespace Scale
{

	Function::Function(Script *script): m_script(script) {}

	bool Function::AddBody(const std::string &content)
	{
		if (content.size() == 0)
			return true;
		std::vector<std::string> fragments;
		StringUtils::SmartSplit(fragments, content, "}", true);
		std::vector<std::string> instructions;
		for (const auto &fragment : fragments)
			StringUtils::SmartSplit(instructions, fragment, ";", false);
		//#todo Implement body syntax check
		return true;
	}

	bool Function::Compile()
	{
		if (!super::Compile(CurrentEnvironment(m_script)))
			return false;
		for (const auto &instruction : m_uncompiledBody)
		{
			ResolutionTree *bodyNode = ResolutionTreeUtils::Compile(instruction);
			if (!bodyNode)
			{
				Clear();
				return false;
			}
			m_body.emplace_back(bodyNode);
		}
		return true;
	}

	bool Function::Call(CallStack &parameters)
	{
		for (auto *instruction : m_body)
		{
			if (!instruction->Resolve(m_script, parameters))
				return false;
		}
		return true;
	}

	void Function::Clear()
	{
		for (auto *instruction : m_body)
			delete(instruction);
	}

	Function::~Function()
	{
		Clear();
	}
}