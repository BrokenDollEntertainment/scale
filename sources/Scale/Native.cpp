#include "NativeUtils.h"

#include "Caster.h"
#include "CurrentEnvironment.h"
#include "DeclaredObjectDefinition.h"
#include "Function.h"
#include "NativeCaster.h"
#include "NativeStringCaster.h"
#include "OutputManager.h"
#include "Unit.h"


namespace Scale
{
	static bool cast_string_to_bool(const std::string &str)
	{
		if (str == "true")
			return true;
		else if (str == "false")
			return false;
		throw std::invalid_argument("String is neither true or false");
	}
	static char cast_string_to_char(const std::string &str) { return static_cast<char>(std::stoi(str)); }
	static unsigned char cast_string_to_uchar(const std::string &str) { return static_cast<unsigned char>(std::stoi(str)); }
	static short cast_string_to_short(const std::string &str) { return static_cast<short>(std::stoi(str)); }
	static unsigned short cast_string_to_ushort(const std::string &str) { return static_cast<unsigned short>(std::stoi(str)); }
	static int cast_string_to_int(const std::string &str) { return std::stoi(str); }
	static unsigned int cast_string_to_uint(const std::string &str) { return static_cast<unsigned int>(std::stoi(str)); }
	static long cast_string_to_long(const std::string &str) { return std::stol(str); }
	static unsigned long cast_string_to_ulong(const std::string &str) { return std::stoul(str); }
	static long long cast_string_to_llong(const std::string &str) { return std::stoll(str); }
	static unsigned long long cast_string_to_ullong(const std::string &str) { return std::stoull(str); }
	static float cast_string_to_float(const std::string &str) { return std::stof(str); }
	static double cast_string_to_double(const std::string &str) { return std::stod(str); }
	static long double cast_string_to_ldouble(const std::string &str) { return std::stold(str); }

	Native::Native(): m_helper(this)
	{
		m_nativeCasters[EVariableType::BOOL] = new NativeCaster<bool>(EVariableType::BOOL, &Unit::m_boolValue, &cast_string_to_bool);
		m_nativeCasters[EVariableType::CHAR] = new NativeCaster<char>(EVariableType::CHAR, &Unit::m_charValue, &cast_string_to_char);
		m_nativeCasters[EVariableType::UNSIGNED_CHAR] = new NativeCaster<unsigned char>(EVariableType::UNSIGNED_CHAR, &Unit::m_ucharValue, &cast_string_to_uchar);
		m_nativeCasters[EVariableType::SHORT] = new NativeCaster<short>(EVariableType::SHORT, &Unit::m_shortValue, &cast_string_to_short);
		m_nativeCasters[EVariableType::UNSIGNED_SHORT] = new NativeCaster<unsigned short>(EVariableType::UNSIGNED_SHORT, &Unit::m_ushortValue, &cast_string_to_ushort);
		m_nativeCasters[EVariableType::INT] = new NativeCaster<int>(EVariableType::INT, &Unit::m_intValue, &cast_string_to_int);
		m_nativeCasters[EVariableType::UNSIGNED_INT] = new NativeCaster<unsigned int>(EVariableType::UNSIGNED_INT, &Unit::m_uintValue, &cast_string_to_uint);
		m_nativeCasters[EVariableType::LONG] = new NativeCaster<long>(EVariableType::LONG, &Unit::m_longValue, &cast_string_to_long);
		m_nativeCasters[EVariableType::UNSIGNED_LONG] = new NativeCaster<unsigned long>(EVariableType::UNSIGNED_LONG, &Unit::m_ulongValue, &cast_string_to_ulong);
		m_nativeCasters[EVariableType::LONG_LONG] = new NativeCaster<long long>(EVariableType::LONG_LONG, &Unit::m_longLongValue, &cast_string_to_llong);
		m_nativeCasters[EVariableType::UNSIGNED_LONG_LONG] = new NativeCaster<unsigned long long>(EVariableType::UNSIGNED_LONG_LONG, &Unit::m_ulongLongValue, &cast_string_to_ullong);
		m_nativeCasters[EVariableType::FLOAT] = new NativeCaster<float>(EVariableType::FLOAT, &Unit::m_floatValue, &cast_string_to_float);
		m_nativeCasters[EVariableType::DOUBLE] = new NativeCaster<double>(EVariableType::DOUBLE, &Unit::m_doubleValue, &cast_string_to_double);
		m_nativeCasters[EVariableType::LONG_DOUBLE] = new NativeCaster<long double>(EVariableType::LONG_DOUBLE, &Unit::m_longDoubleValue, &cast_string_to_ldouble);
		m_nativeCasters[EVariableType::STRING] = new NativeStringCaster();
	}

	bool Native::AddObjectDefinition(DeclaredObjectDefinition *definition)
	{
		if (m_declaredObjectDefinition.find(definition->m_typeName) != m_declaredObjectDefinition.end())
			return false;
		m_declaredObjectDefinition[definition->m_typeName] = definition;
		return true;
	}

	DeclaredObjectDefinition *Native::GetObjectDefinition(const std::string &name)
	{
		auto it = m_declaredObjectDefinition.find(name);
		if (it != m_declaredObjectDefinition.end())
			return (*it).second;
		return nullptr;
	}

	bool Native::HaveObjectDefinition(const std::string &name)
	{
		if (m_declaredObjectDefinition.find(name) != m_declaredObjectDefinition.end())
			return true;
		return false;
	}

	Unit *Native::GetGlobal(const std::string &name)
	{
		auto it = m_globals.find(name);
		if (it != m_globals.end())
			return (*it).second;
		return nullptr;
	}

	Variable Native::GetGlobalVariable(const std::string &name)
	{
		Unit *unit = GetGlobal(name);
		if (unit)
			return Variable(unit, false);
		return Variable::NULL_VARIABLE;
	}

	bool Native::HaveGlobal(const std::string &name)
	{
		if (m_globals.find(name) != m_globals.end())
			return true;
		return false;
	}

	Callable *Native::GetCallable(const std::string &name)
	{
		auto functionIt = m_functions.find(name);
		if (functionIt != m_functions.end())
			return (*functionIt).second;
		auto uncompiledFunctionIt = m_unCompiledFunctions.find(name);
		if (uncompiledFunctionIt != m_unCompiledFunctions.end())
		{
			auto pair = (*uncompiledFunctionIt).second;
			if (m_serializers.find(pair.first) != m_serializers.end())
			{
				if (!pair.second->CheckSignature())
				{
					m_unCompiledFunctions.erase(uncompiledFunctionIt);
					delete(pair.second);
					return nullptr;
				}
				m_functions[(*uncompiledFunctionIt).first] = pair.second;
				m_unCompiledFunctions.erase(uncompiledFunctionIt);
				return pair.second;
			}
			OutputManager::Error(OutputType::RUNTIME, "Cannot build function ", name);
			m_unCompiledFunctions.erase(uncompiledFunctionIt);
			delete(pair.second);
		}
		return nullptr;
	}

	bool Native::HaveCallable(const std::string &name)
	{
		if (m_functions.find(name) != m_functions.end())
			return true;
		else if (m_unCompiledFunctions.find(name) != m_unCompiledFunctions.end())
			return true;
		return false;
	}

	bool Native::CheckType(const VariableType &type, bool output)
	{
		if (type == EVariableType::OBJECT)
		{
			std::string typeName = type.GetName();
			if (!HaveObjectDefinition(typeName))
			{
				if (output)
					OutputManager::Error(OutputType::RUNTIME, "No definition for type \"", typeName, '"');
				return false;
			}
			return true;
		}
		else if (type == EVariableType::ARRAY)
		{
			if (const VariableType *arrayType = type.GetArrayType())
				return CheckType(*arrayType, output);
			return true;
		}
		return true;
	}

	bool Native::CheckType(const std::string &typeName)
	{
		VariableType typeTocheck(typeName);
		return CheckType(typeTocheck, false);
	}

	Unit *Native::CreateUnit(const std::string &variableName, const std::string &variableTypeName, const std::string &variableValue, const std::vector<Tag> &tags)
	{
		Unit *newVariable;
		VariableType variableType = VariableType(variableTypeName);
		if (!CheckType(variableType, true))
			return nullptr;
		if (variableType == EVariableType::OBJECT)
		{
			DeclaredObjectDefinition *variableTypeDefinition = GetObjectDefinition(variableTypeName);
			OutputManager::Verbose(OutputType::RUNTIME, "Creating object ", variableName, " of type ", variableTypeName);
			if (variableName.size() == 0)
				newVariable = new Unit(variableTypeDefinition, "", m_helper);
			else
				newVariable = new Unit(variableName, variableTypeDefinition, "", m_helper);
		}
		else
		{
			OutputManager::Verbose(OutputType::RUNTIME, "Creating ", variableName, " of type ", variableType.GetName());
			if (variableName.size() == 0)
				newVariable = new Unit(variableType, "", m_helper);
			else
				newVariable = new Unit(variableName, variableType, "", m_helper);
		}
		if (!newVariable)
		{
			OutputManager::Error(OutputType::RUNTIME, "Cannot create new variable of type ", variableTypeName, ": Out of memory");
			return nullptr;
		}
		if (variableValue.size() != 0 && !newVariable->Set(variableValue))
		{
			delete(newVariable);
			return nullptr;
		}
		newVariable->AddTags(tags);
		return newVariable;
	}

	Variable Native::CreateVar(const std::string &variableName, const std::string &variableTypeName, const std::string &variableValue, const std::vector<Tag> &tags)
	{
		Unit *newVariable = CreateUnit(variableName, variableTypeName, variableValue, tags);
		if (!newVariable)
			return Variable::NULL_VARIABLE;
		return Variable(newVariable, true);
	}

	Native::~Native()
	{
		for (auto &pair : m_declaredObjectDefinition)
			delete(pair.second);
		for (auto &pair : m_functions)
			delete(pair.second);
		for (auto &pair : m_globals)
			delete(pair.second);
		for (auto &pair : m_serializers)
			delete(pair.second);
		for (auto &pair : m_nativeCasters)
			delete(pair.second);
		for (auto &pair : m_casters)
		{
			for (auto *caster : pair.second)
				delete(caster);
		}
	}

	bool Native::AddCallable(Callable *callable)
	{
		std::string functionName = callable->GetName();
		if (m_functions.find(functionName) != m_functions.end() || m_unCompiledFunctions.find(functionName) != m_unCompiledFunctions.end())
		{
			OutputManager::Error(OutputType::INITIALISATION, functionName, " already exist");
			delete(callable);
			return false;
		}
		if (!callable->Compile(m_helper))
		{
			delete(callable);
			return false;
		}
		OutputManager::Verbose(OutputType::INITIALISATION, "Adding callable ", functionName);
		m_functions[functionName] = callable;
		return true;
	}

	bool Native::AddCallable(size_t hash, Callable *callable)
	{
		std::string functionName = callable->GetName();
		if (m_functions.find(functionName) != m_functions.end() || m_unCompiledFunctions.find(functionName) != m_unCompiledFunctions.end())
		{
			OutputManager::Error(OutputType::INITIALISATION, functionName, " already exist");
			delete(callable);
			return false;
		}
		OutputManager::Verbose(OutputType::INITIALISATION, "Adding callable ", functionName);
		m_unCompiledFunctions[functionName] = std::pair(hash, callable);
		return true;
	}

	bool Native::AddCaster(Caster *caster)
	{
		if (!caster)
			return false;
		VariableType typeA = caster->m_typeA;
		VariableType typeB = caster->m_typeB;
		if (!CheckType(typeA, true) || !CheckType(typeB, true))
			return false;
		if (m_casters.find(typeA) == m_casters.end())
			m_casters[typeA] = std::vector<Caster *>();
		m_casters.at(typeA).emplace_back(caster);
		if (caster->m_isBidirectional)
		{
			if (m_casters.find(typeB) == m_casters.end())
				m_casters[typeB] = std::vector<Caster *>();
			m_casters.at(typeB).emplace_back(caster);
		}
		return true;
	}

	ACaster *Native::GetCaster(const VariableType &from, const VariableType &to)
	{
		auto nativeIt = m_nativeCasters.find(from.GetType());
		if (nativeIt != m_nativeCasters.end())
		{
			if (ACaster *caster = (*nativeIt).second)
			{
				if (caster->CanCast(from, to))
					return caster;
			}
		}
		auto it = m_casters.find(from);
		if (it == m_casters.end())
			return nullptr;
		std::vector<Caster *> casters = (*it).second;
		for (Caster *caster : casters)
		{
			if (caster->CanCast(from, to))
				return caster;
		}
		return nullptr;
	}

	bool Native::CanCast(const VariableType &from, const VariableType &to)
	{
		auto nativeIt = m_nativeCasters.find(from.GetType());
		if (nativeIt != m_nativeCasters.end())
		{
			if (ACaster *caster = (*nativeIt).second)
			{
				if (caster->CanCast(from, to))
					return true;
			}
		}
		auto it = m_casters.find(from);
		if (it == m_casters.end())
			return false;
		std::vector<Caster *> casters = (*it).second;
		for (Caster *caster : casters)
		{
			if (caster->CanCast(from, to))
				return true;
		}
		return false;
	}
}