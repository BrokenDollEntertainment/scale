﻿#include "Script.h"

#include "Caster.h"
#include "CodingStyle.h"
#include "CurrentEnvironment.h"
#include "DeclaredObjectDefinition.h"
#include "Function.h"
#include "OutputManager.h"
#include "ScriptLoader.h"
#include "StringUtils.h"
#include "Variable.h"

namespace Scale
{
	Script::TreeLock::TreeLock() : m_locks()
	{
		for (int n = 0; n != SCRIPT_INNER_CALL_SIZE; ++n)
			m_locks[n] = false;
	}

	bool Script::TreeLock::Lock(ScriptInnerCall call)
	{
		if (m_locks[static_cast<int>(call)])
			return false;
		m_locks[static_cast<int>(call)] = true;
		return true;
	}

	void Script::TreeLock::Unlock(ScriptInnerCall call)
	{
		m_locks[static_cast<int>(call)] = false;
	}

	Script::Script(Native *native, const std::string &scriptName) : m_helper(this), m_scriptName(scriptName), m_compiled(false), m_native(native) {}

	DeclaredObjectDefinition *Script::GetObjectDefinition(const std::string &name, bool searchInPrivate, bool searchInNative)
	{
		return Get<DeclaredObjectDefinition>(name, searchInPrivate, true, searchInNative, &Native::GetObjectDefinition, &Script::GetObjectDefinition, ScriptInnerCall::GetObjectDefinition, m_pr_declaredObjectDefinition, m_declaredObjectDefinition);
	}

	bool Script::HaveObjectDefinition(const std::string &name, bool searchInPrivate, bool searchInNative)
	{
		return Have<DeclaredObjectDefinition>(name, searchInPrivate, true, searchInNative, &Native::HaveObjectDefinition, &Script::HaveObjectDefinition, ScriptInnerCall::HaveObjectDefinition, m_pr_declaredObjectDefinition, m_declaredObjectDefinition);
	}

	Unit *Script::GetGlobal(const std::string &name, bool searchInPrivate, bool searchInNative)
	{
		return Get<Unit>(name, searchInPrivate, true, searchInNative, &Native::GetGlobal, &Script::GetGlobal, ScriptInnerCall::GetGlobal, m_pr_globals, m_globals);
	}

	bool Script::HaveGlobal(const std::string &name, bool searchInPrivate, bool searchInNative)
	{
		return Have<Unit>(name, searchInPrivate, true, searchInNative, &Native::HaveGlobal, &Script::HaveGlobal, ScriptInnerCall::HaveGlobal, m_pr_globals, m_globals);
	}

	Callable *Script::GetCallable(const std::string &name, bool searchInPrivate, bool searchInNative)
	{
		return Get<Callable>(name, searchInPrivate, true, searchInNative, &Native::GetCallable, &Script::GetCallable, ScriptInnerCall::GetCallable, m_pr_functions, m_functions);
	}

	bool Script::HaveCallable(const std::string &name, bool searchInPrivate, bool searchInNative)
	{
		return Have<Callable>(name, searchInPrivate, true, searchInNative, &Native::HaveCallable, &Script::HaveCallable, ScriptInnerCall::HaveCallable, m_pr_functions, m_functions);
	}

	ACaster *Script::GetCaster(const VariableType &from, const VariableType &to, bool searchInPrivate, bool searchNative)
	{
		return GetCaster(from, to, searchInPrivate, true, searchNative);
	}

	ACaster *Script::GetCaster(const VariableType &from, const VariableType &to, bool searchInPrivate, bool searchSelf, bool searchNative)
	{
		TreeLocker locker(&m_treeLock, ScriptInnerCall::GetCaster);
		if (!locker.Lock())
			return nullptr;
		if (searchNative)
		{
			auto *value = m_native->GetCaster(from, to);
			if (value)
				return value;
		}
		if (searchSelf)
		{
			if (searchInPrivate)
			{
				auto privateMapIt = m_pr_casters.find(from);
				if (privateMapIt != m_pr_casters.end())
				{
					std::vector<Caster *> casters = (*privateMapIt).second;
					for (Caster *caster : casters)
					{
						if (caster->CanCast(from, to))
							return caster;
					}
				}
			}
			auto publicMapIt = m_casters.find(from);
			if (publicMapIt != m_casters.end())
			{
				std::vector<Caster *> casters = (*publicMapIt).second;
				for (Caster *caster : casters)
				{
					if (caster->CanCast(from, to))
						return caster;
				}
			}
		}
		if (searchInPrivate)
		{
			for (Script *privateImports : m_allImports)
			{
				auto *value = privateImports->GetCaster(from, to, false, false);
				if (value)
					return value;
			}
		}
		else
		{
			for (Script *publicImport : m_imports)
			{
				auto *value = publicImport->GetCaster(from, to, false, false);
				if (value)
					return value;
			}
		}
		return nullptr;
	}

	bool Script::HaveCaster(const VariableType &from, const VariableType &to, bool searchInPrivate, bool searchNative)
	{
		return HaveCaster(from, to, searchInPrivate, true, searchNative);
	}

	bool Script::HaveCaster(const VariableType &from, const VariableType &to, bool searchInPrivate, bool searchSelf, bool searchNative)
	{
		TreeLocker locker(&m_treeLock, ScriptInnerCall::HaveCaster);
		if (!locker.Lock())
			return false;
		if (searchNative && m_native->CanCast(from, to))
			return true;
		if (searchSelf)
		{
			if (searchInPrivate)
			{
				auto privateMapIt = m_pr_casters.find(from);
				if (privateMapIt != m_pr_casters.end())
				{
					std::vector<Caster *> casters = (*privateMapIt).second;
					for (Caster *caster : casters)
					{
						if (caster->CanCast(from, to))
							return true;
					}
				}
			}
			auto publicMapIt = m_casters.find(from);
			if (publicMapIt != m_casters.end())
			{
				std::vector<Caster *> casters = (*publicMapIt).second;
				for (Caster *caster : casters)
				{
					if (caster->CanCast(from, to))
						return true;
				}
			}
		}
		if (searchInPrivate)
		{
			for (Script *privateImports : m_allImports)
			{
				if (privateImports->HaveCaster(from, to, false, false))
					return true;
			}
		}
		else
		{
			for (Script *publicImport : m_imports)
			{
				if (publicImport->HaveCaster(from, to, false, false))
					return true;
			}
		}
		return false;
	}

	Variable Script::GetGlobal(const std::string &name, bool searchInNative)
	{
		if (!m_compiled)
			return Variable::NULL_VARIABLE;
		return Variable(GetGlobal(name, true, searchInNative), false);
	}

	Unit *Script::CreateUnit(OutputType output, const std::string &variableName, const std::string &variableTypeName, const std::string &variableValue, bool searchInNative, const std::vector<Tag> &tags)
	{
		Unit *newVariable;
		VariableType variableType = VariableType(variableTypeName);
		if (variableType == EVariableType::OBJECT)
		{
			DeclaredObjectDefinition *variableTypeDefinition = GetObjectDefinition(variableTypeName, true, searchInNative);
			if (variableTypeDefinition)
			{
				OutputManager::Verbose(output, "Creating object ", variableName, " of type ", variableTypeName);
				if (variableName.size() == 0)
					newVariable = new Unit(variableTypeDefinition, "", m_helper);
				else
					newVariable = new Unit(variableName, variableTypeDefinition, "", m_helper);
			}
			else
			{
				OutputManager::Error(output, "In ", m_scriptName, ": No definition for type \"", variableTypeName, '"');
				return nullptr;
			}
		}
		else
		{
			OutputManager::Verbose(output, "Creating ", variableName, " of type ", variableType.GetName());
			if (variableName.size() == 0)
				newVariable = new Unit(variableType, "", m_helper);
			else
				newVariable = new Unit(variableName, variableType, "", m_helper);
		}
		if (!newVariable)
		{
			OutputManager::Error(output, "Cannot create new variable: Out of memory");
			return nullptr;
		}
		if (variableValue.size() != 0 && !newVariable->Set(variableValue))
		{
			delete(newVariable);
			return nullptr;
		}
		newVariable->AddTags(tags);
		return newVariable;
	}

	Unit *Script::CreateVar(OutputType output, const std::string &variableName, const std::string &variableTypeName, const std::string &variableValue, bool searchInNative, const std::vector<Tag> &tags)
	{
		if (!CodingStyle::CheckName(CodingStyle::NameType::VARIABLE, variableName))
			return nullptr;
		return CreateUnit(output, variableName, variableTypeName, variableValue, searchInNative, tags);
	}

	Variable Script::CreateVar(const std::string &variableName, const std::string &variableType, const std::string &variableValue, bool searchInNative, const std::vector<Tag> &tags)
	{
		if (!m_compiled)
			return Variable::NULL_VARIABLE;
		Unit *newUnit = CreateVar(OutputType::RUNTIME, variableName, variableType, variableValue, searchInNative, tags);
		if (!newUnit)
			return Variable::NULL_VARIABLE;
		return Variable(newUnit, true);
	}
	Variable Script::CreateVar(const std::string &variableName, const std::string &variableType, bool searchIntNative, const std::vector<Tag> &tags)
	{
		return CreateVar(variableName, variableType, "", searchIntNative, tags);
	}

	void Script::Clear()
	{
		m_pr_imports.clear();
		m_imports.clear();
		for (auto &pair : m_pr_declaredObjectDefinition)
			delete(pair.second);
		m_pr_declaredObjectDefinition.clear();
		for (auto &pair : m_declaredObjectDefinition)
			delete(pair.second);
		m_declaredObjectDefinition.clear();
		for (auto &pair : m_pr_functions)
			delete(pair.second);
		m_pr_functions.clear();
		for (auto &pair : m_functions)
			delete(pair.second);
		m_functions.clear();
		for (auto &pair : m_pr_globals)
			delete(pair.second);
		m_pr_globals.clear();
		for (auto &pair : m_globals)
			delete(pair.second);
		m_globals.clear();
		for (auto &pair : m_pr_casters)
		{
			for (auto *caster : pair.second)
				delete(caster);
		}
		m_pr_casters.clear();
		for (auto &pair : m_casters)
		{
			for (auto *caster : pair.second)
				delete(caster);
		}
		m_casters.clear();
	}

	Script::~Script() {Clear();}

	//==================================================PARSING==================================================//
	bool Script::AddGlobal(const std::string &global, const std::vector<Tag> &tags)
	{
		std::string globalTypeAndName;
		std::string globalValue = "";
		size_t equalOperatorPos = global.find('=');
		if (equalOperatorPos != std::string::npos)
		{
			globalTypeAndName = global.substr(0, equalOperatorPos);
			globalValue = global.substr(equalOperatorPos + 1);
		}
		else
			globalTypeAndName = global;
		StringUtils::Trim(globalTypeAndName);
		StringUtils::Trim(globalValue);
		std::vector<std::string> globalInfo = StringUtils::Split(globalTypeAndName, " ", true, false);
		if (globalInfo.size() == 2)
		{
			bool isPrivate = false;
			std::string globalType = globalInfo[0];
			std::string globalName = globalInfo[1];
			if (globalName.size() == 1 && globalName[0] == '!')
			{
				OutputManager::Error(OutputType::PARSING, "In ", m_scriptName, ": Invalid global \"", globalTypeAndName, "\": No name to global");
				return false;
			}
			if (globalName[0] == '!')
			{
				isPrivate = true;
				globalName = globalName.substr(1);
			}
			if (!CodingStyle::CheckName(CodingStyle::NameType::GLOBAL, globalName))
				return false;
			if (m_registeredGlobalToCompile.find(globalName) != m_registeredGlobalToCompile.end())
			{
				OutputManager::Error(OutputType::PARSING, "In ", m_scriptName, ": Global ", globalName, " already exist");
				return false;
			}
			m_registeredGlobalToCompile.insert(globalName);
			if (globalValue.size() == 0)
			{
				m_globalToCompile.emplace_back(GlobalDefinition{globalName, globalType, globalValue, isPrivate, tags});
				OutputManager::Verbose(OutputType::PARSING, globalType, ' ', globalName, " added without default value but not compiled");
				return true;
			}
			else
			{
				std::vector<std::string> tmp;
				StringUtils::SmartSplit(tmp, globalValue, ",", false);
				m_globalToCompile.emplace_back(GlobalDefinition{globalName, globalType, globalValue, isPrivate, tags});
				OutputManager::Verbose(OutputType::PARSING, globalType, ' ', globalName, " added with default value: ", globalValue, " but not compiled");
				return true;
			}
		}
		OutputManager::Error(OutputType::PARSING, "In ", m_scriptName, ": Invalid global \"", globalTypeAndName, "\": No type or name");
		return false;
	}

	bool Script::AddImport(const std::string &import, const std::string &scriptPath, const std::vector<Tag> &tags, ScriptLoader *scriptLoader)
	{
		bool isPrivate = false;
		std::string importName = import.substr(6);
		StringUtils::Trim(importName);
		size_t importNameSize = importName.size();
		if (importNameSize == 0 || (importNameSize == 1 && importName[0] == '!'))
		{
			OutputManager::Error(OutputType::PARSING, "In ", m_scriptName, ": Invalid import \"", import, "\": No import");
			return false;
		}
		if (importName[0] == '!')
		{
			isPrivate = true;
			importName = importName.substr(1);
		}
		if (m_registeredImportToCompile.find(importName) != m_registeredImportToCompile.end())
		{
			OutputManager::Error(OutputType::PARSING, "In ", m_scriptName, ": ", importName, " already imported");
			return false;
		}
		m_registeredImportToCompile.insert(importName);
		Script *importToAdd = scriptLoader->Load(scriptPath + importName, false, m_native);
		if (!importToAdd)
			return false;
		importToAdd->AddTags(tags);
		if (isPrivate)
		{
			OutputManager::Verbose(OutputType::PARSING, "Private import ", importName, " added");
			m_pr_imports.emplace_back(importToAdd);
		}
		else
		{
			OutputManager::Verbose(OutputType::PARSING, "Public import ", importName, " added");
			m_imports.emplace_back(importToAdd);
		}
		m_allImports.emplace_back(importToAdd);
		return true;
	}

	bool Script::AddDeclaredObject(const std::string &declaration, const std::vector<Tag> &tags)
	{
		bool isPrivate = false;
		std::string typeDefinition = declaration.substr(7);
		StringUtils::Trim(typeDefinition);
		size_t typeDefinitionSize = typeDefinition.size();
		if (typeDefinitionSize == 0 || (typeDefinitionSize == 1 && typeDefinition[0] == '!'))
		{
			OutputManager::Error(OutputType::PARSING, "In ", m_scriptName, ": Invalid declaration \"", declaration, "\": No declaration");
			return false;
		}
		if (typeDefinition[0] == '!')
		{
			isPrivate = true;
			typeDefinition = typeDefinition.substr(1);
		}
		if (m_registeredDefinitionToCompile.find(typeDefinition) != m_registeredDefinitionToCompile.end())
			return false;
		DeclaredObjectDefinition *declaredObjectDefinition = DeclaredObjectDefinition::StringToDefinition(typeDefinition);
		if (!declaredObjectDefinition)
			return false;
		declaredObjectDefinition->AddTags(tags);
		m_registeredDefinitionToCompile.insert(typeDefinition);
		if (isPrivate)
		{
			OutputManager::Verbose(OutputType::PARSING, "Private object of type ", declaredObjectDefinition->m_typeName, " defined but not compiled yet");
			m_pr_declaredObjectDefinition[declaredObjectDefinition->m_typeName] = declaredObjectDefinition;
		}
		else
		{
			OutputManager::Verbose(OutputType::PARSING, "Public object of type ", declaredObjectDefinition->m_typeName, " defined but not compiled yet");
			m_declaredObjectDefinition[declaredObjectDefinition->m_typeName] = declaredObjectDefinition;
		}
		return true;
	}

	bool Script::AddFunction(const std::string &content, const std::vector<Tag> &tags)
	{
		size_t contentSize = content.size();
		size_t openBracketPos = content.find('{');
		std::string signatureStr = content.substr(0, openBracketPos);
		std::string body = content.substr(openBracketPos + 1, (contentSize - 2 - signatureStr.size()));
		StringUtils::Trim(signatureStr);
		StringUtils::Trim(body);
		if (signatureStr.size() == 0)
		{
			OutputManager::Error(OutputType::PARSING, "In ", m_scriptName, ": Invalid function \"", content, "\": No function signature");
			return false;
		}
		Function *newFunction = new Function(this);
		if (!newFunction)
		{
			OutputManager::Error(OutputType::PARSING, "Cannot create new function: Out of memory");
			return false;
		}
		newFunction->AddTags(tags);
		bool isPrivate = false;
		if (!newFunction->SetSignature(signatureStr, isPrivate) || !newFunction->AddBody(body))
		{
			delete(newFunction);
			return false;
		}
		std::string functionName = newFunction->GetName();
		if (m_registeredFunctionToCompile.find(functionName) != m_registeredFunctionToCompile.end())
		{
			OutputManager::Error(OutputType::PARSING, "In ", m_scriptName, ": Function ", functionName, " already exist");
			delete(newFunction);
			return false;
		}
		if (m_pr_functions.find(functionName) != m_pr_functions.end())
		{
			OutputManager::Error(OutputType::PARSING, "In ", m_scriptName, ": Invalid function \"", content.substr(0, openBracketPos), "\": A private function with the same same already exist");
			delete(newFunction);
			return false;
		}
		if (m_functions.find(functionName) != m_functions.end())
		{
			OutputManager::Error(OutputType::PARSING, "In ", m_scriptName, ": Invalid function \"", content.substr(0, openBracketPos), "\": A public function with the same same already exist");
			delete(newFunction);
			return false;
		}
		if (isPrivate)
		{
			OutputManager::Verbose(OutputType::PARSING, "Private function ", functionName, " defined but not compiled yet");
			m_pr_functions[functionName] = newFunction;
		}
		else
		{
			OutputManager::Verbose(OutputType::PARSING, "Public function ", functionName, " defined but not compiled yet");
			m_functions[functionName] = newFunction;
		}
		m_registeredFunctionToCompile.insert(functionName);
		return true;
	}

	bool Script::AddCast(const std::string &content, const std::vector<Tag> &tags, bool isPrivate)
	{
		std::string castDefinition = content;
		StringUtils::Trim(castDefinition);
		//#todo Treat caster definition
		//#todo Add tags to caster
		return true;
	}

	bool Script::AddPublicCast(const std::string &content, const std::vector<Tag> &tags)
	{
		return AddCast(content.substr(4), tags, false);
	}

	bool Script::AddPrivateCast(const std::string &content, const std::vector<Tag> &tags)
	{
		return AddCast(content.substr(5), tags, true);
	}

	//==================================================COMPILATION==================================================//
	std::string Script::CheckDuplicateObjectDefinition(std::unordered_set<std::string> &alreadyDeclared)
	{
		return CheckDuplicate<DeclaredObjectDefinition>(alreadyDeclared, &Script::CheckDuplicateObjectDefinition, ScriptInnerCall::CheckDuplicateObjectDefinition, m_declaredObjectDefinition);
	}

	std::string Script::CheckDuplicateGlobal(std::unordered_set<std::string> &alreadyDeclared)
	{
		return CheckDuplicate<Unit>(alreadyDeclared, &Script::CheckDuplicateGlobal, ScriptInnerCall::CheckDuplicateGlobal, m_globals);
	}

	std::string Script::CheckDuplicateCallable(std::unordered_set<std::string> &alreadyDeclared)
	{
		return CheckDuplicate<Callable>(alreadyDeclared, &Script::CheckDuplicateCallable, ScriptInnerCall::CheckDuplicateCallable, m_functions);
	}

	std::string Script::CheckDuplicateCaster(std::unordered_map<VariableType, std::unordered_set<VariableType, VariableTypeHasher>, VariableTypeHasher> &alreadyDeclared)
	{
		TreeLocker locker(&m_treeLock, ScriptInnerCall::CheckDuplicateCaster);
		if (!locker.Lock())
			return "";
		for (const auto &pair : m_casters)
		{
			if (alreadyDeclared.find(pair.first) != alreadyDeclared.end())
			{
				std::unordered_set<VariableType, VariableTypeHasher> &intoSet = alreadyDeclared[pair.first];
				for (Caster *caster : pair.second)
				{
					if (caster->m_typeA == pair.first)
					{
						if (intoSet.find(caster->m_typeB) != intoSet.end())
							return pair.first.GetName() + " to " + caster->m_typeB.GetName();
						intoSet.insert(caster->m_typeB);
					}
					else
					{
						if (intoSet.find(caster->m_typeA) != intoSet.end())
							return pair.first.GetName() + " to " + caster->m_typeA.GetName();
						intoSet.insert(caster->m_typeA);
					}
				}
			}
			else
			{
				alreadyDeclared[pair.first] = std::unordered_set<VariableType, VariableTypeHasher>();
				std::unordered_set<VariableType, VariableTypeHasher> &intoSet = alreadyDeclared[pair.first];
				for (Caster *caster : pair.second)
				{
					if (caster->m_typeA == pair.first)
						intoSet.insert(caster->m_typeB);
					else
						intoSet.insert(caster->m_typeA);
				}
			}
		}
		for (Script *import : m_imports)
		{
			const std::string &duplicateFound = import->CheckDuplicateCaster(alreadyDeclared);
			if (duplicateFound.size() != 0)
				return duplicateFound;
		}
		return "";
	}

	bool Script::CompileImport(Script *import, std::unordered_set<std::string> &declaredObjectSet, std::unordered_set<std::string> &globalSet, std::unordered_set<std::string> &functionSet, std::unordered_map<VariableType, std::unordered_set<VariableType, VariableTypeHasher>, VariableTypeHasher> &casterSet)
	{
		if (!import->Compile())
			return false;
		OutputManager::Verbose(OutputType::COMPILATION, "Checking duplicate in private import ", import->m_scriptName);
		const std::string &duplicateDeclaredObjectFound = import->CheckDuplicateObjectDefinition(globalSet);
		if (duplicateDeclaredObjectFound.size() != 0)
		{
			OutputManager::Error(OutputType::COMPILATION, "In ", m_scriptName, ": Duplicate declared object \"", duplicateDeclaredObjectFound, "\" from import ", import->m_scriptName);
			return false;
		}
		const std::string &duplicateGlobalFound = import->CheckDuplicateGlobal(globalSet);
		if (duplicateGlobalFound.size() != 0)
		{
			OutputManager::Error(OutputType::COMPILATION, "In ", m_scriptName, ": Duplicate global \"", duplicateGlobalFound, "\" from import ", import->m_scriptName);
			return false;
		}
		const std::string &duplicateFunctionFound = import->CheckDuplicateObjectDefinition(functionSet);
		if (duplicateFunctionFound.size() != 0)
		{
			OutputManager::Error(OutputType::COMPILATION, "In ", m_scriptName, ": Duplicate function \"", duplicateFunctionFound, "\" from import ", import->m_scriptName);
			return false;
		}
		const std::string &duplicateCasterFound = import->CheckDuplicateCaster(casterSet);
		if (duplicateFunctionFound.size() != 0)
		{
			OutputManager::Error(OutputType::COMPILATION, "In ", m_scriptName, ": Duplicate caster \"", duplicateCasterFound, "\" from import ", import->m_scriptName);
			return false;
		}
		return true;
	}

	bool Script::CompileImports()
	{
		m_registeredImportToCompile.clear();
		bool ret = true;
		std::unordered_set<std::string> declaredObjectSet, globalSet, functionSet;
		std::unordered_map<VariableType, std::unordered_set<VariableType, VariableTypeHasher>, VariableTypeHasher> casterSet;
		for (auto it = m_allImports.begin(); it != m_allImports.end();)
		{
			std::unordered_set<std::string> tmpDeclaredObjectSet = declaredObjectSet;
			std::unordered_set<std::string> tmpGlobalSet = globalSet;
			std::unordered_set<std::string> tmpFunctionSet = functionSet;
			std::unordered_map<VariableType, std::unordered_set<VariableType, VariableTypeHasher>, VariableTypeHasher> tmpCasterSet = casterSet;
			if (!CompileImport(*it, declaredObjectSet, globalSet, functionSet, casterSet))
			{
				delete(*it);
				it = m_allImports.erase(it);
				declaredObjectSet = tmpDeclaredObjectSet;
				globalSet = tmpGlobalSet;
				functionSet = tmpFunctionSet;
				casterSet = tmpCasterSet;
				ret = false;
			}
			else
				++it;
		}
		return ret;
	}

	bool Script::CompileDeclaredObjectDefinition(DeclaredObjectDefinition *declaredObject)
	{
		const std::string &typeName = declaredObject->m_typeName;
		if (Get<DeclaredObjectDefinition>(typeName, true, false, true, &Native::GetObjectDefinition, &Script::GetObjectDefinition, ScriptInnerCall::GetObjectDefinition, m_pr_declaredObjectDefinition, m_declaredObjectDefinition))
		{
			OutputManager::Error(OutputType::COMPILATION, "In ", m_scriptName, ": Cannot compile type \"", typeName, "\": A public type with the same name already exist");
			return false;
		}
		else
		{
			const auto &subTypes = declaredObject->m_subTypes;
			for (const auto &subType : subTypes)
			{
				DeclaredObjectDefinition *subTypeDefinition = GetObjectDefinition(subType, true, true);
				if (subTypeDefinition)
					declaredObject->AddDefinitionOf(subTypeDefinition);
				else
				{
					OutputManager::Error(OutputType::COMPILATION, "In ", m_scriptName, ": Cannot compile type \"", declaredObject->m_typeName, "\": Unknown type ", subType);
					return false;
				}
			}
			return true;
		}
	}

	bool Script::CompileDeclaredObjectDefinitions()
	{
		m_registeredDefinitionToCompile.clear();
		bool ret = true;
		for (auto it = m_pr_declaredObjectDefinition.begin(); it != m_pr_declaredObjectDefinition.end();)
		{
			if (!CompileDeclaredObjectDefinition((*it).second))
			{
				delete((*it).second);
				it = m_pr_declaredObjectDefinition.erase(it);
				ret = false;
			}
			else
				++it;
		}
		for (auto it = m_declaredObjectDefinition.begin(); it != m_declaredObjectDefinition.end();)
		{
			if (!CompileDeclaredObjectDefinition((*it).second))
			{
				delete((*it).second);
				it = m_declaredObjectDefinition.erase(it);
				ret = false;
			}
			else
				++it;
		}
		return ret;
	}

	bool Script::CompileGlobals()
	{
		m_registeredGlobalToCompile.clear();
		for (const auto &globalToCompile : m_globalToCompile)
		{
			if (Get<Unit>(globalToCompile.name, true, false, true, &Native::GetGlobal, &Script::GetGlobal, ScriptInnerCall::GetGlobal, m_pr_globals, m_globals))
			{
				OutputManager::Error(OutputType::COMPILATION, "In ", m_scriptName, ": Cannot compile global \"", globalToCompile.name, "\": A public global with the same name already exist in imported script");
				return false;
			}
			else
			{
				Unit *newVariable = CreateUnit(OutputType::COMPILATION, globalToCompile.name, globalToCompile.typeName, globalToCompile.value, true, globalToCompile.tags);
				if (!newVariable)
					return false;
				if (globalToCompile.isPrivate)
					m_pr_globals[globalToCompile.name] = newVariable;
				else
					m_globals[globalToCompile.name] = newVariable;
			}
		}
		return true;
	}

	bool Script::CompileFunction(Function *function)
	{
		std::string functionName = function->GetName();
		if (Get<Callable>(functionName, true, false, true, &Native::GetCallable, &Script::GetCallable, ScriptInnerCall::GetCallable, m_pr_functions, m_functions))
		{
			OutputManager::Error(OutputType::COMPILATION, "In ", m_scriptName, ": Cannot compile function \"", functionName, "\": A public function with the same name already exist");
			return false;
		}
		else
			return function->Compile();
	}

	bool Script::CompileFunctions()
	{
		m_registeredFunctionToCompile.clear();
		bool ret = true;
		for (auto it = m_pr_functions.begin(); it != m_pr_functions.end();)
		{
			if (!CompileFunction(static_cast<Function *>((*it).second)))
			{
				delete((*it).second);
				it = m_pr_functions.erase(it);
				ret = false;
			}
			else
				++it;
		}
		for (auto it = m_functions.begin(); it != m_functions.end();)
		{
			if (!CompileFunction(static_cast<Function *>((*it).second)))
			{
				delete((*it).second);
				it = m_functions.erase(it);
				ret = false;
			}
			else
				++it;
		}
		return ret;
	}

	bool Script::CheckType(const VariableType &type)
	{
		if (type == EVariableType::OBJECT)
		{
			std::string typeName = type.GetName();
			if (!HaveObjectDefinition(typeName, true, true))
			{
				OutputManager::Error(OutputType::RUNTIME, "No definition for type \"", typeName, '"');
				return false;
			}
			return true;
		}
		else if (type == EVariableType::ARRAY)
		{
			if (const VariableType *arrayType = type.GetArrayType())
				return CheckType(*arrayType);
			return true;
		}
		return true;
	}

	bool Script::CheckType(const VariableType &type, bool output, bool searchNative)
	{
		if (type == EVariableType::OBJECT)
		{
			std::string typeName = type.GetName();
			if (!HaveObjectDefinition(typeName, true, searchNative))
			{
				if (output)
					OutputManager::Error(OutputType::RUNTIME, "No definition for type \"", typeName, '"');
				return false;
			}
			return true;
		}
		else if (type == EVariableType::ARRAY)
		{
			if (const VariableType *arrayType = type.GetArrayType())
				return CheckType(*arrayType, output, searchNative);
			return true;
		}
		return true;
	}

	bool Script::CheckType(const std::string &typeName, bool searchNative)
	{
		VariableType typeTocheck(typeName);
		return CheckType(typeTocheck, false, searchNative);
	}

	bool Script::CompileCaster(Caster *caster)
	{
		return CheckType(caster->m_typeA) && CheckType(caster->m_typeB);
	}

	bool Script::CompileCasters()
	{
		m_registeredCasterToCompile.clear();
		bool ret = true;
		for (auto it = m_pr_casters.begin(); it != m_pr_casters.end();)
		{
			std::vector<Caster *> &casters = m_pr_casters.at((*it).first);
			for (auto casterIt = casters.begin(); casterIt != casters.end();)
			{
				if (!CompileCaster(*casterIt))
				{
					delete(*casterIt);
					casterIt = casters.erase(casterIt);
					ret = false;
				}
				else
					++casterIt;
			}
			if (!casters.size() == 0)
				it = m_pr_casters.erase(it);
			else
				++it;
		}
		for (auto it = m_casters.begin(); it != m_casters.end();)
		{
			std::vector<Caster *> &casters = m_casters.at((*it).first);
			for (auto casterIt = casters.begin(); casterIt != casters.end();)
			{
				if (!CompileCaster(*casterIt))
				{
					delete(*casterIt);
					casterIt = casters.erase(casterIt);
					ret = false;
				}
				else
					++casterIt;
			}
			if (!casters.size() == 0)
				it = m_casters.erase(it);
			else
				++it;
		}
		return ret;
	}

	bool Script::Compile()
	{
		TreeLocker locker(&m_treeLock, ScriptInnerCall::Compile);
		if (!locker.Lock())
			return true;
		OutputManager::Verbose(OutputType::COMPILATION, "Compiling script ", m_scriptName);
		if (!CompileImports() ||
			!CompileDeclaredObjectDefinitions() ||
			!CompileGlobals() ||
			!CompileFunctions())
			return false;
		m_compiled = true;
		return true;
	}
}
