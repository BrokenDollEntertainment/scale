#pragma once

#include <string>
#include <vector>
#include "Tag.h"

namespace Scale
{
	class Callable;
	class DeclaredObjectDefinition;
	class Native;
	class Script;
	class Unit;
	class Variable;
	class CurrentEnvironment final
	{
	private:
		Native *m_native;
		Script *m_script;

	public:
		CurrentEnvironment(Native *);
		CurrentEnvironment(Script *);
		DeclaredObjectDefinition *GetObjectDefinition(const std::string &) const;
		Variable GetGlobal(const std::string &) const;
		Unit *CreateVar(const std::string &, const std::string &, const std::string &, const std::vector<Tag> &) const;
		Callable *GetCallable(const std::string &) const;
		bool Cast(Unit *, Unit *) const;
		bool Cast(Variable &, Variable &) const;
	};
}