#pragma once

#include <string>

namespace Scale
{
	class CodingStyle
	{
		friend class Parameters;
	public:
		enum class NameType : char
		{
			VARIABLE,
			GLOBAL,
			DECALRED_OBJECT,
			FUNCTION,
			COUNT,
			INVALID
		};

		enum class CasePreset : char
		{
			CAMEL_CASE, //camelCase
			PASCAL_CASE, //PascalCase
			SNAKE_CASE, //snake_case
			SCREAMING_SNAKE_CASE, //SCREAMING_SNAKE_CASE
			KEBAB_CASE, //kebab-case
			COUNT,
			INVALID
		};
	private:
		static CodingStyle ms_codingStyle;
		static bool ms_doCodingStyle;
		bool (*m_variableChecker)(const std::string &) { nullptr };
		bool (*m_globalChecker)(const std::string &) { nullptr };
		bool (*m_functionChecker)(const std::string &) { nullptr };
		bool (*m_declaredObjectChecker)(const std::string &) { nullptr };

	private:
		bool IsNameCompliant(const NameType &, const std::string &);
		bool SetChecker(const NameType &, bool (*)(const std::string &));
		static void Init();
		static void SetDefaultCodingStyle();
		static const char *ErrorNameOf(const NameType &);

	public:
		static bool SetCheckerFor(const NameType &, const CasePreset &);
		static bool SetCheckerFor(const NameType &, bool (*)(const std::string &));
		static bool CheckName(const NameType &, const std::string &);
	};
}