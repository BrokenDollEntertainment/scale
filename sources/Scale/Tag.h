#pragma once

#include <string>
#include <unordered_set>

namespace Scale
{
	struct Tag
	{
		std::string name;
		std::unordered_set<std::string> params;
	};
}