#pragma once

#include "VariableType.h"

namespace Scale
{
	class VariableTypeHelper
	{
	public:
		static bool IsRaw(const VariableType &);
		static bool IsRaw(const std::string &);
		static bool IsNotObject(const VariableType &);
		static bool IsNotObject(const std::string &);
	};
}