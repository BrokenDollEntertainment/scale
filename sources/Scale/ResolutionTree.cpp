#include "ResolutionTree.h"

#include "OutputManager.h"

namespace Scale
{
	ResolutionTree::ResolutionTree(IResolutionTreeOperation *operation) : m_hasResolve(true), m_operation(operation)
	{
		if (!m_operation)
			OutputManager::Verbose(OutputType::COMPILATION, "Cannot build resolution tree: Out of memory");
	}

	ResolutionTree::ResolutionTree(IResolutionTreeOperation *operation, const std::vector<ResolutionTree *> &childs) : m_hasResolve(true), m_operation(operation), m_childs(childs)
	{
		if (!m_operation)
			OutputManager::Verbose(OutputType::COMPILATION, "Cannot build resolution tree: Out of memory");
	}

	Variable ResolutionTree::ResolveChild(Script *script, CallStack &callstack)
	{
		m_hasResolve = true;
		if (!m_operation)
		{
			m_hasResolve = false;
			return Variable::NULL_VARIABLE;
		}
		bool operationResult = true;
		if (m_childs.size() == 0)
		{
			Variable ret = m_operation->Call(script, callstack, operationResult);
			m_hasResolve = operationResult;
			return ret;
		}
		std::vector<Variable> childResolutions;
		for (auto *child : m_childs)
		{
			childResolutions.emplace_back(child->ResolveChild(script, callstack));
			if (!child->m_hasResolve)
			{
				m_hasResolve = false;
				return Variable::NULL_VARIABLE;
			}
		}
		Variable ret = m_operation->Call(script, callstack, childResolutions, operationResult);
		m_hasResolve = operationResult;
		return ret;
	}

	bool ResolutionTree::Resolve(Script *script, CallStack &callstack)
	{
		if (!m_operation)
			return false;
		bool operationResult = true;
		if (m_childs.size() == 0)
		{
			m_operation->Call(script, callstack, operationResult);
			return operationResult;
		}
		std::vector<Variable> childResolutions;
		for (auto *child : m_childs)
		{
			childResolutions.emplace_back(child->ResolveChild(script, callstack));
			if (!child->m_hasResolve)
				return false;
		}
		m_operation->Call(script, callstack, childResolutions, operationResult);
		return operationResult;
	}

	ResolutionTree::~ResolutionTree()
	{
		if (m_operation)
			delete(m_operation);
		for (auto *child : m_childs)
			delete(child);
	}
}