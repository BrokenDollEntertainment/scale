#pragma once

#include "Variable.h"

namespace Scale
{
	template <typename t_LinkerType>
	class Linker;
	template <typename t_LinkerObjectType>
	class SubTypeLinker
	{
		friend class Linker<t_LinkerObjectType>;
	private:
		bool m_valid;

	protected:
		SubTypeLinker(bool valid) : m_valid(valid) {}

	private:
		virtual Variable Convert(const std::string &, const t_LinkerObjectType &, const CurrentEnvironment &) = 0;
		virtual bool Fill(t_LinkerObjectType &, const Variable &) = 0;
		virtual std::string ToString(const t_LinkerObjectType &) = 0;

	public:
		operator bool() const { return m_valid; }
		virtual ~SubTypeLinker() = default;
	};
}