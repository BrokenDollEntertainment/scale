#pragma once

#include "ILinkerWrapper.h"
#include "Linker.h"

namespace Scale
{
	template <typename t_DecalredObjectType>
	class DeclaredObjectFunction;
	template <typename t_LinkerType>
	class LinkerWrapper final : public ILinkerWrapper
	{
		using super = ILinkerWrapper;
		friend class DeclaredObjectFunction<t_LinkerType>;
		friend class NativeUtils;
	private:
		Linker<t_LinkerType> m_linker;

	public:
		LinkerWrapper(Linker<t_LinkerType> &linker) : m_linker(linker) {}
		inline bool Compile(Native *native) { return m_linker.Compile(native); }
		inline Variable Convert(const std::string &name, const t_LinkerType &value, const CurrentEnvironment &helper) { return m_linker.Convert(name, value, helper); }
		inline VariableType GetTypeOf() { return m_linker.GetTypeOf(); }
		inline bool Fill(const Variable &variable, t_LinkerType &value) { return m_linker.Fill(variable, value); }
	};
}