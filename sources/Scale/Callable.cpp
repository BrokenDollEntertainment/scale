#include "Callable.h"

#include "CodingStyle.h"
#include "CurrentEnvironment.h"
#include "CallStack.h"
#include "OutputManager.h"
#include "StringUtils.h"
#include "Variable.h"

namespace Scale
{
	bool Callable::operator()(CallStack &parameters)
	{
		return Call(parameters);
	}

	bool Callable::CheckParameter(const std::vector<Variable> &variables) const
	{
		const std::vector<Parameter> &parameters = m_signature.m_arguments;
		size_t parametersSize = parameters.size();
		size_t variableSize = variables.size();
		if (variableSize > parametersSize)
		{
			OutputManager::Error(OutputType::RUNTIME, "Too much argument given to ", m_signature.m_name);
			return false;
		}
		size_t idx = 0;
		for (const auto &parameter : parameters)
		{
			if (idx != variableSize)
			{
				const VariableType &variableType = variables[idx].GetVariableType();
				const VariableType &parameterType = parameter.GetType();
				if (variableType != parameterType)
				{
					OutputManager::Error(OutputType::RUNTIME, "Invalid parameter ", idx, ": Parameter is of type ", variableType.GetName(), " instead of ", parameterType.GetName());
					return false;
				}
				++idx;
			}
			else if (!parameter.HaveDefaultValue())
			{
				OutputManager::Error(OutputType::RUNTIME, "Not enough argument given to ", m_signature.m_name);
				return false;
			}
		}
		return true;
	}

	bool Callable::Compile(const CurrentEnvironment &helper)
	{
		if (!CheckSignature() || !m_signature.CompileParametersDefault(helper, m_defaultValues))
			return false;
		return true;
	}

	bool Callable::SetSignature(const std::string &signatureStr, bool &isPrivate)
	{
		std::string tmp = signatureStr;
		std::string signatureArgument;
		std::string signatureName;
		std::string signatureType;
		size_t parenthesesPos = tmp.find('(');
		if (parenthesesPos != std::string::npos)
		{
			size_t closingParenthesesPos = tmp.find(')');
			if (closingParenthesesPos == std::string::npos)
			{
				OutputManager::Error(OutputType::PARSING, "Invalid function signature \"", tmp, "\": Missing )");
				return false;
			}
			signatureArgument = tmp.substr(parenthesesPos + 1, (closingParenthesesPos - (parenthesesPos + 1)));
			tmp = tmp.substr(0, parenthesesPos);
		}
		std::vector<std::string> signatureInfo = StringUtils::Split(tmp, " ", true, false);
		switch (signatureInfo.size())
		{
		case 1:
		{
			signatureName = signatureInfo[0];
			break;
		}
		case 2:
		{
			signatureType = signatureInfo[0];
			signatureName = signatureInfo[1];
			break;
		}
		default:
		{
			OutputManager::Error(OutputType::PARSING, "Invalid function signature \"", tmp, "\": Function can only have one return type");
			return false;
		}
		}
		size_t signatureNameSize = signatureName.size();
		if (signatureNameSize == 0 || (signatureNameSize == 1 && signatureName[0] == '!'))
		{
			OutputManager::Error(OutputType::PARSING, "Invalid function signature \"", tmp, "\": Missing function name");
			return false;
		}
		if (signatureName[0] == '!')
		{
			signatureName = signatureName.substr(1);
			isPrivate = true;
		}
		if (!CodingStyle::CheckName(CodingStyle::NameType::FUNCTION, signatureName))
			return false;
		if (signatureType.size() != 0)
			m_signature.m_returnType = VariableType(signatureType);
		m_signature.m_name = signatureName;
		OutputManager::Verbose(OutputType::PARSING, "Adding function ", m_signature.m_name, " with return type ", m_signature.m_returnType.GetName());
		std::vector<std::string> signatureArgumentList = StringUtils::Split(signatureArgument, ",", true, false);
		for (const auto &argument : signatureArgumentList)
		{
			std::vector argumentInfo = StringUtils::Split(argument, " ", true);
			size_t argumentInfoSize = argumentInfo.size();
			if ((argumentInfoSize == 4 && argumentInfo[2] != "?") || (argumentInfoSize != 2 && argumentInfoSize != 4))
			{
				OutputManager::Error(OutputType::PARSING, "Invalid function signature \"", tmp, "\": Format error in parameter \"", argument, '"');
				return false;
			}
			bool argumentIsMutable = false;
			std::string argumentType = argumentInfo[0];
			std::string argumentName = argumentInfo[1];
			VariableType argumentVariableType(argumentType);
			if (argumentName.size() == 1 && argumentName[0] == '&')
			{
				OutputManager::Error(OutputType::PARSING, "Invalid function signature \"", tmp, "\": Parameter must have a name");
				return false;
			}
			if (argumentName[0] == '&')
			{
				argumentIsMutable = true;
				argumentName = argumentName.substr(1);
			}
			m_signature.AddArgument(argumentName, argumentVariableType, argumentIsMutable);
			if (argumentInfoSize == 4)
				m_defaultValues[argumentName] = argumentInfo[3];
			if (argumentIsMutable)
				OutputManager::Verbose(OutputType::PARSING, "    Adding mutable ", argumentType, ' ', argumentName, " to parameters list");
			else
				OutputManager::Verbose(OutputType::PARSING, "    Adding non-mutable ", argumentType, ' ', argumentName, " to parameters list");
		}
		return true;
	}
}