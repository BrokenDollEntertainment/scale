#include "Parameter.h"

#include "Unit.h"

namespace Scale
{
	Parameter::Parameter(const std::string &name, const VariableType &type, bool isMutable) : m_name(name), m_type(type), m_isMutable(isMutable), m_defaultValue(nullptr) {}

	Parameter::Parameter(const Parameter &other) : m_name(other.m_name), m_type(other.m_type), m_isMutable(other.m_isMutable), m_defaultValue(other.m_defaultValue) {}

	Parameter &Parameter::operator=(const Parameter &other)
	{
		m_name = other.m_name;
		m_type = other.m_type;
		m_isMutable = other.m_isMutable;
		m_defaultValue = other.m_defaultValue;
		return *this;
	}

	void Parameter::AssignDefaultValue(Unit *newDefaultValue)
	{
		m_defaultValue = newDefaultValue;
	}
}