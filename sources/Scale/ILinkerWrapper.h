#pragma once

namespace Scale
{
	class ILinkerWrapper
	{
	public:
		virtual ~ILinkerWrapper() = default;
	};
}