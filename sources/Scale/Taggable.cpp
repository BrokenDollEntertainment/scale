#include "Taggable.h"

#include <sstream>

namespace Scale
{
	bool Taggable::Match(const Tag &tag, EMatchParam matchParam) const
	{
		if (m_tags.find(tag.name) == m_tags.end())
			return false;
		switch (matchParam)
		{
		case Scale::Taggable::EMatchParam::MATCH_NAME_ONLY:
			return true;
		case Scale::Taggable::EMatchParam::MATCH_CONTAIN_PARAM:
		{
			const Tag &tagToCompare = m_tags.at(tag.name);
			const auto &compareEnd = tagToCompare.params.end();
			for (const std::string &paramToSearch : tag.params)
			{
				if (tagToCompare.params.find(paramToSearch) == compareEnd)
					return false;
			}
			return true;
		}
		case Scale::Taggable::EMatchParam::MATCH_FULL:
		{
			const Tag &tagToCompare = m_tags.at(tag.name);
			if (tagToCompare.params.size() != tag.params.size())
				return false;
			const auto &compareEnd = tagToCompare.params.end();
			for (const std::string &paramToSearch : tag.params)
			{
				if (tagToCompare.params.find(paramToSearch) == compareEnd)
					return false;
			}
			return true;
		}
		default:
			return false;
		}
	}

	bool Taggable::AddTag(const Tag &tag)
	{
		if (m_tags.find(tag.name) != m_tags.end())
		{
			Tag &tagToEdit = m_tags.at(tag.name);
			tagToEdit.params.insert(tag.params.begin(), tag.params.end());
			return true;
		}
		m_tags[tag.name] = tag;
		++m_nbTag;
		return true;
	}

	bool Taggable::AddTags(const std::vector<std::string> &tagsNames)
	{
		for (const std::string &tagName : tagsNames)
		{
			if (!AddTag({tagName}))
				return false;
		}
		return true;
	}

	bool Taggable::AddTags(const std::vector<Tag> &tags)
	{
		for (const Tag &tag : tags)
		{
			if (!AddTag(tag))
				return false;
		}
		return true;
	}

	bool Taggable::AddTags(const Taggable *taggable)
	{
		if (taggable)
		{
			for (const auto &pair : taggable->m_tags)
			{
				const Tag &tag = pair.second;
				if (!AddTag(tag))
					return false;
			}
			return true;
		}
		return false;
	}

	bool Taggable::RemoveTag(const std::string &tagName)
	{
		auto &it = m_tags.find(tagName);
		if (it == m_tags.end())
			return false;
		m_tags.erase(it);
		--m_nbTag;
		return true;
	}

	std::string Taggable::TagToString() const
	{
		std::stringstream stream;
		bool first = true;
		stream << '[';
		for (const auto &pair : m_tags)
		{
			const Tag &tag = pair.second;
			if (!first)
				stream << ", ";
			first = false;
			stream << tag.name;
			std::unordered_set<std::string> params = tag.params;
			if (params.size() != 0)
			{
				bool firstParam = true;
				stream << '(';
				for (const std::string &paramValue : params)
				{
					if (!firstParam)
						stream << ", ";
					firstParam = false;
					stream << paramValue;
				}
				stream << ')';
			}
		}
		stream << ']';
		return stream.str();
	}

	std::vector<Scale::Tag> Taggable::GetTagList() const
	{
		std::vector<Tag> tagList;
		for (const auto &pair : m_tags)
			tagList.emplace_back(pair.second);
		return tagList;
	}
}