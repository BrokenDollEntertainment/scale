#include "NativeStringCaster.h"

#include <string>
#include "Unit.h"
#include "VariableTypeHelper.h"

namespace Scale
{
	bool NativeStringCaster::CanCast(const VariableType &from, const VariableType &to)
	{
		return (to == EVariableType::STRING && VariableTypeHelper::IsRaw(from));
	}

	bool NativeStringCaster::Cast(Unit *from, Unit *to)
	{
		switch (from->m_type.GetType())
		{
		case EVariableType::VOID:
			break;
		case EVariableType::BOOL:
		{
			to->m_stringValue = (from->m_boolValue) ? "true" : "false";
			break;
		}
		case EVariableType::CHAR:
		{
			to->m_stringValue = std::to_string(from->m_charValue);
			break;
		}
		case EVariableType::UNSIGNED_CHAR:
		{
			to->m_stringValue = std::to_string(from->m_ucharValue);
			break;
		}
		case EVariableType::SHORT:
		{
			to->m_stringValue = std::to_string(from->m_shortValue);
			break;
		}
		case EVariableType::UNSIGNED_SHORT:
		{
			to->m_stringValue = std::to_string(from->m_ushortValue);
			break;
		}
		case EVariableType::INT:
		{
			to->m_stringValue = std::to_string(from->m_intValue);
			break;
		}
		case EVariableType::UNSIGNED_INT:
		{
			to->m_stringValue = std::to_string(from->m_uintValue);
			break;
		}
		case EVariableType::LONG:
		{
			to->m_stringValue = std::to_string(from->m_longValue);
			break;
		}
		case EVariableType::UNSIGNED_LONG:
		{
			to->m_stringValue = std::to_string(from->m_ulongValue);
			break;
		}
		case EVariableType::LONG_LONG:
		{
			to->m_stringValue = std::to_string(from->m_longLongValue);
			break;
		}
		case EVariableType::UNSIGNED_LONG_LONG:
		{
			to->m_stringValue = std::to_string(from->m_ulongLongValue);
			break;
		}
		case EVariableType::FLOAT:
		{
			to->m_stringValue = std::to_string(from->m_floatValue);
			break;
		}
		case EVariableType::DOUBLE:
		{
			to->m_stringValue = std::to_string(from->m_doubleValue);
			break;
		}
		case EVariableType::LONG_DOUBLE:
		{
			to->m_stringValue = std::to_string(from->m_longDoubleValue);
			break;
		}
		case EVariableType::STRING:
		{
			to->m_stringValue = from->m_stringValue;
		}
		case EVariableType::ARRAY:
		case EVariableType::GROUP:
		case EVariableType::OBJECT:
		case EVariableType::COUNT:
		case EVariableType::INVALID:
		default:
			return false;
		}
		to->m_null = false;
		return true;
	}
}