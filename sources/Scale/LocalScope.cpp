#include "LocalScope.h"

namespace Scale
{
	Variable LocalScope::Get(const std::string &name)
	{
		return m_locals.at(name).GetMutable();
	}

	void LocalScope::Add(const std::string &name, Variable &&variable)
	{
		m_locals[name] = variable;
	}
	bool LocalScope::Have(const std::string &name)
	{
		return m_locals.find(name) != m_locals.end();
	}
}