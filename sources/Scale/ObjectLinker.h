#pragma once

#include "SubTypeLinker.h"

namespace Scale
{
	template <typename t_LinkerObjectType, typename t_ObjectSubType>
	class ObjectLinker final : SubTypeLinker<t_LinkerObjectType>
	{
		using super = SubTypeLinker<t_LinkerObjectType>;
		friend class Linker<t_LinkerObjectType>;
	private:
		const t_ObjectSubType &(t_LinkerObjectType::*m_constGetter)() const;
		t_ObjectSubType &(t_LinkerObjectType::*m_getter)();
		t_ObjectSubType t_LinkerObjectType::*m_raw;
		const Linker<t_ObjectSubType> &m_subTypeObjectLinker;

	private:
		ObjectLinker(const t_ObjectSubType &(t_LinkerObjectType::*constGetter)() const, t_ObjectSubType &(t_LinkerObjectType::*getter)(), const Linker<t_ObjectSubType> &subTypeObjectLinker) : SubTypeLinker<t_LinkerObjectType>(constGetter != nullptr && getter != nullptr), m_constGetter(constGetter), m_getter(getter), m_raw(nullptr), m_subTypeObjectLinker(subTypeObjectLinker) {}
		ObjectLinker(t_ObjectSubType t_LinkerObjectType::*raw, const Linker<t_ObjectSubType> &subTypeObjectLinker) : SubTypeLinker<t_LinkerObjectType>(raw != nullptr), m_constGetter(nullptr), m_getter(nullptr), m_raw(raw), m_subTypeObjectLinker(subTypeObjectLinker) {}

		Variable Convert(const std::string &name, const t_LinkerObjectType &value, const CurrentEnvironment &helper) override
		{
			if (m_constGetter)
				return m_subTypeObjectLinker.Convert(name, (value.*m_constGetter)(), helper);
			return m_subTypeObjectLinker.Convert(name, value.*m_raw, helper);

		}

		bool Fill(t_LinkerObjectType &value, const Variable &variable) override
		{
			if (m_getter)
				return m_subTypeObjectLinker.Fill(variable, (value.*m_getter)());
			else
				return m_subTypeObjectLinker.Fill(variable, value.*m_raw);
		}

		std::string ToString(const t_LinkerObjectType &value) override
		{
			if (m_constGetter)
				return m_subTypeObjectLinker.ToString((value.*m_constGetter)());
			return m_subTypeObjectLinker.ToString(value.*m_raw);
		}
	};
}