#include "Parameters.h"

#include "CodingStyle.h"
#include "OutputManager.h"
#include "StringUtils.h"

namespace Scale
{
	bool Parameters::ms_isInit = false;
	Parameters Parameters::ms_parameters;

	Parameters::Parameters()
	{
		#ifndef DEBUG
			AddParameter("DoVerbose", false);
		#else
			AddParameter("DoVerbose", true);
		#endif
		AddParameter("DoSmartBuild", true);
		AddParameter("DoConsoleOutput", true);
		AddParameter("DoCodingStyle", false);
		AddParameter("DefaultCodingStyle", false);
		AddParameter("DisplayName", false);
		AddParameter("DisplayTag", false);
	}

	void Parameters::AddParameter(const std::string &name, bool value)
	{
		m_boolParameters[name] = value;
	}

	void Parameters::TreatArgumentValue(const std::string &name, const std::string &value)
	{
		if (m_boolParameters.find(name) != m_boolParameters.end())
		{
			if (value == "true")
				m_boolParameters[name] = true;
			else if (value == "false")
				m_boolParameters[name] = false;
		}
	}

	bool Parameters::CompareParameterTo(const std::string &name, bool value)
	{
		if (m_boolParameters.find(name) != m_boolParameters.end())
			return m_boolParameters[name] == value;
		return !value;
	}

	void Parameters::ParseArgs(const std::vector<std::string> &arguments)
	{
		if (!ms_isInit)
		{
			for (const auto &argument : arguments)
			{
				if (argument.size() > 2 && argument[0] == '-' && argument[1] == '-')
				{
					std::string rawArgument = argument.substr(2);
					std::string argumentName = "";
					std::string argumentValue = "";
					size_t separatorPos = rawArgument.find(':');
					if (separatorPos != std::string::npos)
					{
						argumentName = rawArgument.substr(0, separatorPos);
						argumentValue = rawArgument.substr(separatorPos + 1);
					}
					else
						argumentName = rawArgument;
					StringUtils::Trim(argumentName);
					StringUtils::Trim(argumentValue);
					ms_parameters.TreatArgumentValue(argumentName, argumentValue);
				}
			}
			ParseArgs();
		}
	}

	void Parameters::ParseArgs()
	{
		if (!ms_isInit)
		{
			OutputManager::Init();
			OutputManager::Verbose(OutputType::PARSING, "SmartBuild is ", Is("DoSmartBuild", true) ? "enable" : "disable");
			OutputManager::Verbose(OutputType::PARSING, "Console output is ", Is("DoConsoleOutput", true) ? "enable" : "disable");
			OutputManager::Verbose(OutputType::PARSING, "Coding style check is ", Is("DoCodingStyle", true) ? "enable" : "disable");
			OutputManager::Verbose(OutputType::PARSING, "Variable name in ToString is ", Is("DisplayName", true) ? "enable" : "disable");
			OutputManager::Verbose(OutputType::PARSING, "Arguments parsed");
			CodingStyle::Init();
			if (Is("DefaultCodingStyle", true))
				CodingStyle::SetDefaultCodingStyle();
			ms_isInit = true;
		}
	}
}