#pragma once

#include <string>
#include <unordered_map>
#include <vector>
#include "Variable.h"

namespace Scale
{
	namespace NativeFunction
	{
		template <typename t_DecalredObjectType>
		class DeclaredObjectFunction;
		template <typename t_RawType>
		class RawFunction;
		class VoidFunction;
	}
	class LocalScope;
	class Native;
	class Parameter;
	class Script;
	class CallStack final
	{
		friend class Function;
		friend class Interpreter;
		template <typename t_DecalredObjectType>
		friend class NativeFunction::DeclaredObjectFunction;
		template <typename t_RawType>
		friend class NativeFunction::RawFunction;
		friend class NativeFunction::VoidFunction;
	private:
		Native *m_native;
		Script *m_current;
		std::unordered_map<std::string, Variable> m_parameters;
		std::vector<LocalScope *> m_locals;
		Variable m_return;

	private:
		CallStack(Native *);
		CallStack(Native *, Script *);
		CallStack(Native *, const std::vector<Parameter> &, const std::vector<Variable> &);
		CallStack(Native *, Script *, const std::vector<Parameter> &, const std::vector<Variable> &);
		bool CreateVar(const std::string &, const std::string &, const std::string &, const std::vector<Tag> &);
		inline Variable GetReturn() {return m_return;}
		void SetReturn(Variable &&);
		bool OpenScope();
		void CloseScope();

	public:
		Variable operator[](const std::string &);
		~CallStack();
	};
}