#pragma once

#include <unordered_map>
#include "Tag.h"

namespace Scale
{
	class Taggable
	{
	private:
		enum class EMatchParam : int
		{
			MATCH_NAME_ONLY,
			MATCH_CONTAIN_PARAM,
			MATCH_FULL
		};
	private:
		size_t m_nbTag {0};
		std::unordered_map<std::string, Tag> m_tags;

	private:
		bool Match(const Tag &, EMatchParam) const;

	public:
		inline bool HaveTags() const { return m_nbTag != 0; }
		inline bool HaveTagWithName(const Tag &tag) const {return Match(tag, EMatchParam::MATCH_NAME_ONLY);}
		inline bool HaveTagWithName(const std::string &tagName) const {return Match(Tag{tagName}, EMatchParam::MATCH_NAME_ONLY);}
		inline bool HaveTagWithParam(const Tag &tag) const {return Match(tag, EMatchParam::MATCH_CONTAIN_PARAM);}
		inline bool HaveTagWithParam(const std::string &tagName, const std::unordered_set<std::string> &tagParams) const {return Match(Tag{tagName, tagParams}, EMatchParam::MATCH_CONTAIN_PARAM);}
		inline bool HaveTag(const Tag &tag) const {return Match(tag, EMatchParam::MATCH_FULL);}
		inline bool HaveTag(const std::string &tagName, const std::unordered_set<std::string> &tagParams) const {return Match(Tag{tagName, tagParams}, EMatchParam::MATCH_FULL);}
		inline const Tag &GetTag(const Tag &tag) const  {return m_tags.at(tag.name);}
		inline const Tag &GetTag(const std::string &tagName) const {return m_tags.at(tagName);}
		inline bool AddTag(const std::string &tagName) {return AddTag(Tag{tagName});}
		bool AddTag(const Tag &);
		bool AddTags(const std::vector<std::string> &);
		bool AddTags(const std::vector<Tag> &);
		bool AddTags(const Taggable *);
		inline bool RemoveTag(const Tag &tag) {return RemoveTag(tag.name);}
		bool RemoveTag(const std::string &);
		std::string TagToString() const;
		std::vector<Tag> GetTagList() const;
	};
}