#pragma once

#include <vector>
#include "Tag.h"
#include "VariableType.h"

namespace Scale
{
	struct VariableDefinition final
	{
		std::string m_name;
		VariableType m_type;
		std::string m_defaultValue;
		std::vector<Tag> m_tags;
	};
}