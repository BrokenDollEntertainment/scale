#pragma once

#include "DeclaredObjectDefinition.h"
#include "Taggable.h"
#include "UnitTyper/TyperArray.h"
#include "VariableDefinition.h"

namespace Scale
{
	namespace UnitTyper
	{
		class ArrayTyper;
		class BoolTyper;
		class CharTyper;
		class DoubleTyper;
		class FloatTyper;
		class GroupTyper;
		class IntTyper;
		class LongDoubleTyper;
		class LongLongTyper;
		class LongTyper;
		class ObjectTyper;
		class ShortTyper;
		class StringTyper;
		class TyperArray;
		class UnsignedCharTyper;
		class UnsignedIntTyper;
		class UnsignedLongLongTyper;
		class UnsignedLongTyper;
		class UnsignedShortTyper;
		class VoidTyper;
	}
	template <typename t_NativeTypeToCastTo>
	class NativeCaster;
    class Unit final: public Taggable
	{
		friend class UnitTyper::ArrayTyper;
		friend class UnitTyper::BoolTyper;
		friend class UnitTyper::CharTyper;
		friend class UnitTyper::DoubleTyper;
		friend class UnitTyper::FloatTyper;
		friend class UnitTyper::GroupTyper;
		friend class UnitTyper::IntTyper;
		friend class UnitTyper::LongDoubleTyper;
		friend class UnitTyper::LongLongTyper;
		friend class UnitTyper::LongTyper;
		friend class UnitTyper::ObjectTyper;
		friend class UnitTyper::ShortTyper;
		friend class UnitTyper::StringTyper;
		friend class UnitTyper::TyperArray;
		friend class UnitTyper::UnsignedCharTyper;
		friend class UnitTyper::UnsignedIntTyper;
		friend class UnitTyper::UnsignedLongLongTyper;
		friend class UnitTyper::UnsignedLongTyper;
		friend class UnitTyper::UnsignedShortTyper;
		friend class UnitTyper::VoidTyper;
		friend class Caster;
		friend class CurrentEnvironment;
		friend class Native;
		template <typename t_NativeTypeToCastTo>
		friend class NativeCaster;
		friend class NativeStringCaster;
		friend class Script;
		friend class Signature;
		friend class Variable;
	private:
		static UnitTyper::TyperArray m_typer;

	private:
		bool m_boolValue{false};
		char m_charValue{0};
		unsigned char m_ucharValue{ 0 };
		short m_shortValue{ 0 };
		unsigned short m_ushortValue{ 0 };
		int m_intValue{0};
		unsigned int m_uintValue{ 0 };
		long m_longValue{ 0 };
		unsigned long m_ulongValue{ 0 };
		long long m_longLongValue{ 0 };
		unsigned long long m_ulongLongValue{ 0 };
		float m_floatValue{0};
		double m_doubleValue{0};
		long double m_longDoubleValue{0};
		std::string m_stringValue{ "" };
		std::vector<Unit *> m_groupValue;

    private:
		bool m_error;
		bool m_build;
        bool m_null;
		std::string m_name;
		VariableType m_type;
        std::unordered_map<std::string, Unit *> m_child;
		const DeclaredObjectDefinition *m_definition;
		std::string m_default;
		const CurrentEnvironment &m_helper;

	private:
		Unit();
		Unit(Unit *);
		Unit(const std::string &, bool);
		Unit(const std::string &, char);
		Unit(const std::string &, unsigned char);
		Unit(const std::string &, short);
		Unit(const std::string &, unsigned short);
		Unit(const std::string &, int);
		Unit(const std::string &, unsigned int);
		Unit(const std::string &, long);
		Unit(const std::string &, unsigned long);
		Unit(const std::string &, long long);
		Unit(const std::string &, unsigned long long);
		Unit(const std::string &, float);
		Unit(const std::string &, double);
		Unit(const std::string &, long double);
		Unit(const std::string &, const std::string &);
		Unit(const std::string &, const std::vector<Unit *> &);
		Unit(const std::string &, const std::vector<Unit *> &, const VariableType &);
		Unit(const DeclaredObjectDefinition *, bool, const std::string &, const CurrentEnvironment &);
		Unit(const std::string &, const DeclaredObjectDefinition *, bool, const std::string &, const CurrentEnvironment &);
		Unit(const VariableType &, const std::string &, const CurrentEnvironment &);
		Unit(const DeclaredObjectDefinition *, const std::string &, const CurrentEnvironment &);
		Unit(const std::string &, const VariableType &, const std::string &, const CurrentEnvironment &);
		Unit(const std::string &, const DeclaredObjectDefinition *, const std::string &, const CurrentEnvironment &);
		void BuildType(const DeclaredObjectDefinition *);
		void Build();
		template <typename t_TypeToCompareTo>
		bool Is() const { return false; }
		std::string ToString() const;
		std::string ToRawString() const;
		bool Set(const std::string &, bool);
		bool Set(Unit *, bool);
		bool Set(const std::string &);
		bool Set(Unit *);
		bool Set(const std::string &, const std::string &);
		bool Set(const std::string &, Unit *);
		bool Set(size_t, Unit *);
		Unit *Get(size_t) const;
		Unit *Get(const std::string &);
		bool Have(const std::string &);
		bool IsNull() const;
		inline explicit operator bool() const {return m_boolValue;}
		inline explicit operator char() const {return m_charValue;}
		inline explicit operator unsigned char() const {return m_ucharValue;}
		inline explicit operator short() const {return m_shortValue;}
		inline explicit operator unsigned short() const {return m_ushortValue;}
		inline explicit operator int() const {return m_intValue;}
		inline explicit operator unsigned int() const {return m_uintValue;}
		inline explicit operator long() const {return m_longValue;}
		inline explicit operator unsigned long() const {return m_ulongValue;}
		inline explicit operator long long() const {return m_longLongValue;}
		inline explicit operator unsigned long long() const {return m_ulongLongValue;}
		inline explicit operator float() const {return m_floatValue;}
		inline explicit operator double() const {return m_doubleValue;}
		inline explicit operator long double() const {return m_longDoubleValue;}
		inline explicit operator std::string() const { return m_stringValue; }
		void ClearGroup();

	public:
		~Unit();
	};

	template <> bool Unit::Is<void>() const;
	template <> bool Unit::Is<bool>() const;
	template <> bool Unit::Is<char>() const;
	template <> bool Unit::Is<unsigned char>() const;
	template <> bool Unit::Is<short>() const;
	template <> bool Unit::Is<unsigned short>() const;
	template <> bool Unit::Is<int>() const;
	template <> bool Unit::Is<unsigned int>() const;
	template <> bool Unit::Is<long>() const;
	template <> bool Unit::Is<unsigned long>() const;
	template <> bool Unit::Is<long long>() const;
	template <> bool Unit::Is<unsigned long long>() const;
	template <> bool Unit::Is<float>() const;
	template <> bool Unit::Is<double>() const;
	template <> bool Unit::Is<long double>() const;
	template <> bool Unit::Is<std::string>() const;
}