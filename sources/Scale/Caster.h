#pragma once

#include <string>
#include <unordered_map>
#include "ACaster.h"
#include "VariableType.h"

namespace Scale
{
	class Caster : public ACaster
	{
		using super = ACaster;
		friend class Interpreter;
		friend class Native;
		friend class Script;
	private:
		bool m_isBidirectional;
		VariableType m_typeA;
		VariableType m_typeB;
		std::unordered_map<std::string, std::string> m_aToB;
		std::unordered_map<std::string, std::string> m_bToA;

	private:
		Caster(const Caster &);
		bool Cast(Unit *, const std::string &, Unit *, const std::string &);
		bool Cast(Unit *, Unit *) override;
		bool CanCast(const VariableType &, const VariableType &) override;
		Unit *GetUnitToUse(Unit *, const std::string &);

	public:
		Caster(const std::string &, const std::string &, bool);
		void AddCastDefinition(const std::string &, const std::string &);
	};
}