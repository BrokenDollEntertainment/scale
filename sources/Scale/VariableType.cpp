#include "VariableType.h"

#include "OutputManager.h"
#include "StringUtils.h"

namespace Scale
{
	const VariableType VariableType::invalid;

	VariableType::VariableType() : m_type(EVariableType::INVALID), m_arrayType(nullptr) {}

	VariableType::VariableType(const VariableType &other): m_type(other.m_type), m_arrayType(nullptr), m_name(other.m_name)
	{
		if (other.m_arrayType)
		{
			m_arrayType = new VariableType(*other.m_arrayType);
			if (!m_arrayType)
				OutputManager::Error(OutputType::INITIALISATION, "Cannot initialize variable type ", m_name, ": Out of memory");
		}
	}

	VariableType::VariableType(const EVariableType &type, const VariableType &arrayType) : m_type(EVariableType::ARRAY), m_arrayType(new VariableType(arrayType))
	{
		m_name = "array[" + m_arrayType->m_name + "]";
		if (!m_arrayType)
			OutputManager::Error(OutputType::INITIALISATION, "Cannot initialize variable type ", m_name, ": Out of memory");
	}

	VariableType::VariableType(const EVariableType &type, const EVariableType &arrayType): m_type(EVariableType::ARRAY), m_arrayType(new VariableType(arrayType))
	{
		m_name = "array[" + m_arrayType->m_name + "]";
		if (!m_arrayType)
			OutputManager::Error(OutputType::INITIALISATION, "Cannot initialize variable type ", m_name, ": Out of memory");
	}

	VariableType::VariableType(const EVariableType &type) : m_type(type), m_arrayType(nullptr)
	{
		switch (type)
		{
		case EVariableType::VOID:
		{
			m_name = "void";
			break;
		}
		case EVariableType::GROUP:
		{
			m_name = "group";
			break;
		}
		case EVariableType::ARRAY:
		{
			m_name = "array";
			break;
		}
		case EVariableType::BOOL:
		{
			m_name = "bool";
			break;
		}
		case EVariableType::CHAR:
		{
			m_name = "char";
			break;
		}
		case EVariableType::UNSIGNED_CHAR:
		{
			m_name = "uchar";
			break;
		}
		case EVariableType::SHORT:
		{
			m_name = "short";
			break;
		}
		case EVariableType::UNSIGNED_SHORT:
		{
			m_name = "ushort";
			break;
		}
		case EVariableType::INT:
		{
			m_name = "int";
			break;
		}
		case EVariableType::UNSIGNED_INT:
		{
			m_name = "uint";
			break;
		}
		case EVariableType::LONG:
		{
			m_name = "long";
			break;
		}
		case EVariableType::UNSIGNED_LONG:
		{
			m_name = "ulong";
			break;
		}
		case EVariableType::LONG_LONG:
		{
			m_name = "llong";
			break;
		}
		case EVariableType::UNSIGNED_LONG_LONG:
		{
			m_name = "ullong";
			break;
		}
		case EVariableType::FLOAT:
		{
			m_name = "float";
			break;
		}
		case EVariableType::DOUBLE:
		{
			m_name = "double";
			break;
		}
		case EVariableType::LONG_DOUBLE:
		{
			m_name = "ldouble";
			break;
		}
		case EVariableType::STRING:
		{
			m_name = "string";
			break;
		}
		case EVariableType::OBJECT:
		case EVariableType::COUNT:
		case EVariableType::INVALID:
		{
			m_name = "";
			break;
		}
		}
	}

	VariableType::VariableType(const std::string &name) : m_arrayType(nullptr), m_name(name)
	{
		if (name == "void")
			m_type = EVariableType::VOID;
		else if (name == "group")
			m_type = EVariableType::GROUP;
		else if (StringUtils::StartWith(name, "array"))
		{
			size_t nameSize = name.size();
			if (nameSize != 5)
			{
				if (nameSize > 7)
				{
					if (name[5] == '[' && name[nameSize - 1] == ']')
					{
						std::string arrayType = name.substr(6, nameSize - 7);
						if (arrayType.size() != 0)
						{
							m_arrayType = new VariableType(arrayType);
							if (!m_arrayType)
								OutputManager::Error(OutputType::INITIALISATION, "Cannot initialize variable type ", m_name, ": Out of memory");
						}
						m_type = EVariableType::ARRAY;
					}
					else
						m_type = EVariableType::OBJECT;
				}
				else
					m_type = EVariableType::OBJECT;
			}
			else
				m_type = EVariableType::ARRAY;
		}
		else if (name == "bool")
			m_type = EVariableType::BOOL;
		else if (name == "char")
			m_type = EVariableType::CHAR;
		else if (name == "uchar")
			m_type = EVariableType::UNSIGNED_CHAR;
		else if (name == "short")
			m_type = EVariableType::SHORT;
		else if (name == "ushort")
			m_type = EVariableType::UNSIGNED_SHORT;
		else if (name == "int")
			m_type = EVariableType::INT;
		else if (name == "uint")
			m_type = EVariableType::UNSIGNED_INT;
		else if (name == "long")
			m_type = EVariableType::LONG;
		else if (name == "ulong")
			m_type = EVariableType::UNSIGNED_LONG;
		else if (name == "llong")
			m_type = EVariableType::LONG_LONG;
		else if (name == "ullong")
			m_type = EVariableType::UNSIGNED_LONG_LONG;
		else if (name == "float")
			m_type = EVariableType::FLOAT;
		else if (name == "double")
			m_type = EVariableType::DOUBLE;
		else if (name == "ldouble")
			m_type = EVariableType::LONG_DOUBLE;
		else if (name == "string")
			m_type = EVariableType::STRING;
		else
			m_type = EVariableType::OBJECT;
	}

	VariableType::~VariableType()
	{
		if (m_arrayType)
			delete(m_arrayType);
	}

	bool VariableType::SetArrayType(const VariableType &arrayType)
	{
		if (m_arrayType)
			delete(m_arrayType);
		m_arrayType = new VariableType(arrayType);
		if (!m_arrayType)
		{
			OutputManager::Error(OutputType::INITIALISATION, "Cannot initialize variable type ", m_name, ": Out of memory");
			return false;
		}
		return true;
	}

	std::string VariableType::GetName() const
	{
		if (m_type != EVariableType::ARRAY)
			return m_name;
		else if (m_arrayType)
			return "array[" + m_arrayType->GetName() + "]";
		return "array";
	}

	std::size_t VariableTypeHasher::operator()(const Scale::VariableType &s) const noexcept
	{
		return std::hash<std::string>{}(s.GetName());
	}
}