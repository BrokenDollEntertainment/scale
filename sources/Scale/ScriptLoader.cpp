#include "ScriptLoader.h"

#include <fstream>
#include <filesystem>
#include <sys/stat.h>
#include "OutputManager.h"
#include "StringUtils.h"

namespace Scale
{
	bool ScriptLoader::TreatTags(const std::string &line, size_t &nbChars, std::vector<Tag> &tags)
	{
		size_t lineSize = line.size();
		bool inTagParameter = false;
		bool foundTagEnd = false;
		std::string currentTag;
		std::string currentParameter;
		std::unordered_set<std::string> currentTagParameter;
		for (nbChars = 1; nbChars != lineSize && !foundTagEnd; ++nbChars)
		{
			char c = line[nbChars];
			if (inTagParameter)
			{
				switch (c)
				{
				case ')':
					inTagParameter = false;
				case ',':
				{
					StringUtils::Trim(currentParameter);
					if (currentParameter.size() != 0)
					{
						currentTagParameter.insert(currentParameter);
						currentParameter = "";
					}
					break;
				}
				default:
					currentParameter += c;
				}
			}
			else
			{
				switch (c)
				{
				case ']':
				{
					foundTagEnd = true;
				}
				case ',':
				{
					StringUtils::Trim(currentTag);
					if (currentTag.size() != 0)
					{
						tags.emplace_back(Tag{ currentTag, currentTagParameter });
						currentTag = "";
						currentTagParameter.clear();
					}
					break;
				}
				case '(':
				{
					inTagParameter = true;
					break;
				}
				default:
					currentTag += c;
				}
			}
		}
		return foundTagEnd;
	}

	bool ScriptLoader::LoadFromContent(Script *script, const std::string &content, const std::string &scriptPath, const std::string &scriptName)
	{
		std::vector<std::string> fragments;
		int check = StringUtils::SmartSplit(fragments, content, "}", true);
		switch (check)
		{
		case 1:
		{
			OutputManager::Error(OutputType::PARSING, "Invalid script \"", scriptName, "\": Closing } doesn't match any {");
			return false;
		}
		case 2:
		{
			OutputManager::Error(OutputType::PARSING, "Invalid script \"", scriptName, "\": Non closing {");
			return false;
		}
		case 3:
		{
			OutputManager::Error(OutputType::PARSING, "Invalid script \"", scriptName, "\": Non closing \"");
			return false;
		}
		}
		for (const auto &fragment : fragments)
		{
			std::vector<std::string> declarations;
			StringUtils::SmartSplit(declarations, fragment, ";", false);
			std::vector<Tag> tags;
			for (const auto &declaration : declarations)
			{
				std::string line = declaration;
				while (StringUtils::StartWith(line, "["))
				{
					size_t tagSize = 1;
					if (!TreatTags(line, tagSize, tags))
						return false;
					line = line.substr(tagSize);
				}
				if (StringUtils::StartWith(line, "declare"))
				{
					if (!script->AddDeclaredObject(line, tags))
						return false;
					tags.clear();
				}
				else if (StringUtils::StartWith(line, "import"))
				{
					if (!script->AddImport(line, scriptPath, tags, this))
						return false;
				}
				else if (StringUtils::StartWith(line, "cast"))
				{
					if (!script->AddPublicCast(line, tags))
						return false;
					tags.clear();
				}
				else if (StringUtils::StartWith(line, "!cast"))
				{
					if (!script->AddPrivateCast(line, tags))
						return false;
					tags.clear();
				}
				else
				{
					size_t openBracketPos = line.find('{');
					if (openBracketPos == std::string::npos || line.find('=') < openBracketPos)
					{
						if (!script->AddGlobal(line, tags))
							return false;
						tags.clear();
					}
					else
					{
						if (!script->AddFunction(line, tags))
							return false;
						tags.clear();
					}
				}
			}
		}
		return true;
	}

	Script *ScriptLoader::Load(const std::string &path, bool reload, Native *native)
	{
		std::filesystem::path fsPath(path);
		std::string fileName = fsPath.filename().string();
		std::string relativePath = path.substr(0, path.size() - fileName.size());
		std::filesystem::path fsAbsolutePath = std::filesystem::absolute(fsPath);
		std::string absoluteName = fsAbsolutePath.string();
		std::string absolutePath = absoluteName.substr(0, absoluteName.size() - fileName.size());
		Script *scriptToLoad = nullptr;
		if (!reload)
		{
			auto loadedScriptIt = m_loadedScript.find(absoluteName);
			if (loadedScriptIt != m_loadedScript.end())
				return (*loadedScriptIt).second;
			scriptToLoad = new Script(native, fileName);
			if (!scriptToLoad)
			{
				OutputManager::Error(OutputType::PARSING, "Cannot create new script: Out of memory");
				m_loadedScript[absoluteName] = nullptr;
				return nullptr;
			}
			m_loadedScript[absoluteName] = scriptToLoad;
		}
		else
		{
			auto loadedScriptIt = m_loadedScript.find(absoluteName);
			if (loadedScriptIt != m_loadedScript.end())
			{
				scriptToLoad = (*loadedScriptIt).second;
				if (!scriptToLoad)
				{
					OutputManager::Verbose(OutputType::PARSING, "Cannot reload ", fileName, " try loading it instead");
					return Load(path, false, native);
				}
			}
			else
				return nullptr;
			scriptToLoad->Clear();
		}
		struct stat buf;
		if (stat(path.c_str(), &buf) != 0)
		{
			OutputManager::Error(OutputType::PARSING, "Cannot open ", path, ": No such file");
			m_loadedScript[absoluteName] = nullptr;
			if (!reload)
				delete(scriptToLoad);
			return nullptr;
		}
		std::ifstream fileStream(path.c_str());
		std::string content((std::istreambuf_iterator<char>(fileStream)), std::istreambuf_iterator<char>());
		StringUtils::RemoveComment(content);
		if (LoadFromContent(scriptToLoad, content, relativePath, fileName))
			return scriptToLoad;
		if (!reload)
		{
			delete(scriptToLoad);
			m_loadedScript[absoluteName] = nullptr;
		}
		return nullptr;
	}

	Script *ScriptLoader::Load(const std::string &path, Native *native)
	{
		Script *loadedScript = Load(path, false, native);
		if (loadedScript)
			loadedScript->Compile();
		return loadedScript;
	}

	Script *ScriptLoader::Get(const std::string &path)
	{
		std::string absoluteName = std::filesystem::absolute(std::filesystem::path(path)).string();
		auto loadedScriptIt = m_loadedScript.find(absoluteName);
		if (loadedScriptIt != m_loadedScript.end())
			return (*loadedScriptIt).second;
		return nullptr;
	}

	bool ScriptLoader::Reload(const std::string &path, Native *native)
	{
		Script *loadedScript = Load(path, false, native);
		if (loadedScript)
			loadedScript->Compile();
		return (loadedScript != nullptr);
	}

	ScriptLoader::~ScriptLoader()
	{
		for (const auto &pair : m_loadedScript)
		{
			if (pair.second)
				delete(pair.second);
		}
	}
}