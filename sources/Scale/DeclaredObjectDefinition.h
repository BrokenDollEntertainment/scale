#pragma once

#include <unordered_map>
#include <unordered_set>
#include <vector>
#include "Taggable.h"

namespace Scale
{
	class Unit;
	struct VariableDefinition;
	class VariableType;
	class DeclaredObjectDefinition final : public Taggable
	{
		friend class Native;
		template <typename t_LinkerType>
		friend class Linker;
		friend class Script;
		friend class Unit;
	public:
		typedef std::vector<VariableDefinition>::iterator iterator;
		typedef std::vector<VariableDefinition>::const_iterator const_iterator;

	private:
		std::string m_typeName;
		std::unordered_set<std::string> m_subTypes;
		std::vector<VariableDefinition> m_variables;
		std::unordered_map<std::string, DeclaredObjectDefinition *> m_subTypesDefinition;

	private:
		DeclaredObjectDefinition() = default;
		bool AddVariable(const std::string &, const VariableType &, const std::string &, const std::vector<Tag> &);
		inline bool AddVariable(const std::string &, const VariableType &, const std::vector<Tag> &);
		void AddDefinitionOf(DeclaredObjectDefinition *);
		DeclaredObjectDefinition *GetDefinitionOf(const std::string &) const;

	public:
		inline iterator begin() {return m_variables.begin();}
		inline const_iterator begin() const {return m_variables.begin();}
		inline iterator end() {return m_variables.end();}
		inline const_iterator end() const {return m_variables.end();}

	private:
		static DeclaredObjectDefinition *StringToDefinition(const std::string &);
	};
}