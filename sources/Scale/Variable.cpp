#include "Variable.h"

#include "OutputManager.h"

namespace Scale
{
	Variable Variable::NULL_VARIABLE(nullptr, false, true);

	Variable::Variable() : Variable(nullptr, false, false) {}
	Variable::Variable(Unit *unit, bool autoDelete) : Variable(unit, autoDelete, false) {}
	Variable::Variable(Unit *unit, bool autoDelete, bool raw) : m_unit(unit), m_delete(autoDelete), m_raw(raw) {}

	Variable::Variable(Unit *unit) : m_unit(unit), m_delete(true), m_raw(false)
	{
		if (!m_unit)
		{
			OutputManager::Error(OutputType::RUNTIME, "Cannot create new variable: Out of memory");
			m_delete = false;
		}
	}

	Variable::Variable(const VariableType &type, const CurrentEnvironment &helper) : Variable(new Unit(type, "", helper)) {}
	Variable::Variable(const std::string &name, bool value) : Variable(new Unit(name, value)) {}
	Variable::Variable(const std::string &name, char value) : Variable(new Unit(name, value)) {}
	Variable::Variable(const std::string &name, unsigned char value) : Variable(new Unit(name, value)) {}
	Variable::Variable(const std::string &name, short value) : Variable(new Unit(name, value)) {}
	Variable::Variable(const std::string &name, unsigned short value) : Variable(new Unit(name, value)) {}
	Variable::Variable(const std::string &name, int value) : Variable(new Unit(name, value)) {}
	Variable::Variable(const std::string &name, unsigned int value) : Variable(new Unit(name, value)) {}
	Variable::Variable(const std::string &name, long value) : Variable(new Unit(name, value)) {}
	Variable::Variable(const std::string &name, unsigned long value) : Variable(new Unit(name, value)) {}
	Variable::Variable(const std::string &name, long long value) : Variable(new Unit(name, value)) {}
	Variable::Variable(const std::string &name, unsigned long long value) : Variable(new Unit(name, value)) {}
	Variable::Variable(const std::string &name, float value) : Variable(new Unit(name, value)) {}
	Variable::Variable(const std::string &name, double value) : Variable(new Unit(name, value)) {}
	Variable::Variable(const std::string &name, long double value) : Variable(new Unit(name, value)) {}
	Variable::Variable(const std::string &name, const std::string &value) : Variable(new Unit(name, value)) {}
	Variable::Variable(DeclaredObjectDefinition *definition, const CurrentEnvironment &helper) : Variable(new Unit(definition, "", helper)) {}

	Variable::Variable(Variable &other) : m_unit(other.m_unit), m_delete(other.m_delete), m_raw(false)
	{
		if (!other.m_raw)
		{
			other.m_unit = nullptr;
			other.m_delete = false;
		}
		else
			m_delete = false;
	}

	Variable &Variable::operator=(Variable &other)
	{
		m_unit = other.m_unit;
		m_delete = other.m_delete;
		if (!other.m_raw)
		{
			other.m_unit = nullptr;
			other.m_delete = false;
		}
		else
			m_delete = false;
		return *this;
	}

	Variable::Variable(Variable &&other) noexcept: m_unit(other.m_unit), m_delete(other.m_delete), m_raw(false)
	{
		if (!other.m_raw)
		{
			other.m_unit = nullptr;
			other.m_delete = false;
		}
		else
			m_delete = false;
	}

	Variable::Variable(const std::string &name, std::vector<Variable> &elements): m_delete(true), m_raw(false)
	{
		std::vector<Unit *> unitElements;
		for (Variable &element : elements)
		{
			if (element.m_unit)
			{
				unitElements.emplace_back(element.m_unit);
				element.m_unit = nullptr;
			}
		}
		m_unit = new Unit(name, unitElements);
		if (!m_unit)
		{
			OutputManager::Error(OutputType::RUNTIME, "Cannot create new variable: Out of memory");
			m_delete = false;
		}
	}

	Variable::Variable(const std::string &name, std::vector<Variable> &elements, const VariableType &arrayType) : m_delete(true), m_raw(false)
	{
		std::vector<Unit *> unitElements;
		for (Variable &element : elements)
		{
			if (element.m_unit)
			{
				unitElements.emplace_back(element.m_unit);
				element.m_unit = nullptr;
			}
		}
		m_unit = new Unit(name, unitElements, arrayType);
		if (!m_unit)
		{
			OutputManager::Error(OutputType::RUNTIME, "Cannot create new variable: Out of memory");
			m_delete = false;
		}
	}

	Variable &Variable::operator=(Variable &&other) noexcept
	{
		m_unit = other.m_unit;
		m_delete = other.m_delete;
		if (!other.m_raw)
		{
			other.m_unit = nullptr;
			other.m_delete = false;
		}
		else
			m_delete = false;
		return *this;
	}

	bool Variable::Have(const std::string &element) const
	{
		if (!m_unit)
			return true;
		return m_unit->Have(element);
	}

	bool Variable::IsNull() const
	{
		if (!m_unit)
			return true;
		return m_unit->IsNull();
	}

	std::string Variable::ToString() const
	{
		if (!m_unit)
			return "null";
		return m_unit->ToString();
	}

	std::string Variable::ToRawString() const
	{
		if (!m_unit)
			return "null";
		return m_unit->ToRawString();
	}

	Variable::~Variable()
	{
		if (m_delete && m_unit != nullptr)
			delete(m_unit);
	}

	bool Variable::Set(const std::string &value)
	{
		if (m_raw)
			return false;
		if (m_unit)
			return m_unit->Set(value);
		return false;
	}

	bool Variable::Set(const std::string &key, const std::string &value)
	{
		if (m_raw)
			return false;
		if (m_unit)
			return m_unit->Set(key, value);
		return false;
	}

	bool Variable::Set(const Variable &value)
	{
		if (m_raw || !m_unit || !value.m_unit)
			return false;
		return m_unit->Set(value.m_unit);
	}

	bool Variable::Set(const std::string &key, const Variable &value)
	{
		if (m_raw || !m_unit || !value.m_unit)
			return false;
		return m_unit->Set(key, value.m_unit);
	}

	Variable Variable::Get(const std::string &name) const
	{
		if (!m_unit)
			return Variable::NULL_VARIABLE;
		return Variable(m_unit->Get(name), false);
	}

	Variable Variable::GetMutable() const
	{
		if (!m_unit)
			return Variable::NULL_VARIABLE;
		return Variable(m_unit, false);
	}

	Variable Variable::GetCopy() const
	{
		if (!m_unit)
			return Variable::NULL_VARIABLE;
		Unit *copy = new Unit(m_unit);
		if (!copy)
		{
			OutputManager::Error(OutputType::RUNTIME, "Cannot copy variable: Out of memory");
			return Variable::NULL_VARIABLE;
		}
		return Variable(copy, true);
	}

	bool Variable::HaveTags() const
	{
		if (!m_unit)
			return false;
		return m_unit->HaveTags();
	}

	bool Variable::HaveTagWithName(const Tag &tag) const
	{
		if (!m_unit)
			return false;
		return m_unit->HaveTagWithName(tag);
	}

	bool Variable::HaveTagWithName(const std::string &tagName) const
	{
		if (!m_unit)
			return false;
		return m_unit->HaveTagWithName(tagName);
	}

	bool Variable::HaveTagWithParam(const Tag &tag) const
	{
		if (!m_unit)
			return false;
		return m_unit->HaveTagWithParam(tag);
	}

	bool Variable::HaveTagWithParam(const std::string &tagName, const std::unordered_set<std::string> &tagParams) const
	{
		if (!m_unit)
			return false;
		return m_unit->HaveTagWithParam(tagName, tagParams);
	}

	bool Variable::HaveTag(const Tag &tag) const
	{
		if (!m_unit)
			return false;
		return m_unit->HaveTag(tag);
	}

	bool Variable::HaveTag(const std::string &tagName, const std::unordered_set<std::string> &tagParams) const
	{
		if (!m_unit)
			return false;
		return m_unit->HaveTag(tagName, tagParams);
	}

	bool Variable::AddTag(const std::string &tagName)
	{
		if (!m_unit)
			return false;
		return m_unit->AddTag(tagName);
	}

	bool Variable::AddTag(const Tag &tag)
	{
		if (!m_unit)
			return false;
		return m_unit->AddTag(tag);
	}

	bool Variable::AddTags(const std::vector<std::string> &tagsNames)
	{
		if (!m_unit)
			return false;
		return m_unit->AddTags(tagsNames);
	}

	bool Variable::AddTags(const std::vector<Tag> &tags)
	{
		if (!m_unit)
			return false;
		return m_unit->AddTags(tags);
	}

	bool Variable::AddTags(const Taggable *taggable)
	{
		if (!m_unit)
			return false;
		return m_unit->AddTags(taggable);
	}

	bool Variable::RemoveTag(const Tag &tag)
	{
		if (!m_unit)
			return false;
		return m_unit->RemoveTag(tag);
	}

	bool Variable::RemoveTag(const std::string &tagName)
	{
		if (!m_unit)
			return false;
		return m_unit->RemoveTag(tagName);
	}

	std::string Variable::TagToString() const
	{
		if (!m_unit)
			return "";
		return m_unit->TagToString();
	}

	std::vector<Scale::Tag> Variable::GetTagList() const
	{
		if (!m_unit)
			return std::vector<Tag>();
		return m_unit->GetTagList();
	}

	std::ostream &operator<<(std::ostream &os, const Variable &var)
	{
		os << var.ToString();
		return os;
	}
}
