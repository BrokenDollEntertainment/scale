#include "Signature.h"

#include "CurrentEnvironment.h"
#include "OutputManager.h"
#include "Unit.h"

namespace Scale
{
	void Signature::AddArgument(const std::string &name, const VariableType &type, bool isMutable)
	{
		Parameter parameter(name, type, isMutable);
		m_arguments.emplace_back(parameter);
	}

	Signature::Signature() : m_returnType(EVariableType::VOID) {}

	bool Signature::CompileParametersDefault(const CurrentEnvironment &helper, const std::unordered_map<std::string, std::string> &defaultValues)
	{
		for (auto &parameter : m_arguments)
		{
			auto it = defaultValues.find(parameter.GetName());
			if (it != defaultValues.end())
			{
				std::string defaultValue = (*it).second;
				const VariableType &parameterType = parameter.GetType();
				Unit *newUnit;
				if (parameterType == EVariableType::OBJECT)
				{
					std::string parameterTypeName = parameterType.GetName();
					if (DeclaredObjectDefinition *declaredObjectDefinition = helper.GetObjectDefinition(parameterTypeName))
						newUnit = new Unit(declaredObjectDefinition, defaultValue, helper);
					else
					{
						OutputManager::Error(OutputType::COMPILATION, "Cannot compile function ", m_name, ": Unknown parameter type ", parameterTypeName, " for ", parameter.GetName());
						return false;
					}
				}
				else
				{
					newUnit = new Unit(parameterType, defaultValue, helper);
				}
				if (!newUnit)
				{
					OutputManager::Error(OutputType::COMPILATION, "Cannot compile function ", m_name, ": Out of memory ");
					return false;
				}
				OutputManager::Verbose(OutputType::COMPILATION, "Assigning default value ", defaultValue, " to parameter ", parameter.GetName(), " in function ", m_name);
				parameter.AssignDefaultValue(newUnit);
			}
		}
		return true;
	}

	Signature::~Signature()
	{
		for (auto &parameter : m_arguments)
		{
			if (parameter.m_defaultValue)
				delete(parameter.m_defaultValue);
		}
	}
}