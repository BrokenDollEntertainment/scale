#pragma once

#include "Callable.h"

namespace Scale
{
	class CallStack;
	class ResolutionTree;
	class Script;
	class Function final : public Callable
	{
		using super = Callable;
		friend class Script;
	private:
		Script *m_script;
		std::vector<std::string> m_uncompiledBody;
		std::vector<ResolutionTree *> m_body;

	private:
		Function(Script *);
		bool AddBody(const std::string &);
		bool Compile();
		bool Call(CallStack &) override;
		void Clear();

	public:
		~Function();
	};
}