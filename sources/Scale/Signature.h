#pragma once

#include <unordered_map>
#include <vector>
#include "Parameter.h"

namespace Scale
{
	class Callable;
	class CurrentEnvironment;
	class Signature final
	{
		friend class Callable;
	private:
		VariableType m_returnType;
		std::string m_name;
		std::vector<Parameter> m_arguments;

	private:
		void AddArgument(const std::string &, const VariableType &, bool);
		bool CompileParametersDefault(const CurrentEnvironment &, const std::unordered_map<std::string, std::string> &);

	public:
		inline const VariableType &GetReturnType() const {return m_returnType;}
		inline const std::string &GetName() const {return m_name;}
		inline const std::vector<Parameter> &GetParameters() const {return m_arguments;}
		Signature();
		~Signature();
	};
}