#include "Variable.h"

#include <sstream>
#include "CurrentEnvironment.h"
#include "OutputManager.h"
#include "Parameters.h"
#include "StringUtils.h"
#include "VariableType.h"

namespace Scale
{
	UnitTyper::TyperArray Unit::m_typer;
	Unit::Unit() : m_error(true), m_build(false), m_null(true), m_type(EVariableType::INVALID), m_definition(nullptr), m_helper(static_cast<Native *>(nullptr)) {}

	//Copy Unit//
	Unit::Unit(Unit *other):
		m_boolValue(other->m_boolValue),
		m_charValue(other->m_charValue),
		m_ucharValue(other->m_ucharValue),
		m_shortValue(other->m_shortValue),
		m_ushortValue(other->m_ushortValue),
		m_intValue(other->m_intValue),
		m_uintValue(other->m_uintValue),
		m_longValue(other->m_longValue),
		m_ulongValue(other->m_ulongValue),
		m_longLongValue(other->m_longLongValue),
		m_ulongLongValue(other->m_ulongLongValue),
		m_floatValue(other->m_floatValue),
		m_doubleValue(other->m_doubleValue),
		m_longDoubleValue(other->m_longDoubleValue),
		m_stringValue(other->m_stringValue),
		m_error(other->m_error),
		m_build(other->m_build),
		m_null(other->m_null),
		m_name(other->m_name),
		m_type(other->m_type),
		m_definition(other->m_definition),
		m_default(other->m_default),
		m_helper(other->m_helper)
	{
		for (const auto &pair : other->m_child)
		{
			Unit *newUnit = new Unit(pair.second);
			if (!newUnit)
			{
				OutputManager::Error(OutputType::INITIALISATION, "Cannot copy variable: Out of memory");
				break;
			}
			m_child[pair.first] = newUnit;
		}
		AddTags(other);
		Build();
	}

	//==================================================Build raw type variable==================================================//
	Unit::Unit(const VariableType &type, const std::string &value, const CurrentEnvironment &helper) : Unit("", type, value, helper) {}
	Unit::Unit(const std::string &name, const VariableType &type, const std::string &value, const CurrentEnvironment &helper) :
		m_error(false),
		m_build(true),
		m_name(name),
		m_type(type),
		m_definition(nullptr),
		m_helper(helper)
	{
		if (type == EVariableType::ARRAY || type == EVariableType::GROUP)
		{
			Unit *groupSize = new Unit(EVariableType::UNSIGNED_LONG_LONG, "0", m_helper);
			if (!groupSize)
			{
				m_error = true;
			}
			else
			{
				m_child["size"] = groupSize;
				if (value.size() != 0)
					Set(value);
				else
					m_null = true;
			}
		}
		else
		{
			if (value.size() != 0)
				Set(value);
			else
				m_null = true;
		}
	}

	//==================================================Build declared object type variable==================================================//
	Unit::Unit(const DeclaredObjectDefinition *type, const std::string &value, const CurrentEnvironment &helper) : Unit("", type, true, value, helper) {}
	Unit::Unit(const std::string &name, const DeclaredObjectDefinition *type, const std::string &value, const CurrentEnvironment &helper) : Unit(name, type, true, value, helper) {}
	//==================================================Smart build declared object type variable==================================================//
	Unit::Unit(const DeclaredObjectDefinition *type, bool build, const std::string &value, const CurrentEnvironment &helper) : Unit("", type, build, value, helper) {}
	Unit::Unit(const std::string &name, const DeclaredObjectDefinition *type, bool build, const std::string &value, const CurrentEnvironment &helper) :
		m_error(false),
		m_build(false),
		m_null(true),
		m_name(name),
		m_type(type->m_typeName),
		m_definition(type),
		m_default(value),
		m_helper(helper)
	{
		if (build)
		{
			if (type)
			{
				BuildType(type);
				if (value.size() != 0)
					Set(value);
				else
					m_null = true;
			}
			else
				m_null = true;
		}
		if (type)
			AddTags(type);
	}

	Unit::Unit(const std::string &name, bool value) : m_boolValue(value), m_error(false), m_build(true), m_null(false), m_name(name), m_type(EVariableType::BOOL), m_definition(nullptr), m_helper(static_cast<Native *>(nullptr)) {}
	Unit::Unit(const std::string &name, char value) : m_charValue(value), m_error(false), m_build(true), m_null(false), m_name(name), m_type(EVariableType::CHAR), m_definition(nullptr), m_helper(static_cast<Native *>(nullptr)) {}
	Unit::Unit(const std::string &name, unsigned char value) : m_ucharValue(value), m_error(false), m_build(true), m_null(false), m_name(name), m_type(EVariableType::UNSIGNED_CHAR), m_definition(nullptr), m_helper(static_cast<Native *>(nullptr)) {}
	Unit::Unit(const std::string &name, short value) : m_shortValue(value), m_error(false), m_build(true), m_null(false), m_name(name), m_type(EVariableType::SHORT), m_definition(nullptr), m_helper(static_cast<Native *>(nullptr)) {}
	Unit::Unit(const std::string &name, unsigned short value) : m_ushortValue(value), m_error(false), m_build(true), m_null(false), m_name(name), m_type(EVariableType::UNSIGNED_SHORT), m_definition(nullptr), m_helper(static_cast<Native *>(nullptr)) {}
	Unit::Unit(const std::string &name, int value) : m_intValue(value), m_error(false), m_build(true), m_null(false), m_name(name), m_type(EVariableType::INT), m_definition(nullptr), m_helper(static_cast<Native *>(nullptr)) {}
	Unit::Unit(const std::string &name, unsigned int value) : m_uintValue(value), m_error(false), m_build(true), m_null(false), m_name(name), m_type(EVariableType::UNSIGNED_INT), m_definition(nullptr), m_helper(static_cast<Native *>(nullptr)) {}
	Unit::Unit(const std::string &name, long value) : m_longValue(value), m_error(false), m_build(true), m_null(false), m_name(name), m_type(EVariableType::LONG), m_definition(nullptr), m_helper(static_cast<Native *>(nullptr)) {}
	Unit::Unit(const std::string &name, unsigned long value) : m_ulongValue(value), m_error(false), m_build(true), m_null(false), m_name(name), m_type(EVariableType::UNSIGNED_LONG), m_definition(nullptr), m_helper(static_cast<Native *>(nullptr)) {}
	Unit::Unit(const std::string &name, long long value) : m_longLongValue(value), m_error(false), m_build(true), m_null(false), m_name(name), m_type(EVariableType::LONG_LONG), m_definition(nullptr), m_helper(static_cast<Native *>(nullptr)) {}
	Unit::Unit(const std::string &name, unsigned long long value) : m_ulongLongValue(value), m_error(false), m_build(true), m_null(false), m_name(name), m_type(EVariableType::UNSIGNED_LONG_LONG), m_definition(nullptr), m_helper(static_cast<Native *>(nullptr)) {}
	Unit::Unit(const std::string &name, float value) : m_floatValue(value), m_error(false), m_build(true), m_null(false), m_name(name), m_type(EVariableType::FLOAT), m_definition(nullptr), m_helper(static_cast<Native *>(nullptr)) {}
	Unit::Unit(const std::string &name, double value) : m_doubleValue(value), m_error(false), m_build(true), m_null(false), m_name(name), m_type(EVariableType::DOUBLE), m_definition(nullptr), m_helper(static_cast<Native *>(nullptr)) {}
	Unit::Unit(const std::string &name, long double value) : m_longDoubleValue(value), m_error(false), m_build(true), m_null(false), m_name(name), m_type(EVariableType::LONG_DOUBLE), m_definition(nullptr), m_helper(static_cast<Native *>(nullptr)) {}
	Unit::Unit(const std::string &name, const std::string &value) : m_stringValue(value), m_error(false), m_build(true), m_null(false), m_name(name), m_type(EVariableType::STRING), m_definition(nullptr), m_helper(static_cast<Native *>(nullptr)) {}
	Unit::Unit(const std::string &name, const std::vector<Unit *> &value) : m_groupValue(value), m_error(false), m_build(true), m_null(false), m_name(name), m_type(EVariableType::GROUP), m_definition(nullptr), m_helper(static_cast<Native *>(nullptr)) {}
	Unit::Unit(const std::string &name, const std::vector<Unit *> &value, const VariableType &arrayType) : m_groupValue(value), m_error(false), m_build(true), m_null(false), m_name(name), m_type(EVariableType::ARRAY, arrayType), m_definition(nullptr), m_helper(static_cast<Native *>(nullptr)) {}

	void Unit::BuildType(const DeclaredObjectDefinition *definition)
	{
		m_build = true;
		for (const auto &variable : *definition)
		{
			Unit *newUnit;
			std::string variableTypeName = variable.m_type.GetName();
			if (variable.m_type != EVariableType::OBJECT)
				newUnit = new Unit(variable.m_name, variable.m_type, variable.m_defaultValue, m_helper);
			else if (variableTypeName == definition->m_typeName)
			{
				if (Parameters::Is("DoSmartBuild", true))
					newUnit = new Unit(variable.m_name, definition, false, variable.m_defaultValue, m_helper);
				else
				{
					OutputManager::Error(OutputType::RUNTIME, "Cannot create \"", variable.m_name, "\": Recursive type without SmartBuild enabled will cause a StackOverflow");
					newUnit = new Unit();
				}
			}
			else if (auto *definitionType = definition->GetDefinitionOf(variableTypeName))
			{
				if (Parameters::Is("DoSmartBuild", true))
					newUnit = new Unit(variable.m_name, definitionType, false, variable.m_defaultValue, m_helper);
				else
					newUnit = new Unit(variable.m_name, definitionType, true, variable.m_defaultValue, m_helper);
			}
			else
			{
				OutputManager::Error(OutputType::RUNTIME, "Cannot create \"", variable.m_name, "\": Unknown type");
				newUnit = new Unit();
			}
			if (!newUnit)
			{
				OutputManager::Error(OutputType::RUNTIME, "Cannot build type ", definition->m_typeName, ": Out of memory");
				return;
			}
			newUnit->AddTags(variable.m_tags);
			m_child[variable.m_name] = newUnit;
		}
	}

	bool Unit::Set(const std::string &value, bool setSubType)
	{
		if (m_error)
			return true;
		Build();
		if (value.size() == 0)
		{
			OutputManager::Error(OutputType::RUNTIME, "Cannot set Unit: Empty value");
			m_null = true;
			return false;
		}
		if (value == "null")
		{
			m_null = true;
			return true;
		}
		return m_typer.Set(m_type.GetType(), this, value, setSubType, m_helper);
	}

	bool Unit::Set(Unit *value, bool setSubType)
	{
		if (m_error)
			return true;
		if (!value)
			return false;
		Build();
		if (!value)
		{
			OutputManager::Error(OutputType::RUNTIME, "Cannot set Unit: Empty value");
			m_null = true;
			return false;
		}
		if (value->m_null)
		{
			m_null = true;
			return true;
		}
		if (m_type == value->m_type)
			return m_typer.Set(m_type.GetType(), this, value, setSubType, m_helper);
		return m_helper.Cast(value, this);
	}

	bool Unit::Set(const std::string &value)
	{
		return Set(value, true);
	}

	bool Unit::Set(Unit *value)
	{
		if (!value)
			return false;
		return Set(value, true);
	}

	bool Unit::Set(const std::string &key, const std::string &value)
	{
		if (key.size() == 0)
			Set(value, true);
		if (m_error)
			return false;
		Build();
		size_t pos = key.find('.');
		if (pos != std::string::npos)
		{
			std::string name = key.substr(0, pos);
			std::string newKey = key.substr(pos + 1);
			auto childIt = m_child.find(name);
			if (childIt != m_child.end() && (*childIt).second->Set(newKey, value))
			{
				m_null = false;
				return true;
			}
			return false;
		}
		auto childToEditIt = m_child.find(key);
		if (childToEditIt != m_child.end() && (*childToEditIt).second->Set(value))
		{
			m_null = false;
			return true;
		}
		return false;
	}

	bool Unit::Set(const std::string &key, Unit *newUnit)
	{
		if (m_error)
			return true;
		if (!newUnit)
			return false;
		Build();
		size_t pos = key.find('.');
		if (pos != std::string::npos)
		{
			std::string name = key.substr(0, pos);
			std::string newKey = key.substr(pos + 1);
			auto childIt = m_child.find(name);
			if (childIt != m_child.end() && (*childIt).second->Set(newKey, newUnit))
			{
				m_null = false;
				return true;
			}
			return false;
		}
		auto childToEditIt = m_child.find(key);
		if (childToEditIt != m_child.end() && (*childToEditIt).second->Set(newUnit))
		{
			m_null = false;
			return true;
		}
		return false;
	}

	bool Unit::Set(size_t idx, Unit *unit)
	{
		if (m_error)
			return true;
		if (m_type != EVariableType::ARRAY && m_type != EVariableType::GROUP)
		{
			OutputManager::Error(OutputType::RUNTIME, "Cannot change element ", idx, " in array ", m_name, ": ", m_name, " is neither a group nor an array");
			return false;
		}
		if (m_groupValue.size() <= idx)
		{
			for (size_t n = m_groupValue.size(); n <= idx; ++n)
				m_groupValue.emplace_back(nullptr);
			m_child["size"]->m_ulongLongValue = idx;
		}
		else if (m_groupValue[idx])
			return m_groupValue[idx]->Set(unit);
		Unit *newUnit =  nullptr;
		if (m_type == EVariableType::ARRAY)
		{
			if (const VariableType *arrayType = m_type.GetArrayType())
			{
				if (*arrayType != unit->m_type)
				{
					if (arrayType->GetType() != EVariableType::OBJECT)
						newUnit = new Unit(*arrayType, "", m_helper);
					else
					{
						if (auto *definitionType = m_helper.GetObjectDefinition(arrayType->GetName()))
						{
							if (Parameters::Is("DoSmartBuild", true))
								newUnit = new Unit(definitionType, false, "", m_helper);
							else
								newUnit = new Unit(definitionType, true, "", m_helper);
						}
						else
						{
							OutputManager::Error(OutputType::RUNTIME, "Cannot change element ", idx, " in array ", m_name, ": Unknown type ", arrayType->GetName());
							return false;
						}
					}
					if (newUnit)
					{
						if (!m_helper.Cast(unit, newUnit))
						{
							delete(newUnit);
							OutputManager::Error(OutputType::RUNTIME, "Cannot change element ", idx, " in array ", m_name, ": Cannot cast ", unit->m_type.GetName(), " into ", arrayType->GetName());
							return false;
						}
					}
				}
				else
					newUnit = new Unit(unit);
			}
			else
			{
				newUnit = new Unit(unit);
				m_type.SetArrayType(unit->m_type);
			}
		}
		else
			newUnit = new Unit(unit);
		if (!newUnit)
		{
			OutputManager::Error(OutputType::RUNTIME, "Cannot change element ", idx, " in array ", m_name, ": Out of memory");
			return false;
		}
		m_groupValue[idx] = newUnit;
		return true;
	}

	bool Unit::Have(const std::string &key)
	{
		if (m_error)
			return false;
		return (Get(key) != nullptr);
	}

	//SmartBuild
	//It will build the declared object only when using it to avoid infinite loop with recursive type
	void Unit::Build()
	{
		if (m_type == EVariableType::OBJECT && !m_build)
		{
			BuildType(m_definition);
			if (m_default.size() != 0)
				Set(m_default, false);
		}
	}

	Unit *Unit::Get(const std::string &key)
	{
		if (m_error)
			return nullptr;
		Build();
		size_t pos = key.find('.');
		if (pos != std::string::npos)
		{
			std::string name = key.substr(0, pos);
			std::string newKey = key.substr(pos + 1);
			auto childIt = m_child.find(name);
			if (childIt != m_child.end())
				return (*childIt).second->Get(newKey);
			return nullptr;
		}
		auto childToGetIt = m_child.find(key);
		if (childToGetIt == m_child.end())
			return nullptr;
		Unit *ret = (*childToGetIt).second;
		ret->Build();
		return ret;
	}

	Unit *Unit::Get(size_t idx) const
	{
		if (m_error)
			return nullptr;
		if (m_groupValue.size() <= idx)
			return nullptr;
		return m_groupValue[idx];
	}

	bool Unit::IsNull() const
	{
		return m_null;
	}

	std::string Unit::ToString() const
	{
		std::string prefix;
		if (Parameters::Is("DisplayTag", true) && HaveTags())
			prefix = TagToString() + " ";
		if (Parameters::Is("DisplayName", true))
			prefix = prefix + m_name + ": ";
		if (m_error)
			return prefix + "ERROR";
		if (!m_build)
			return prefix + "null";
		return prefix + m_typer.ToString(m_type.GetType(), this);
	}

	std::string Unit::ToRawString() const
	{
		if (!m_build)
			return "null";
		return m_typer.ToString(m_type.GetType(), this);
	}

	void Unit::ClearGroup()
	{
		for (auto *elem : m_groupValue)
		{
			if (elem)
				delete(elem);
		}
	}

	Unit::~Unit()
	{
		ClearGroup();
		for (const auto &children : m_child)
			delete(children.second);
	}

	template <> bool Unit::Is<void>() const { return m_type == EVariableType::VOID; }
	template <> bool Unit::Is<bool>() const { return m_type == EVariableType::BOOL; }
	template <> bool Unit::Is<char>() const { return m_type == EVariableType::CHAR; }
	template <> bool Unit::Is<unsigned char>() const { return m_type == EVariableType::UNSIGNED_CHAR; }
	template <> bool Unit::Is<short>() const { return m_type == EVariableType::SHORT; }
	template <> bool Unit::Is<unsigned short>() const { return m_type == EVariableType::UNSIGNED_SHORT; }
	template <> bool Unit::Is<int>() const { return m_type == EVariableType::INT; }
	template <> bool Unit::Is<unsigned int>() const { return m_type == EVariableType::UNSIGNED_INT; }
	template <> bool Unit::Is<long>() const { return m_type == EVariableType::LONG; }
	template <> bool Unit::Is<unsigned long>() const { return m_type == EVariableType::UNSIGNED_LONG; }
	template <> bool Unit::Is<long long>() const { return m_type == EVariableType::LONG_LONG; }
	template <> bool Unit::Is<unsigned long long>() const { return m_type == EVariableType::UNSIGNED_LONG_LONG; }
	template <> bool Unit::Is<float>() const { return m_type == EVariableType::FLOAT; }
	template <> bool Unit::Is<double>() const { return m_type == EVariableType::DOUBLE; }
	template <> bool Unit::Is<long double>() const { return m_type == EVariableType::LONG_DOUBLE; }
	template <> bool Unit::Is<std::string>() const { return m_type == EVariableType::STRING; }
}