#pragma once

#include <string>
#include <unordered_map>
#include "Variable.h"

namespace Scale
{
	class LocalScope final
	{
	private:
		std::unordered_map<std::string, Variable> m_locals;

	public:
		Variable Get(const std::string &);
		void Add(const std::string &, Variable &&);
		bool Have(const std::string &);
	};
}