#pragma once

#include <string>
#include "ACaster.h"
#include "Unit.h"
#include "VariableTypeHelper.h"

namespace Scale
{
	template <typename t_NativeTypeToCastTo>
	class NativeCaster : public ACaster
	{
		using super = ACaster;
		friend class Native;
		friend class Script;
	private:
		VariableType m_type;
		t_NativeTypeToCastTo Unit::*m_value;
		t_NativeTypeToCastTo (*m_stringConverter)(const std::string &);

	private:
		bool CanCast(const VariableType &from, const VariableType &to) override
		{
			return (to == m_type && VariableTypeHelper::IsRaw(from));
		}

		bool Cast(Unit *from, Unit *to) override
		{
			switch (from->m_type.GetType())
			{
			case EVariableType::VOID:
				break;
			case EVariableType::BOOL:
			{
				(to->*m_value) = static_cast<t_NativeTypeToCastTo>(from->m_boolValue);
				break;
			}
			case EVariableType::CHAR:
			{
				(to->*m_value) = static_cast<t_NativeTypeToCastTo>(from->m_charValue);
				break;
			}
			case EVariableType::UNSIGNED_CHAR:
			{
				(to->*m_value) = static_cast<t_NativeTypeToCastTo>(from->m_ucharValue);
				break;
			}
			case EVariableType::SHORT:
			{
				(to->*m_value) = static_cast<t_NativeTypeToCastTo>(from->m_shortValue);
				break;
			}
			case EVariableType::UNSIGNED_SHORT:
			{
				(to->*m_value) = static_cast<t_NativeTypeToCastTo>(from->m_ushortValue);
				break;
			}
			case EVariableType::INT:
			{
				(to->*m_value) = static_cast<t_NativeTypeToCastTo>(from->m_intValue);
				break;
			}
			case EVariableType::UNSIGNED_INT:
			{
				(to->*m_value) = static_cast<t_NativeTypeToCastTo>(from->m_uintValue);
				break;
			}
			case EVariableType::LONG:
			{
				(to->*m_value) = static_cast<t_NativeTypeToCastTo>(from->m_longValue);
				break;
			}
			case EVariableType::UNSIGNED_LONG:
			{
				(to->*m_value) = static_cast<t_NativeTypeToCastTo>(from->m_ulongValue);
				break;
			}
			case EVariableType::LONG_LONG:
			{
				(to->*m_value) = static_cast<t_NativeTypeToCastTo>(from->m_longLongValue);
				break;
			}
			case EVariableType::UNSIGNED_LONG_LONG:
			{
				(to->*m_value) = static_cast<t_NativeTypeToCastTo>(from->m_ulongLongValue);
				break;
			}
			case EVariableType::FLOAT:
			{
				(to->*m_value) = static_cast<t_NativeTypeToCastTo>(from->m_floatValue);
				break;
			}
			case EVariableType::DOUBLE:
			{
				(to->*m_value) = static_cast<t_NativeTypeToCastTo>(from->m_doubleValue);
				break;
			}
			case EVariableType::LONG_DOUBLE:
			{
				(to->*m_value) = static_cast<t_NativeTypeToCastTo>(from->m_longDoubleValue);
				break;
			}
			case EVariableType::STRING:
			{
				try
				{
					(to->*m_value) = m_stringConverter(from->m_stringValue);
				}
				catch (const std::exception &)
				{
					to->m_null = true;
					OutputManager::Error(OutputType::RUNTIME, "Bad format value \"", from->m_stringValue, "\": cannot cast");
					return false;
				}
			}
			case EVariableType::ARRAY:
			case EVariableType::GROUP:
			case EVariableType::OBJECT:
			case EVariableType::COUNT:
			case EVariableType::INVALID:
			default:
				return false;
			}
			to->m_null = from->m_null;
			return true;
		}

	public:
		NativeCaster(const EVariableType &type, t_NativeTypeToCastTo Unit::*value, t_NativeTypeToCastTo (*stringConverter)(const std::string &)) : m_type(type), m_value(value), m_stringConverter(stringConverter) {}
		virtual ~NativeCaster() = default;
	};
}