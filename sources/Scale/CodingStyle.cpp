#include "CodingStyle.h"

#include <regex>
#include "OutputManager.h"
#include "Parameters.h"

constexpr char VARIABLE_ERROR_NAME[] = "Variable";
constexpr char GLOBAL_ERROR_NAME[] = "Global";
constexpr char FUNCTION_ERROR_NAME[] = "Function";
constexpr char DECLARED_OBJECT_ERROR_NAME[] = "Declared object";

static std::regex SNAKE_CASE_REGEX("[a-z]+(_([a-z]*))*");
static std::regex SCREAMING_SNAKE_CASE_REGEX("[A-Z]+(_([A-Z]*))*");
static std::regex KEBAB_CASE_REGEX("[a-z]+(-([a-z]*))*");
static std::regex CAMEL_CASE_REGEX("[a-z]+([A-Z][a-z]*)*");
static std::regex PASCAL_CASE_REGEX("([A-Z][a-z]*)+");

static std::regex SCALE_CASE_REGEX("([A-Z]|[a-z])([A-Z]|[a-z]|[0-9]|_|-)*");

namespace Scale
{
	CodingStyle CodingStyle::ms_codingStyle;
	bool CodingStyle::ms_doCodingStyle = true;

	static bool is_snake_case(const std::string &str) { return std::regex_match(str, SNAKE_CASE_REGEX); }
	static bool is_screaming_snake_case(const std::string &str) { return std::regex_match(str, SCREAMING_SNAKE_CASE_REGEX); }
	static bool is_kebab_case(const std::string &str) { return std::regex_match(str, KEBAB_CASE_REGEX); }
	static bool is_camel_case(const std::string &str) { return std::regex_match(str, CAMEL_CASE_REGEX); }
	static bool is_pascal_case(const std::string &str) { return std::regex_match(str, PASCAL_CASE_REGEX); }

	bool CodingStyle::IsNameCompliant(const NameType &nameType, const std::string &nameToCheck)
	{
		switch (nameType)
		{
		case NameType::VARIABLE:
		{
			if (m_variableChecker)
				return m_variableChecker(nameToCheck);
			return true;
		}
		case NameType::GLOBAL:
		{
			if (m_globalChecker)
				return m_globalChecker(nameToCheck);
			return true;
		}
		case NameType::DECALRED_OBJECT:
		{
			if (m_declaredObjectChecker)
				return m_declaredObjectChecker(nameToCheck);
			return true;
		}
		case NameType::FUNCTION:
		{
			if (m_functionChecker)
				return m_functionChecker(nameToCheck);
			return true;
		}
		case NameType::COUNT:
		case NameType::INVALID:
			return false;
		}
		return false;
	}

	bool CodingStyle::SetChecker(const NameType &nameType, bool (*newChecker)(const std::string &))
	{
		if (!newChecker)
			return false;
		switch (nameType)
		{
		case NameType::VARIABLE:
		{
			m_variableChecker = newChecker;
			return true;
		}
		case NameType::GLOBAL:
		{
			m_globalChecker = newChecker;
			return true;
		}
		case NameType::DECALRED_OBJECT:
		{
			m_declaredObjectChecker = newChecker;
			return true;
		}
		case NameType::FUNCTION:
		{
			m_functionChecker = newChecker;
			return true;
		}
		case NameType::COUNT:
		case NameType::INVALID:
			return false;
		}
		return false;
	}

	void CodingStyle::Init()
	{
		ms_doCodingStyle = Parameters::Is("DoCodingStyle", true);
	}

	void CodingStyle::SetDefaultCodingStyle()
	{
		SetCheckerFor(NameType::VARIABLE, CasePreset::CAMEL_CASE);
		SetCheckerFor(NameType::DECALRED_OBJECT, CasePreset::PASCAL_CASE);
		SetCheckerFor(NameType::FUNCTION, CasePreset::SNAKE_CASE);
		SetCheckerFor(NameType::GLOBAL, CasePreset::SCREAMING_SNAKE_CASE);
	}

	const char *CodingStyle::ErrorNameOf(const NameType &nameType)
	{
		switch (nameType)
		{
		case NameType::VARIABLE:
			return VARIABLE_ERROR_NAME;
		case NameType::GLOBAL:
			return GLOBAL_ERROR_NAME;
		case NameType::DECALRED_OBJECT:
			return DECLARED_OBJECT_ERROR_NAME;
		case NameType::FUNCTION:
			return FUNCTION_ERROR_NAME;
		case NameType::COUNT:
		case NameType::INVALID:
			return "";
		}
		return "";
	}

	bool CodingStyle::SetCheckerFor(const NameType &nameType, const CasePreset &casePreset)
	{
		switch (casePreset)
		{
		case CasePreset::CAMEL_CASE:
			return ms_codingStyle.SetChecker(nameType, &is_camel_case);
		case CasePreset::PASCAL_CASE:
			return ms_codingStyle.SetChecker(nameType, &is_pascal_case);
		case CasePreset::SNAKE_CASE:
			return ms_codingStyle.SetChecker(nameType, &is_snake_case);
		case CasePreset::SCREAMING_SNAKE_CASE:
			return ms_codingStyle.SetChecker(nameType, &is_screaming_snake_case);
		case CasePreset::KEBAB_CASE:
			return ms_codingStyle.SetChecker(nameType, &is_kebab_case);
		case CasePreset::COUNT:
		case CasePreset::INVALID:
			return false;
		}
		return false;
	}

	bool CodingStyle::SetCheckerFor(const NameType &nameType, bool (*newChecker)(const std::string &)) {return ms_codingStyle.SetChecker(nameType, newChecker);}

	bool CodingStyle::CheckName(const NameType &nameType, const std::string &nameToCheck)
	{
		if (!std::regex_match(nameToCheck, SCALE_CASE_REGEX))
		{
			OutputManager::Error(OutputType::PARSING, ErrorNameOf(nameType), " name \"", nameToCheck, "\": A name should start with an alphabetical character and be composed of only alphanumerical character or - or _");
			return false;
		}
		if (!ms_doCodingStyle)
			return true;
		if (!ms_codingStyle.IsNameCompliant(nameType, nameToCheck))
		{
			OutputManager::Error(OutputType::PARSING, ErrorNameOf(nameType), " name \"", nameToCheck, "\": Does not comply to coding style");
			return false;
		}
		return true;
	}
}