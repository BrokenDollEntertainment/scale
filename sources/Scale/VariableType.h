#pragma once

#include <string>
#include "EVariableType.h"

namespace Scale
{
	class VariableType final
	{
	public:
		static const VariableType invalid;
	private:
		EVariableType m_type;
		VariableType *m_arrayType;
		std::string m_name;

	public:
		VariableType();
		VariableType(const VariableType &);
		VariableType(const EVariableType &);
		VariableType(const EVariableType &, const VariableType &);
		VariableType(const EVariableType &, const EVariableType &);
		VariableType(const std::string &);
		inline const EVariableType &GetType() const {return m_type;}
		std::string GetName() const;
		inline const VariableType *GetArrayType() const { return m_arrayType; }
		bool SetArrayType(const VariableType &);
		inline bool operator==(const EVariableType &type) const {return type == m_type;}
		inline bool operator!=(const EVariableType &type) const {return type != m_type;}
		inline bool operator==(const std::string &name) const {return name == m_name;}
		inline bool operator!=(const std::string &name) const {return name != m_name;}
		inline bool operator==(const VariableType &other) const
		{
			return (m_type == other.m_type && m_name == other.m_name && (m_arrayType == other.m_arrayType || (m_arrayType != nullptr && other.m_arrayType != nullptr && *m_arrayType == *other.m_arrayType)));
		}
		inline bool operator!=(const VariableType &other) const {return !(*this == other);}
		~VariableType();
	};
	
	struct VariableTypeHasher
	{
		std::size_t operator()(const Scale::VariableType &) const noexcept;
	};
}