#include "VariableTypeHelper.h"

namespace Scale
{
	bool VariableTypeHelper::IsRaw(const VariableType &variableType)
	{
		const EVariableType &type = variableType.GetType();
		return (type == EVariableType::BOOL || type == EVariableType::CHAR || type == EVariableType::UNSIGNED_CHAR ||
			type == EVariableType::SHORT || type == EVariableType::UNSIGNED_SHORT || type == EVariableType::INT ||
			type == EVariableType::UNSIGNED_INT || type == EVariableType::LONG || type == EVariableType::UNSIGNED_LONG ||
			type == EVariableType::LONG_LONG || type == EVariableType::UNSIGNED_LONG_LONG || type == EVariableType::FLOAT ||
			type == EVariableType::DOUBLE || type == EVariableType::LONG_DOUBLE || type == EVariableType::STRING);
	}

	bool VariableTypeHelper::IsRaw(const std::string &typeName)
	{
		VariableType typeTocheck(typeName);
		return IsRaw(typeTocheck);
	}

	bool VariableTypeHelper::IsNotObject(const VariableType &variableType)
	{
		const EVariableType &type = variableType.GetType();
		return (IsRaw(variableType) || type == EVariableType::ARRAY || type == EVariableType::GROUP);
	}

	bool VariableTypeHelper::IsNotObject(const std::string &typeName)
	{
		VariableType typeTocheck(typeName);
		return IsNotObject(typeTocheck);
	}
}