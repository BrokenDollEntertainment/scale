/*
* File use to list tasks in Visual Assist VA Hashtags list
* Use # followed by one of the following keyword to:
* - task -> Do a task
* - bug -> Fix a bug
*/

//#task Add casting operators to allow implicit cast within the code
//Example:
// cast Pos -> int # Allow to cast from Pos to int but not from int to Pos #
// {
//     x -> int;
// }
// cast Pos <-> array[int] # Allow to cast from Pos to array of int and from array of int to Pos #
// {
//     x <-> [0];
//     y <-> [1];
// }
// cast Pos <-> ScalePos # Allow to cast from Pos to ScalePos and from ScalePos to Pos #
// {
//     x <-> x;
//     y <-> y;
// }

//#task Allow Interpreter::Fill with std::vector WARNING: FOR ARRAY ONLY, No viable fill will work with group

//#task Treat function body syntax (through static Util)

//#task Treat function body to generate working resolution tree